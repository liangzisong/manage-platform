# ************************************************************
# Sequel Ace SQL dump
# 版本号： 20025
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# 主机: 39.98.195.4 (MySQL 5.7.25-log)
# 数据库: test001
# 生成时间: 2023-02-23 01:34:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# 转储表 role_menu
# ------------------------------------------------------------

CREATE TABLE `role_menu` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `menu_id` varchar(32) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `role_menu` WRITE;
/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`)
VALUES
	('1','1','1487270859235131394'),
	('10','1','1487270859251908610'),
	('11','1','1487270859243520001'),
	('12','1','1487270859256102914'),
	('13','1','1487270859247714305'),
	('14','1','c81219dd12c6f8f8e9a27b888d7dd1dc'),
	('15','1','3128c8c10a7d6a6da19a1586fb961833'),
	('1506268531023507457','a3d31cde65bd702d43eed920c25c1a07','5d7fcbd06891ef3a20dda8b062987ca8'),
	('1506268531296137217','a3d31cde65bd702d43eed920c25c1a07','ad960606743aa757a3e329288a406783'),
	('1506268531598127106','a3d31cde65bd702d43eed920c25c1a07','798cbee78a932dc20f2eafdbfcf49003'),
	('1506268531895922690','a3d31cde65bd702d43eed920c25c1a07','beaef6092e08502f923f7589b656d2cd'),
	('1506268532218884098','a3d31cde65bd702d43eed920c25c1a07','59aecf996785cc942341e8ebc1928952'),
	('1547146923947868162','2','60c98068946c36f22e38a53e69eac246'),
	('1547146924363104258','2','34c103122c262f9da855227d2bf14998'),
	('1547146924618956802','2','624204218c75e8149a1fed64c8c73990'),
	('1547146924870615042','2','56081180c826badfe2e3c1f5c7272ee6'),
	('1547146925235519490','2','d7d6f60089fe37bdb152a2fedce20980'),
	('1547146925478789121','2','8b17b32217a9edcde2e18cac99508056'),
	('1547146925784973313','2','dcf2da9b042162e8be070761d617b09d'),
	('1547146926128906242','2','2c43062793b0774de15a79670ec54029'),
	('1547146926414118913','2','019036263a467f111d58188f9c40d713'),
	('1547146926728691713','2','b4fa7417fc2a6c1aa5f051cc9ef9945f'),
	('1547146927026487298','2','c37068640fab07e055f694b2d11d51b9'),
	('1547146927261368322','2','c5038ace7c47e58f97d8ac798440a038'),
	('1547146927802433537','2','9f40582dc1448cdc9f0cbb87ed42227c'),
	('1547146928049897473','2','bc4f93a9baea3c20c046238d4aced715'),
	('1547146928301555713','2','3128c8c10a7d6a6da19a1586fb961833'),
	('1547146928674848770','2','e5af73a587d350d74bb063f3648b9377'),
	('1547146929014587394','2','c5a4d7d49f039aabe6741a821f24e166'),
	('1547146929295605762','2','5c052a1f7b19c06b20feb1181ff38c53'),
	('1547146929660510210','2','f81a4ff46db275cbbea553b8e04cf0bc'),
	('1547146929962500097','2','5f973ddb60f0f5c4d645367c7f0d4607'),
	('1547146930268684290','2','c357e7bbcee80b73acaa4b673123306c'),
	('1547146930511953922','2','2a0e4bc0cfe04042435be5db44f9c91b'),
	('1547146930813943810','2','cd6d90e974a20389dd1a3b9660614ba0'),
	('1547146931120128002','2','de72f41568b15782d283e9a4c0455526'),
	('1547146931371786242','2','ff6dc41c69011fde72ebb58b01f5ae77'),
	('1547146931669581826','2','5d8f82c117e39aac8e865eb2147b2a44'),
	('1547146932370030593','2','c30c6b5a383c171da16587566014bf75'),
	('16','1','2a0e4bc0cfe04042435be5db44f9c91b'),
	('17','1','9f40582dc1448cdc9f0cbb87ed42227c'),
	('18','1','60c98068946c36f22e38a53e69eac246'),
	('19','1','5d8f82c117e39aac8e865eb2147b2a44'),
	('2','1','c37068640fab07e055f694b2d11d51b9'),
	('20','1','019036263a467f111d58188f9c40d713'),
	('21','1','e5af73a587d350d74bb063f3648b9377'),
	('22','1','ff6dc41c69011fde72ebb58b01f5ae77'),
	('23','1','b4fa7417fc2a6c1aa5f051cc9ef9945f'),
	('3','1','de72f41568b15782d283e9a4c0455526'),
	('4','1','c357e7bbcee80b73acaa4b673123306c'),
	('5','1','5c052a1f7b19c06b20feb1181ff38c53'),
	('6','1','8b17b32217a9edcde2e18cac99508056'),
	('7','1','5f973ddb60f0f5c4d645367c7f0d4607'),
	('8','1','c30c6b5a383c171da16587566014bf75'),
	('9','1','a0afa28531d30bab0d6c27585121ec83');

/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_dict
# ------------------------------------------------------------

CREATE TABLE `sys_dict` (
  `id` varchar(32) NOT NULL,
  `dictionary_name` varchar(255) NOT NULL DEFAULT '' COMMENT '字典名',
  `dictionary_describe` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统字典';

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;

INSERT INTO `sys_dict` (`id`, `dictionary_name`, `dictionary_describe`)
VALUES
	('0b80533d8da1c7f50efc42a02bf5d9ce','user_type','用户类型'),
	('29f30264c9ff4e625c3356f5fab5a2f0','del_flag','是否删除'),
	('f38d1349a1b8ecce8b3c24ac248df918','option_type','操作状态');

/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_dict_detail
# ------------------------------------------------------------

CREATE TABLE `sys_dict_detail` (
  `id` varchar(32) NOT NULL,
  `dictionary_label` varchar(255) NOT NULL COMMENT '标签名',
  `dictionary_value` varchar(255) NOT NULL COMMENT '标签值',
  `dictionary_describe` varchar(255) NOT NULL DEFAULT '',
  `sys_dict_id` varchar(255) NOT NULL COMMENT '字典id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统字典详情';

LOCK TABLES `sys_dict_detail` WRITE;
/*!40000 ALTER TABLE `sys_dict_detail` DISABLE KEYS */;

INSERT INTO `sys_dict_detail` (`id`, `dictionary_label`, `dictionary_value`, `dictionary_describe`, `sys_dict_id`)
VALUES
	('1485845875191250946','管理员','system','','0b80533d8da1c7f50efc42a02bf5d9ce'),
	('1485845922595274753','普通用户','common','','0b80533d8da1c7f50efc42a02bf5d9ce'),
	('1489563340425433089','已删除','1','','29f30264c9ff4e625c3356f5fab5a2f0'),
	('1489563395093991425','未删除','0','','29f30264c9ff4e625c3356f5fab5a2f0'),
	('1509072100567027714','成功','0','','f38d1349a1b8ecce8b3c24ac248df918'),
	('1509072124193542145','失败','1','','f38d1349a1b8ecce8b3c24ac248df918');

/*!40000 ALTER TABLE `sys_dict_detail` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_logs
# ------------------------------------------------------------

CREATE TABLE `sys_logs` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT 'id',
  `operation_status` varchar(2) NOT NULL DEFAULT '' COMMENT '操作状态0成功1失败',
  `operation_user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '操作用户id',
  `operation_describe` varchar(200) NOT NULL DEFAULT '' COMMENT '操作描述',
  `request_params` varchar(500) NOT NULL DEFAULT '' COMMENT '请求内容',
  `request_ip` varchar(50) NOT NULL DEFAULT '' COMMENT '请求ip',
  `request_url` varchar(300) NOT NULL DEFAULT '' COMMENT '请求路径',
  `response_data` varchar(800) NOT NULL DEFAULT '' COMMENT '响应内容',
  `err_msg` varchar(500) NOT NULL DEFAULT '' COMMENT '异常信息',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `sys_logs` WRITE;
/*!40000 ALTER TABLE `sys_logs` DISABLE KEYS */;

INSERT INTO `sys_logs` (`id`, `operation_status`, `operation_user_id`, `operation_describe`, `request_params`, `request_ip`, `request_url`, `response_data`, `err_msg`, `create_time`)
VALUES
	('0d500c1df148bb047081195f17e3a5be','0','1','删除|根据id删除日志','[\"46f272d8d516d85c088484913f7231ab\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/46f272d8d516d85c088484913f7231ab','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"success\":true}','',1648645703465),
	('1045c4b2dc6004bc1a0a1d871f950b46','0','1','删除|根据id删除日志','[\"4ac14f472d34370201e010b20801a078\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/4ac14f472d34370201e010b20801a078','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"success\":true}','',1648645698901),
	('524f7d293ee9bd62965ba0287d2a4638','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648645661842,\"errMsg\":\"空指针异常\",\"id\":\"055c56e099f6b995ad214877ade1081a\",\"operationDescribe\":\"删除|根据id删除日志\",\"operationStatus\":\"1\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[\\\"3d48fbe4c9cd362a2c6dbd24d5339fdf\\\"]\",\"requestUrl\":\"http://localhost:8193/sysLogs/deleteById/3d48fbe4c9cd362a2c6dbd24d5339fdf\",\"responseData\":\"\"},{\"createTime\":1648633442473,\"errMsg\":\"\",\"id\":\"4ac14f472d34370201e010b20801a078\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"0\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[{\\\"pageNum\\\":1,\\\"pageSize\\\":10},{}]\",\"requestUrl\":\"http://localhost:8193/sysLogs/getByPage\",\"respons','',1648645690539),
	('5846da936e4577012079ea2dc3ea676a','1','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','','',1648633198534),
	('5f6d9b75336c015e4860423732b50a9d','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648633357878,\"errMsg\":\"\\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'FROM sys_logs AS sl\\n        LEFT JOIN sys_user AS su ON sl.operation_user_id = s\' at line 14\\n### The error may exist in file [/Users/liangzisong/devTools/IdeaProjects/manage-platform/common-modules/target/classes/mappers/SysLogsMapper.xml]\\n### The error may involve defaultParameterMap\\n### The\",\"id\":\"3d48fbe4c9cd362a2c6dbd24d5339fdf\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"1\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"','',1648633669795),
	('71642838a60afea443b4ccb40623bf11','1','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','','\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'FROM sys_logs AS sl\n        LEFT JOIN sys_user AS su ON sl.operation_user_id = s\' at line 14\n### The error may exist in file [/Users/liangzisong/devTools/IdeaProjects/manage-platform/common-modules/target/classes/mappers/SysLogsMapper.xml]\n### The error may involve defaultParameterMap\n### The',1648633297911),
	('73492ef0621c1ecff0f1cca7c0cf1dfb','0','1','删除|根据id删除日志','[\"055c56e099f6b995ad214877ade1081a\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/055c56e099f6b995ad214877ade1081a','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"success\":true}','',1648645693649),
	('7c1d3d0b3ff256031dcdf35315cbdb3b','0','1','删除|根据id删除日志','[\"3d48fbe4c9cd362a2c6dbd24d5339fdf\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/3d48fbe4c9cd362a2c6dbd24d5339fdf','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"success\":true}','',1648645686197),
	('83da4844de18507e14f09a99cc21dde7','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648633357878,\"errMsg\":\"\\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'FROM sys_logs AS sl\\n        LEFT JOIN sys_user AS su ON sl.operation_user_id = s\' at line 14\\n### The error may exist in file [/Users/liangzisong/devTools/IdeaProjects/manage-platform/common-modules/target/classes/mappers/SysLogsMapper.xml]\\n### The error may involve defaultParameterMap\\n### The\",\"id\":\"3d48fbe4c9cd362a2c6dbd24d5339fdf\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"1\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"','',1648633601475),
	('8bf25e06185dfaeca860fb70ffbae4e3','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648645703465,\"errMsg\":\"\",\"id\":\"0d500c1df148bb047081195f17e3a5be\",\"operationDescribe\":\"删除|根据id删除日志\",\"operationStatus\":\"0\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[\\\"46f272d8d516d85c088484913f7231ab\\\"]\",\"requestUrl\":\"http://localhost:8193/sysLogs/deleteById/46f272d8d516d85c088484913f7231ab\",\"responseData\":\"{\\\"code\\\":200,\\\"fail\\\":false,\\\"msg\\\":\\\"操作成功\\\",\\\"success\\\":true}\"},{\"createTime\":1648645698901,\"errMsg\":\"\",\"id\":\"1045c4b2dc6004bc1a0a1d871f950b46\",\"operationDescribe\":\"删除|根据id删除日志\",\"operationStatus\":\"0\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[\\\"4ac14f472d34370201e010b20801a078\\\"]\",\"','',1648645703752),
	('9617d6a62a1a6d9f038ce46a92fedc21','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648645698901,\"errMsg\":\"\",\"id\":\"1045c4b2dc6004bc1a0a1d871f950b46\",\"operationDescribe\":\"删除|根据id删除日志\",\"operationStatus\":\"0\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[\\\"4ac14f472d34370201e010b20801a078\\\"]\",\"requestUrl\":\"http://localhost:8193/sysLogs/deleteById/4ac14f472d34370201e010b20801a078\",\"responseData\":\"{\\\"code\\\":200,\\\"fail\\\":false,\\\"msg\\\":\\\"操作成功\\\",\\\"success\\\":true}\"},{\"createTime\":1648645699185,\"errMsg\":\"\",\"id\":\"46f272d8d516d85c088484913f7231ab\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"0\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[{\\\"pageNum\\\":1,\\\"pageSize\\\":10},{}]\",\"requ','',1648645701658),
	('a1784c866962dc3edc6e0917b02d8826','0','1','删除|根据id删除日志','[\"326b2671d5793c4a59f1c5c9761243be\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/326b2671d5793c4a59f1c5c9761243be','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"success\":true}','',1648645701315),
	('d55e46ed1eaeced76f4316cc8e677258','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648633357878,\"errMsg\":\"\\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'FROM sys_logs AS sl\\n        LEFT JOIN sys_user AS su ON sl.operation_user_id = s\' at line 14\\n### The error may exist in file [/Users/liangzisong/devTools/IdeaProjects/manage-platform/common-modules/target/classes/mappers/SysLogsMapper.xml]\\n### The error may involve defaultParameterMap\\n### The\",\"id\":\"3d48fbe4c9cd362a2c6dbd24d5339fdf\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"1\",\"operationUser\":{\"id\":\"1\",\"username\":\"admin\"},\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"','',1648645656702),
	('e2d59c39ff5d3f2614272d4f572398e9','1','1','删除|根据id删除日志','[\"3d48fbe4c9cd362a2c6dbd24d5339fdf\"]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/deleteById/3d48fbe4c9cd362a2c6dbd24d5339fdf','','删除失败',1648645689304),
	('ee7cb79578b3d983a64a13ce20ea6e99','0','1','查询|分页查询日志','[{\"pageNum\":1,\"pageSize\":10},{}]','0:0:0:0:0:0:0:1','http://localhost:8193/sysLogs/getByPage','{\"code\":200,\"fail\":false,\"msg\":\"操作成功\",\"rows\":[{\"createTime\":1648631503786,\"errMsg\":\"\",\"id\":\"ee7cb79578b3d983a64a13ce20ea6e99\",\"operationDescribe\":\"查询|分页查询日志\",\"operationStatus\":\"1\",\"operationUserId\":\"1\",\"requestIp\":\"0:0:0:0:0:0:0:1\",\"requestParams\":\"[{\\\"pageNum\\\":1,\\\"pageSize\\\":10},{}]\",\"requestUrl\":\"http://localhost:8193/sysLogs/getByPage\",\"responseData\":\"\"}],\"success\":true,\"total\":1,\"totalPage\":1}','',1648631503786);

/*!40000 ALTER TABLE `sys_logs` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_menu
# ------------------------------------------------------------

CREATE TABLE `sys_menu` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `menu_name` varchar(255) NOT NULL COMMENT '权限名',
  `menu_code` varchar(255) NOT NULL COMMENT '权限码',
  `vue_path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `vue_component` varchar(255) NOT NULL DEFAULT '' COMMENT '组件位置',
  `vue_name` varchar(255) NOT NULL DEFAULT '' COMMENT '组件名称',
  `vue_title` varchar(255) NOT NULL DEFAULT '' COMMENT '组件标题',
  `vue_icon` varchar(255) NOT NULL DEFAULT '' COMMENT '组件图标',
  `parent_id` varchar(32) NOT NULL DEFAULT '' COMMENT '父级id',
  `vue_redirect` varchar(255) NOT NULL DEFAULT '' COMMENT '重定向地址',
  `params` varchar(255) NOT NULL DEFAULT '' COMMENT '页面携带参数',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;

INSERT INTO `sys_menu` (`id`, `menu_name`, `menu_code`, `vue_path`, `vue_component`, `vue_name`, `vue_title`, `vue_icon`, `parent_id`, `vue_redirect`, `params`)
VALUES
	('019036263a467f111d58188f9c40d713','[系统角色]列表','sys_role:list','sys_role','modules/system/sysRole/sysRoleIndex','sys_role_list','系统角色','el-icon-user','de72f41568b15782d283e9a4c0455526','',''),
	('07f0e99696e1d11b0b11e06f1eabb808','[table_name5]删除','table_name5:delete','delete','','table_name5_delete','table_name5_删除','el-icon-s-flag','c55c871fe9392fbf8a621b1802a1452b','',''),
	('1f6d0781e458e49aa754f04711141a7d','[table_name5]添加','table_name5:save','save','','table_name5_save','table_name5_添加','el-icon-s-flag','c55c871fe9392fbf8a621b1802a1452b','',''),
	('219e9c25e38fa09c530c97335090a5bd','[table_name5]添加','table_name5:save','save','','table_name5_save','table_name5_添加','el-icon-s-flag','709fb473475b943c62aae959420d9fdc','',''),
	('2a0e4bc0cfe04042435be5db44f9c91b','[系统用户]列表','sys_user:list','sys_user','modules/system/sysUser/sysUserIndex','sys_user_list','系统用户','el-icon-user-solid','de72f41568b15782d283e9a4c0455526','',''),
	('2c43062793b0774de15a79670ec54029','[测试备注]删除','table_name4:delete','delete','','table_name4_delete','测试备注_删除','el-icon-s-flag','c5a4d7d49f039aabe6741a821f24e166','',''),
	('3128c8c10a7d6a6da19a1586fb961833','[系统用户]修改','sys_user:edit','edit','','sys_user_edit','系统用户_修改','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('34c103122c262f9da855227d2bf14998','[系统日志]列表','sys_logs:list','list','modules/logs/sysLogs/sysLogsIndex','sys_logs_list','系统日志','el-icon-receiving','c5038ace7c47e58f97d8ac798440a038','',''),
	('4457b778435fbdcaac72bd9cbe98c0e7','[table_name5]修改','table_name5:edit','edit','','table_name5_edit','table_name5_修改','el-icon-s-flag','c55c871fe9392fbf8a621b1802a1452b','',''),
	('4c177ce098e6641acf81d37c74326099','[table_name5]修改','table_name5:edit','edit','','table_name5_edit','table_name5_修改','el-icon-s-flag','709fb473475b943c62aae959420d9fdc','',''),
	('56081180c826badfe2e3c1f5c7272ee6','[系统日志]删除','sys_logs:delete','delete','','sys_logs_delete','系统日志_删除','el-icon-s-flag','c5038ace7c47e58f97d8ac798440a038','',''),
	('596864ebcf21bb08de103d6b1ee6c819','[table_name5]列表','table_name5:list','list','modules/test5/tableName5/tableName5Index','table_name5_list','table_name5_列表','el-icon-s-flag','709fb473475b943c62aae959420d9fdc','',''),
	('5c052a1f7b19c06b20feb1181ff38c53','[系统菜单]列表','sys_menu:list','list','modules/system/sysMenu/sysMenuIndex','sys_menu_list','系统菜单','el-icon-menu','de72f41568b15782d283e9a4c0455526','',''),
	('5d8f82c117e39aac8e865eb2147b2a44','[系统角色]修改','sys_role:edit','edit','','sys_role_edit','系统角色_修改','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('5f973ddb60f0f5c4d645367c7f0d4607','[系统菜单]添加','sys_menu:save','save','','sys_menu_save','系统菜单_添加','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('60c98068946c36f22e38a53e69eac246','[系统用户]添加','sys_user:save','save','','sys_user_save','系统用户_添加','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('624204218c75e8149a1fed64c8c73990','[系统日志]添加','sys_logs:save','save','','sys_logs_save','系统日志_添加','el-icon-s-flag','c5038ace7c47e58f97d8ac798440a038','',''),
	('709fb473475b943c62aae959420d9fdc','[table_name5]','table_name5','/table_name5','Layout','table_name5','table_name5','el-icon-s-flag','','',''),
	('847f0c74baa6d4ff5fd29cf0812a3225','[table_name5]删除','table_name5:delete','delete','','table_name5_delete','table_name5_删除','el-icon-s-flag','709fb473475b943c62aae959420d9fdc','',''),
	('8b17b32217a9edcde2e18cac99508056','[系统菜单]删除','sys_menu:delete','delete','','sys_menu_delete','系统菜单_删除','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('9f40582dc1448cdc9f0cbb87ed42227c','[系统用户]删除','sys_user:delete','delete','','sys_user_delete','系统用户_删除','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('b4fa7417fc2a6c1aa5f051cc9ef9945f','[系统角色内roleMenu]列表','sys_role:subTableData:list','subTableList','','sys_role-roleMenu','[系统角色内roleMenu]列表','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('bc4f93a9baea3c20c046238d4aced715','[测试备注]列表','table_name4:list','list','modules/test4/tableName4/tableName4Index','table_name4_list','测试备注_列表','el-icon-s-flag','c5a4d7d49f039aabe6741a821f24e166','',''),
	('c30c6b5a383c171da16587566014bf75','[系统菜单内sysMenu]列表','sys_menu:subTableData:list','subTableList','','sys_menu-sys_menu','系统菜单','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('c357e7bbcee80b73acaa4b673123306c','[系统菜单]修改','sys_menu:edit','edit','','sys_menu_edit','系统菜单_修改','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('c37068640fab07e055f694b2d11d51b9','[系统用户内userRole]列表','sys_user:userRole:list','list','','sys_user-userRole','[系统用户内userRole]列表','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('c5038ace7c47e58f97d8ac798440a038','[系统日志]','sys_logs','/sys_logs','Layout','sys_logs','系统日志','el-icon-receiving','','',''),
	('c55c871fe9392fbf8a621b1802a1452b','[table_name5]','table_name5','/table_name5','Layout','table_name5','table_name5','el-icon-s-flag','','',''),
	('c5a4d7d49f039aabe6741a821f24e166','[测试备注]','table_name4','/table_name4','Layout','table_name4','测试备注','el-icon-s-flag','','',''),
	('cd6d90e974a20389dd1a3b9660614ba0','[测试备注内tableName5]列表','table_name4:subTableData:list','subTableList','','table_name4-tableName5','[测试备注内tableName5]列表','el-icon-s-flag','c5a4d7d49f039aabe6741a821f24e166','',''),
	('d7d6f60089fe37bdb152a2fedce20980','[系统日志]修改','sys_logs:edit','edit','','sys_logs_edit','系统日志_修改','el-icon-s-flag','c5038ace7c47e58f97d8ac798440a038','',''),
	('dcf2da9b042162e8be070761d617b09d','[测试备注]添加','table_name4:save','save','','table_name4_save','测试备注_添加','el-icon-s-flag','c5a4d7d49f039aabe6741a821f24e166','',''),
	('dd35790f1506b9d5617c4dec9dd06a83','[table_name5]列表','table_name5:list','list','modules/test05/tableName5/tableName5Index','table_name5_list','table_name5_列表','el-icon-s-flag','c55c871fe9392fbf8a621b1802a1452b','',''),
	('de72f41568b15782d283e9a4c0455526','系统设置','sys_menu','/system_manage','Layout','sys_menu','系统设置','el-icon-setting','','',''),
	('e5af73a587d350d74bb063f3648b9377','[系统角色]删除','sys_role:delete','delete','','sys_role_delete','系统角色_删除','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','',''),
	('f81a4ff46db275cbbea553b8e04cf0bc','[测试备注]修改','table_name4:edit','edit','','table_name4_edit','测试备注_修改','el-icon-s-flag','c5a4d7d49f039aabe6741a821f24e166','',''),
	('ff6dc41c69011fde72ebb58b01f5ae77','[系统角色]添加','sys_role:save','save','','sys_role_save','系统角色_添加','el-icon-s-flag','de72f41568b15782d283e9a4c0455526','','');

/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_role
# ------------------------------------------------------------

CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `role_name` varchar(255) NOT NULL COMMENT '角色名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;

INSERT INTO `sys_role` (`id`, `role_name`)
VALUES
	('2','ADMIN');

/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_user
# ------------------------------------------------------------

CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `del_flag` char(1) NOT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;

INSERT INTO `sys_user` (`id`, `username`, `password`, `del_flag`)
VALUES
	('1','admin','$2a$10$UP1pzJ2xy/oc1z0Eww3zK.NklM44jg9g/rsDRq2k1vWWw41QROyeC','0'),
	('2','22','22','0'),
	('afdd706a92497d88fbf4e2e6b178db8c','abc','123456','0');

/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 table_name4
# ------------------------------------------------------------

CREATE TABLE `table_name4` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `test_name` varchar(255) NOT NULL COMMENT '测试名称',
  `aaacs` varchar(255) NOT NULL DEFAULT '',
  `create_time` bigint(20) DEFAULT NULL,
  `update_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `table_name4_uid_uindex` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试备注';

LOCK TABLES `table_name4` WRITE;
/*!40000 ALTER TABLE `table_name4` DISABLE KEYS */;

INSERT INTO `table_name4` (`uid`, `test_name`, `aaacs`, `create_time`, `update_time`)
VALUES
	(3,'system','/Users/liangzisong/Downloads/WX20220704-195014@2x.png',1653901397083,1653901405595),
	(4,'23','/Users/liangzisong/devTools/IdeaProjects/manage-platform/system-modules/target/classes/static/img/pic3350930320.PNG',1654076186075,NULL),
	(5,'22','/Users/liangzisong/devTools/IdeaProjects/manage-platform/system-modules/target/classes/static/img/pic3350930320.PNG',1654076188843,NULL),
	(106,'wqewqr','<p>qwerqwer</p>',1657703691594,NULL);

/*!40000 ALTER TABLE `table_name4` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 table_name5
# ------------------------------------------------------------

CREATE TABLE `table_name5` (
  `id` varchar(32) NOT NULL,
  `column_a` varchar(255) DEFAULT '',
  `table_name_four_id` int(11) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `update_time` bigint(20) DEFAULT NULL,
  `column_b` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `table_name5` WRITE;
/*!40000 ALTER TABLE `table_name5` DISABLE KEYS */;

INSERT INTO `table_name5` (`id`, `column_a`, `table_name_four_id`, `create_time`, `update_time`, `column_b`)
VALUES
	('058b60a5979bad708c811354201ba571','common',3,1656981725369,1656987817627,'0'),
	('1484078155378470914','2222',1,NULL,NULL,''),
	('1485822070024630273','22',1,NULL,NULL,''),
	('1485935038397714434','system',1,NULL,NULL,''),
	('1485937434586898434','system',1,NULL,NULL,''),
	('1485937508670889985','common',1,NULL,NULL,''),
	('1486154305701462018','2121',2,NULL,NULL,''),
	('1491679738719219713','123',1157754886,NULL,NULL,''),
	('1491680689882431489','test',3,NULL,NULL,''),
	('1491680726125412353','qqq',3,NULL,1644479621586,''),
	('23defae8cda0a42d88835a33f55d741f','d f d sa',123,12,212,''),
	('30949b1e378f0ec4fa926a5a1d35aba3','system',3,1656987827906,1657251825188,'123'),
	('33','332121',2,NULL,NULL,''),
	('538713fd643d0c08461a3d540d4a4dc7','system',333,1657251172270,NULL,'字段b_3'),
	('5994329fec3706cb3c23af115a862682','asd',3,1644479617839,NULL,''),
	('6874fa39462a468fe9c84391cad76b3a','1324',1234,2134,1234,''),
	('6fb0099a9b09aa4b9fffd90408b30bcf','common',2222,1657251172201,NULL,'字段b_2'),
	('d88dbb9fdb09400ba9ae6ee568e804df','system',111,1657251172119,NULL,'字段b_1');

/*!40000 ALTER TABLE `table_name5` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 user_role
# ------------------------------------------------------------

CREATE TABLE `user_role` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;

INSERT INTO `user_role` (`id`, `role_id`, `user_id`)
VALUES
	('1','2','1'),
	('1491229711995518977','1','1'),
	('1491701446239928322','2','afdd706a92497d88fbf4e2e6b178db8c');

/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
