-- Create syntax for TABLE 'role_menu'
CREATE TABLE `role_menu`
(
    `id`      varchar(32) NOT NULL COMMENT 'id',
    `role_id` varchar(32) NOT NULL COMMENT '角色id',
    `menu_id` varchar(32) NOT NULL COMMENT '权限id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Create syntax for TABLE 'sys_dict'
CREATE TABLE `sys_dict`
(
    `id`                  varchar(32)  NOT NULL,
    `dictionary_name`     varchar(255) NOT NULL DEFAULT '' COMMENT '字典名',
    `dictionary_describe` varchar(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统字典';

-- Create syntax for TABLE 'sys_dict_detail'
CREATE TABLE `sys_dict_detail`
(
    `id`                  varchar(32)  NOT NULL,
    `dictionary_label`    varchar(255) NOT NULL COMMENT '标签名',
    `dictionary_value`    varchar(255) NOT NULL COMMENT '标签值',
    `dictionary_describe` varchar(255) NOT NULL DEFAULT '',
    `sys_dict_id`         varchar(255) NOT NULL COMMENT '字典id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统字典详情';

-- Create syntax for TABLE 'sys_logs'
CREATE TABLE `sys_logs`
(
    `id`                 varchar(32)  NOT NULL DEFAULT '' COMMENT 'id',
    `operation_status`   varchar(2)   NOT NULL DEFAULT '' COMMENT '操作状态0成功1失败',
    `operation_user_id`  varchar(32)  NOT NULL DEFAULT '' COMMENT '操作用户id',
    `operation_describe` varchar(200) NOT NULL DEFAULT '' COMMENT '操作描述',
    `request_params`     varchar(500) NOT NULL DEFAULT '' COMMENT '请求内容',
    `request_ip`         varchar(50)  NOT NULL DEFAULT '' COMMENT '请求ip',
    `request_url`        varchar(300) NOT NULL DEFAULT '' COMMENT '请求路径',
    `response_data`      varchar(800) NOT NULL DEFAULT '' COMMENT '响应内容',
    `err_msg`            varchar(500) NOT NULL DEFAULT '' COMMENT '异常信息',
    `create_time`        bigint(20) NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Create syntax for TABLE 'sys_menu'
CREATE TABLE `sys_menu`
(
    `id`            varchar(32)  NOT NULL COMMENT 'id',
    `menu_name`     varchar(255) NOT NULL COMMENT '权限名',
    `menu_code`     varchar(255) NOT NULL COMMENT '权限码',
    `vue_path`      varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
    `vue_component` varchar(255) NOT NULL DEFAULT '' COMMENT '组件位置',
    `vue_name`      varchar(255) NOT NULL DEFAULT '' COMMENT '组件名称',
    `vue_title`     varchar(255) NOT NULL DEFAULT '' COMMENT '组件标题',
    `vue_icon`      varchar(255) NOT NULL DEFAULT '' COMMENT '组件图标',
    `parent_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '父级id',
    `vue_redirect`  varchar(255) NOT NULL DEFAULT '' COMMENT '重定向地址',
    `params`        varchar(255) NOT NULL DEFAULT '' COMMENT '页面携带参数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY             `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Create syntax for TABLE 'sys_role'
CREATE TABLE `sys_role`
(
    `id`        varchar(32)  NOT NULL COMMENT 'id',
    `role_name` varchar(255) NOT NULL COMMENT '角色名',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Create syntax for TABLE 'sys_user'
CREATE TABLE `sys_user`
(
    `id`       varchar(32)  NOT NULL COMMENT 'id',
    `username` varchar(255) NOT NULL COMMENT '用户名',
    `password` varchar(255) NOT NULL COMMENT '密码',
    `del_flag` char(1)      NOT NULL COMMENT '是否删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Create syntax for TABLE 'table_name4'
CREATE TABLE `table_name4`
(
    `uid`         int(11) NOT NULL AUTO_INCREMENT,
    `test_name`   varchar(255) NOT NULL COMMENT '测试名称',
    `aaacs`       varchar(255) NOT NULL DEFAULT '',
    `create_time` bigint(20) DEFAULT NULL,
    `update_time` bigint(20) DEFAULT NULL,
    PRIMARY KEY (`uid`),
    UNIQUE KEY `table_name4_uid_uindex` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COMMENT='测试备注';

-- Create syntax for TABLE 'table_name5'
CREATE TABLE `table_name5`
(
    `id`                 varchar(32) NOT NULL,
    `column_a`           varchar(255) DEFAULT '',
    `table_name_four_id` int(11) DEFAULT NULL,
    `create_time`        bigint(20) DEFAULT NULL,
    `update_time`        bigint(20) DEFAULT NULL,
    `column_b`           varchar(255) DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Create syntax for TABLE 'user_role'
CREATE TABLE `user_role`
(
    `id`      varchar(32) NOT NULL COMMENT 'id',
    `role_id` varchar(32) NOT NULL COMMENT '角色id',
    `user_id` varchar(32) NOT NULL COMMENT '用户id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;