//package com.liangzisong;
//
//import com.alibaba.fastjson.JSON;
//import com.lzs.ApiApplication;
//import com.lzs.common.utils.wechat.WeChatUtil;
//import com.lzs.common.vo.CommonResult;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import java.math.BigDecimal;
//import java.util.Map;
//
//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ApiApplication.class)
//@WebAppConfiguration
//public class WecatPayTest {
//    @Autowired
//    private WeChatUtil weChatUtil;
//
//    @Test
//    public void test(){
//        CommonResult<Map<String, String>> mapCommonResult = weChatUtil.uniformOrder(new BigDecimal("20"), "12311dsa34321", "13412", "131");
//        Map<String, String> obj = mapCommonResult.getObj();
//        System.out.println("JSON.toJSONString(obj) = " + JSON.toJSONString(obj));
//    }
//}
