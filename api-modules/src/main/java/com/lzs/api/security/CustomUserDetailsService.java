//package com.lzs.api.security;
//
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.lzs.common.config.Constant;
//import com.lzs.common.dao.SysMenuDao;
//import com.lzs.common.dao.SysRoleDao;
//import com.lzs.common.dao.UserAuthDao;
//import com.lzs.common.exception.LzsBusinessException;
//import com.lzs.common.pojo.addressBookUser.AddressBookUser;
//import com.lzs.common.pojo.common.SysUser;
//import com.lzs.common.redis.RedisUserInfo;
//import com.lzs.common.service.UserAuthService;
//import com.lzs.common.utils.SecurityUser;
//import com.lzs.common.vo.GetUserPowerVo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component("userDetailsService")
//public class CustomUserDetailsService implements UserDetailsService {
//
//    @Autowired
//    private AddressBookUserDao addressBookUserDao;
//    @Autowired
//    private RedisTemplate redisTemplate;
//    @Autowired
//    private UserAuthService userAuthService;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) {
//        // 1. 查询用户
//        AddressBookUser sysUser = addressBookUserDao.selectOne(Wrappers.<AddressBookUser>lambdaQuery()
//                .select()
//                .eq(AddressBookUser::getAuditStatus,1)
//                .eq(AddressBookUser::getBlockStatus,0)
//                .eq(AddressBookUser::getPhoneNumber, username));
//        if (sysUser == null) {
//            throw new LzsBusinessException("手机号不存在");
//        }
//        List simpleGrantedAuthorityList = new ArrayList<SimpleGrantedAuthority>();
//
//        //普通用户
//        if ("1".equals(sysUser.getUserType())) {
//            simpleGrantedAuthorityList.add(new SimpleGrantedAuthority(Constant.ROLE_+"common"));
//            //管理员
//        }else if ("2".equals(sysUser.getUserType())) {
//            simpleGrantedAuthorityList.add(new SimpleGrantedAuthority(Constant.ROLE_+"admin"));
//        }
//        SecurityUser securityUser = new SecurityUser(sysUser.getId(), username, "123456", simpleGrantedAuthorityList);
//        return securityUser;
//    }
//}
