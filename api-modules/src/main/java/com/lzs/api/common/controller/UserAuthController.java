package com.lzs.api.common.controller;

import com.lzs.api.common.dto.ApiLoginDto;
import com.lzs.common.config.JwtTokenUtil;
import com.lzs.common.dto.LoginDto;
import com.lzs.common.redis.CommonRedisKeyPrefix;
import com.lzs.common.service.SysMenuService;
import com.lzs.common.service.UserAuthService;
import com.lzs.common.utils.SecurityUser;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.GetUserPowerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("userAuth")
@Api(tags = "用户登陆认证")
@Slf4j
public class UserAuthController {

//    @Autowired
//    private CustomUserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserAuthService userAuthService;

    @ApiOperation("登陆")
    @PostMapping("login")
    public CommonResult authorize(@RequestBody ApiLoginDto loginDto) {
        log.info("loginDto=[{}]",loginDto);
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Object codeObj = valueOperations.get(CommonRedisKeyPrefix.SEND_LOGIN_SMS + loginDto.getPhone());
        if (Objects.isNull(codeObj) || !(codeObj instanceof String)) {
            return CommonResult.Err("请发送验证码");
        }
        String code = (String) codeObj;
        if (!StringUtils.equals(code,loginDto.getCode())){
            return CommonResult.Err("验证码输入错误");
        }
        // 创建Spring Security登录token
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getPhone(), "123456");
        // 委托Spring Security认证组件执行认证过程
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        // 认证成功，设置上下文
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 生成jwt token
        Object principl = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = ((SecurityUser)principl).getUserId();

        String token = JwtTokenUtil.createToken(userId);
        //查询用户角色和权限

//        SysUserBo sysUserBo = new SysUserBo();
//        String uuid = UUIDUtils.generateUUID();
//        ValueOperations valueOperations = redisTemplate.opsForValue();
//        String sysUserBoJsonStr = JSON.toJSONString(sysUserBo);
//        valueOperations.set(uuid,sysUserBoJsonStr,30, TimeUnit.MINUTES);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return CommonResult.OK(token);
    }

    @GetMapping("info")
    public CommonResult info(){
//        SecurityUser securityUser = SecurityUserUtils.getSecurityUser().get();
        return userAuthService.getInfo();


    }

    public CommonResult logout(){
        return CommonResult.OK();
    }

}
