package com.lzs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@MapperScan(basePackages = {"com.lzs.api.common.dao","com.lzs.common.dao","com.lzs.api.modules.*.dao"})
@SpringBootApplication
@ConfigurationPropertiesScan
@EnableSwagger2
//@EnableKnife4j
//@EnableSwagger2WebMvc
//@Import(BeanValidatorPluginsConfiguration.class)
public class ApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

}
