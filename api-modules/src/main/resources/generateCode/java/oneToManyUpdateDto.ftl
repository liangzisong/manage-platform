package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ${oneToManyTableDto.j_oinTable}UpdateByIdJoinDto {

<#list oneToManyTableDto.updateFieldList as updateField>
    @ApiModelProperty("${updateField.columnName}")
    private <#rt>
    <#if updateField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if updateField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if updateField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if updateField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${updateField.columnName};
</#list>

}
