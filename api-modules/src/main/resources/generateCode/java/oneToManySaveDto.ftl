package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ${oneToManyTableDto.j_oinTable}SaveJoinDto {

<#list oneToManyTableDto.saveFieldList as saveField>
    @ApiModelProperty("${saveField.columnName}")
    private <#rt>
    <#if saveField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if saveField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if saveField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if saveField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${saveField.columnName};
</#list>

}
