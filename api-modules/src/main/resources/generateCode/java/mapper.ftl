<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.lzs.system.modules.${moduleName}.dao.${t_ableName}Dao">

    <resultMap id="baseResultMap" type="com.lzs.common.pojo.${moduleName}.${t_ableName}">
        <#list tableFieldList as tableField>
            <#-- 是否为主键 -->
            <#if tableField.primaryKey>
        <id<#rt>
            <#else>
        <result<#rt>
            </#if>
            <#lt> column="${tableField.column_name}" property="${tableField.columnName}" jdbcType="${tableField.jdbcType}"/>
        </#list>
    </resultMap>

    <#-- 分页查询 -->
    <select id="getByPage" resultMap="baseResultMap">
        SELECT
        <#list showFieldList as showField>
            ${asTableName}.${showField.column_name}<#if showField_has_next>,</#if>
        </#list>
        FROM ${table_name} AS ${asTableName}
        <#-- 查询字段 -->
        <#if whereFieldList?? && (whereFieldList?size>0)>
            <where>
                <#list whereFieldList as whereField>
                    <if test="dto.${whereField.columnName}!=null">
                        AND ${asTableName}.${whereField.column_name}
                        <#if whereField.fieldSearchType=="EQ" ||  whereField.fieldSearchType=="SELECT">
                            =
                        </#if>
                        <#if whereField.fieldSearchType=="LIKE">
                            LIKE CONCAT('%',
                        </#if>
                        <#noparse>#{</#noparse>dto.${whereField.columnName}<#noparse>}</#noparse>
                        <#if whereField.fieldSearchType=="LIKE">
                            ,'%')
                        </#if>
                    </if>
                </#list>
            </where>
        </#if>
    </select>
    <update id="deleteFlagById">
        UPDATE ${table_name}
        SET del_flag = '1'
        WHERE id = <#noparse>#{id}</#noparse>
    </update>
</mapper>
