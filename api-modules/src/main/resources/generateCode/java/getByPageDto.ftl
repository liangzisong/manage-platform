package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ${t_ableName}GetByPageDto {

    <#list whereFieldList as whereField>
        @ApiModelProperty("${whereField.columnName}")
        private <#rt>
        <#if whereField.dataType== "VARCHAR">
            <#lt>String<#rt>
        </#if>
        <#if whereField.dataType== "INT">
            <#lt>Integer<#rt>
        </#if>
        <#if whereField.dataType== "BIGINT">
            <#lt>Long<#rt>
        </#if>
        <#if whereField.dataType== "CHAR">
            <#lt>String<#rt>
        </#if>
        <#lt> ${whereField.columnName};
    </#list>

}
