package com.lzs.common.pojo.${moduleName};

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Repository;

@ApiModel("${tableComment}(${t_ableName})")
@Data
public class ${t_ableName} {
<#list tableFieldList as tableField>
    <#if tableField.columnComment!="">
        <#lt>   @ApiModelProperty("${tableField.columnComment}")
    <#else>
        <#lt>   @ApiModelProperty("${tableField.columnName}")
    </#if>
    <#if tableField.primaryKey>
        <#if primaryKeyCreatePolicy == 'ASSIGN_UUID'>
            @TableId(type = IdType.ASSIGN_UUID)
        </#if>
        <#if primaryKeyCreatePolicy == 'INPUT'>
            @TableId(type = IdType.INPUT)
        </#if>
        <#if primaryKeyCreatePolicy == 'ASSIGN_ID'>
            @TableId(type = IdType.ASSIGN_ID)
        </#if>
        <#if primaryKeyCreatePolicy == 'AUTO'>
            @TableId(type = IdType.AUTO)
        </#if>
    </#if>
    <#if tableField.fieldInputType=='CREATE_TIME'>
        @TableField(fill = FieldFill.INSERT)
    </#if>
    <#if tableField.fieldInputType=='UPDATE_TIME'>
        @TableField(fill = FieldFill.UPDATE)
    </#if>
    private <#rt>
    <#if tableField.dataType== "VARCHAR"
    || tableField.dataType== "CHAR"
    || tableField.dataType== "TEXT">
        <#lt>String<#rt>
    </#if>
    <#if tableField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if tableField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#lt> ${tableField.columnName};
</#list>
}
