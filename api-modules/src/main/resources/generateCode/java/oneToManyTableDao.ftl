package com.lzs.system.modules.${moduleName}.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.pojo.${moduleName}.${oneToManyTableDto.j_oinTable}Join;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ${oneToManyTableDto.j_oinTable}JoinDao extends BaseMapper<${oneToManyTableDto.j_oinTable}Join> {

}