package com.lzs.system.modules.${moduleName}.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.common.utils.valid.NumberValidator;
<#if oneToManyTableDto.open>
import com.lzs.system.modules.${moduleName}.dao.${oneToManyTableDto.j_oinTable}Dao;
import com.lzs.common.pojo.${moduleName}.${oneToManyTableDto.j_oinTable};
</#if>
import com.lzs.system.modules.${moduleName}.dto.*;
import com.lzs.system.modules.${moduleName}.service.${t_ableName}Service;
import com.lzs.common.vo.CommonResult;
import com.lzs.system.modules.${moduleName}.dao.${t_ableName}Dao;
import com.lzs.common.pojo.${moduleName}.${t_ableName};
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ${t_ableName}ServiceImpl extends ServiceImpl<${t_ableName}Dao,${t_ableName}> implements ${t_ableName}Service {

    <#if oneToManyTableDto.open>
        @Autowired
        private ${oneToManyTableDto.j_oinTable}JoinDao ${oneToManyTableDto.joinTable}JoinDao;
    </#if>

    @Override
    public CommonResult<${t_ableName}> getByPage(PageQuery pageQuery,${t_ableName}GetByPageDto dto){
        baseMapper.getByPage(pageQuery.toPage(), dto);
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<${t_ableName}> getById(String id){
        ${t_ableName} ${tableName} = baseMapper.selectById(id);
        return CommonResult.OK(${tableName});
    }

    @Override
    public CommonResult updateById(${t_ableName}UpdateByIdDto dto){
        BeanValidator.check(dto,${t_ableName}UpdateByIdDto.class);
        ${t_ableName} ${tableName} = new ${t_ableName}();
        BeanUtils.copyProperties(dto,${tableName});
        int updateRow = baseMapper.updateById(${tableName});
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteById(String id){
        int deleteRow = baseMapper.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteFlagById(String id){
        Integer deleteFlagRow = baseMapper.deleteFlagById(id);
        NumberValidator.checkIsNotOne(deleteFlagRow,"禁用失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult save(${t_ableName}SaveDto dto){
        BeanValidator.check(dto,${t_ableName}UpdateByIdDto.class);
        ${t_ableName} ${tableName} = new ${t_ableName}();
        BeanUtils.copyProperties(dto,${tableName});
        Integer insertRow = baseMapper.insert(${tableName});
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

<#if oneToManyTableDto.open>
    @Override
    public CommonResult get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(PageQuery pageQuery
    ,Get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}PageDto dto){
        ${oneToManyTableDto.joinTable}JoinDao.selectPage(pageQuery.toPage(), Wrappers.<${oneToManyTableDto.j_oinTable}Join>lambdaQuery()
    .select()
    .eq(${oneToManyTableDto.j_oinTable}::get${oneToManyTableDto.j_oinTableField},dto.get${oneToManyTableDto.j_oinTableField}())
    <#list oneToManyTableDto.whereFieldList as whereField>
        <#if whereField.fieldSearchType=="EQ" || whereField.fieldSearchType=="SELECT">
            .eq(
        </#if>
        <#if whereField.fieldSearchType=="LIKE">
            .like(
        </#if>
        <#if whereField.fieldSearchType=="NE">
            .ne(
        </#if>
        <#if whereField.fieldSearchType=="GT">
            .gt(
        </#if>
        <#if whereField.fieldSearchType=="LT">
            .lt(
        </#if>
        <#if whereField.fieldSearchType=="GE">
            .ge(
        </#if>
        <#if whereField.fieldSearchType=="LE">
            .le(
        </#if>
        <#if whereField.jdbcType=="VARCHAR">
            StringUtils.isNotBlank
        <#else >
            Objects.nonNull
        </#if>
            (dto.get${whereField.columnNameTitleCase}()),${oneToManyTableDto.j_oinTable}::get${whereField.columnNameTitleCase},dto.get${whereField.columnNameTitleCase}())

    </#list>
    );
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<${oneToManyTableDto.j_oinTable}> get${oneToManyTableDto.j_oinTable}ById(String id){
    ${oneToManyTableDto.j_oinTable} ${oneToManyTableDto.joinTable}Join = ${oneToManyTableDto.joinTable}JoinDao.selectById(id);
        return CommonResult.OK(${oneToManyTableDto.joinTable}Join);
    }

    @Override
    public CommonResult update${oneToManyTableDto.j_oinTable}ById(${oneToManyTableDto.j_oinTable}UpdateByIdDto dto){
        BeanValidator.check(dto,${oneToManyTableDto.j_oinTable}UpdateByIdJoinDto.class);
        ${oneToManyTableDto.j_oinTable}Join ${oneToManyTableDto.joinTable}Join = new ${oneToManyTableDto.j_oinTable}Join();
        BeanUtils.copyProperties(dto,${oneToManyTableDto.joinTable}Join);
        int updateRow = ${oneToManyTableDto.joinTable}JoinDao.updateById(${oneToManyTableDto.joinTable}Join);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult delete${oneToManyTableDto.j_oinTable}ById(String id){
        int deleteRow = ${oneToManyTableDto.joinTable}JoinDao.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult save${oneToManyTableDto.j_oinTable}(${oneToManyTableDto.j_oinTable}SaveDto dto){
    BeanValidator.check(dto,${oneToManyTableDto.j_oinTable}UpdateByIdDto.class);
        ${oneToManyTableDto.j_oinTable}Join ${oneToManyTableDto.joinTable}Join = new ${oneToManyTableDto.j_oinTable}Join();
        BeanUtils.copyProperties(dto,${oneToManyTableDto.joinTable}Join);
        Integer insertRow = ${oneToManyTableDto.joinTable}JoinDao.insert(${oneToManyTableDto.joinTable}Join);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }
</#if>
}
