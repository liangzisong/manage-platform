package com.lzs.system.modules.${moduleName}.controller;

import com.lzs.system.modules.${moduleName}.dto.*;
import com.lzs.common.pojo.${moduleName}.${t_ableName};
<#if oneToManyTableDto.open>
import com.lzs.common.pojo.${moduleName}.${oneToManyTableDto.j_oinTable}Join;
</#if>
import com.lzs.system.modules.${moduleName}.service.${t_ableName}Service;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.pojo.${moduleName}.${t_ableName};

/**
* ${tableComment}Controller
*/
@RestController
@RequestMapping("${tableName}")
public class ${t_ableName}Controller {

    @Autowired
    private ${t_ableName}Service ${tableName}Service;

    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<${t_ableName}> getByPage(PageQuery pageQuery,${t_ableName}GetByPageDto dto){
        return ${tableName}Service.getByPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<${t_ableName}> getById(@PathVariable String id){
        return ${tableName}Service.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody ${t_ableName}UpdateByIdDto dto){
        return ${tableName}Service.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return ${tableName}Service.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return ${tableName}Service.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody ${t_ableName}SaveDto dto){
        return ${tableName}Service.save(dto);
    }

<#if oneToManyTableDto.open>
    @ApiOperation("查询子表[${oneToManyTableDto.tableComment}]数据")
    @GetMapping("get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page")
    public CommonResult<${oneToManyTableDto.j_oinTable}Join> get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(PageQuery pageQuery
    ,Get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}PageDto dto){
        return ${tableName}Service.get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(pageQuery,dto);
    }

    @ApiOperation("根据id查询子表[${oneToManyTableDto.tableComment}]")
    @GetMapping("get${oneToManyTableDto.j_oinTable}ById/{id}")
    public CommonResult<${oneToManyTableDto.j_oinTable}> get${oneToManyTableDto.j_oinTable}ById(@PathVariable String id){
        return ${tableName}Service.get${oneToManyTableDto.j_oinTable}ById(id);
    }

    @ApiOperation("根据id修改子表[${oneToManyTableDto.tableComment}]")
    @PutMapping("update${oneToManyTableDto.j_oinTable}ById")
    public CommonResult update${oneToManyTableDto.j_oinTable}ById(@RequestBody ${oneToManyTableDto.j_oinTable}UpdateByIdDto dto){
        return ${tableName}Service.update${oneToManyTableDto.j_oinTable}ById(dto);
    }

    @ApiOperation("根据id删除子表[${oneToManyTableDto.tableComment}]")
    @DeleteMapping("delete${oneToManyTableDto.j_oinTable}ById/{id}")
    public CommonResult delete${oneToManyTableDto.j_oinTable}ById(@PathVariable String id){
        return ${tableName}Service.delete${oneToManyTableDto.j_oinTable}ById(id);
    }

    @ApiOperation("添加数据子表[${oneToManyTableDto.tableComment}]")
    @PostMapping("save${oneToManyTableDto.j_oinTable}")
    public CommonResult save${oneToManyTableDto.j_oinTable}(@RequestBody ${oneToManyTableDto.j_oinTable}SaveDto dto){
        return ${tableName}Service.save${oneToManyTableDto.j_oinTable}(dto);
    }
</#if>
}
