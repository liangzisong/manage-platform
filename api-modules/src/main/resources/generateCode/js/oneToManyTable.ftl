<template>
    <el-dialog
            title="${oneToManyTableDto.tableComment}信息"
            :visible.sync="dialogVisible"
            :close-on-click-modal="false"
            width="80%">
        <div>
            <div class="search-group">
                <div class="search-item">
                    <#list oneToManyTableDto.whereFieldList as whereField>
                        <#if whereField.joinDict!="">
                            <el-select clearable v-model="query.${whereField.columnName}" placeholder="请选择${whereField.columnComment}">
                                <el-option
                                        v-for="item in ${whereField.columnName}DictList"
                                        :key="item.value"
                                        :label="item.label"
                                        :value="item.value">
                                </el-option>
                            </el-select>
                        <#elseif (whereField.fieldSearchType == "EQ" || whereField.fieldSearchType == "LIKE")>
                            <el-input v-model="query.${whereField.columnName}" placeholder="请输入${whereField.columnComment}" />
                        </#if>
                    </#list>
                </div>
                <el-button-group>
                    <el-button type="primary" icon="el-icon-plus" @click="add()" />
                    <el-button icon="el-icon-search" @click="search" />
                    <el-button icon="el-icon-refresh" @click="reset" />
                </el-button-group>
            </div>
            <div class="content">
                <el-table v-loading="table.loading" :data="table.data" border style="width: 100%">
                    <#list oneToManyTableDto.showFieldList as showField>
                        <#--如果是字典值-->
                        <#if showField.joinDict!="">
                            <el-table-column align="center" label="${showField.columnComment}">
                                <template slot-scope="{row}">
                                    {{
                                    ${showField.columnName}DictList.filter(d=>d.value===row.${showField.columnName}).length===1?
                                    ${showField.columnName}DictList.filter(d=>d.value===row.${showField.columnName})[0].label:
                                    row.${showField.columnName}
                                    }}
                                </template>
                            </el-table-column>
                        <#elseif showField.fieldInputType=='CREATE_TIME' || showField.fieldInputType=='UPDATE_TIME'>
                            <el-table-column align="center" label="${showField.columnComment}" prop="${showField.columnName}">
                                <template slot-scope="{row}">
                                    {{row.${showField.columnName} | parseTime}}
                                </template>
                            </el-table-column>
                        <#else>
                            <el-table-column align="center" label="${showField.columnComment}" prop="${showField.columnName}" />
                        </#if>
                    </#list>
                    <el-table-column align="center" label="操作" fixed="right" width="100px">
                        <template slot-scope="{row}">
                            <el-button-group class="operation">
                                <el-button type="text" @click="edit('see',row.${oneToManyTableDto.primaryKeyField.columnName})">详情</el-button>
<#--                                <#if oneToManyTableDto.open>-->
<#--                                    <el-button type="text" @click="open${oneToManyTableDto.j_oinTable}(row.${primaryKeyName})">查看${oneToManyTableDto.tableComment}数据</el-button>-->
<#--                                </#if>-->
                                <el-button type="text" @click="edit('edit',row.${oneToManyTableDto.primaryKeyField.columnName})">编辑</el-button>
                                <c-button type="text" auto-confirm @confirm="remove(row.${oneToManyTableDto.primaryKeyField.columnName})"></c-button>
                            </el-button-group>
                        </template>
                    </el-table-column>
                </el-table>
                <table-page :total="table.total" :current="query.pageNum" :size="query.pageSize" @settingTablePage="settingTablePage" />
            </div>
            <Edit ref="edit" @refreshPageData="search" />
        </div>
    </el-dialog>
</template>

<script>
import { delete${oneToManyTableDto.j_oinTable}ById, get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page } from '@/api/modules/${moduleName}/${tableName}Api'
import Edit from '@/views/modules/${moduleName}/${tableName}/operation/${tableName}EditOneToManyTable'
import TablePage from '@/components/tablePage'
import CButton from '@/components/confirmButton/CButton'
import {getSysDictDetaillistByDictLabel} from "@/api/common/dict";

export default {
    name: 'oneToManyTable',
    components: { CButton, TablePage, Edit },
    data() {
        return {
            dialogVisible: false,
            query: {
                pageNum: 1,
                pageSize: 10,
                ${oneToManyTableDto.joinTableFieldHump}:null,
                <#list whereFieldList as whereField>
                ${whereField.columnName}: null,
                </#list>
            },
            table: {
                data: null,
                total: null,
                loading: false
            },
            /*字典表数据*/
        <#list oneToManyTableDto.showFieldList as tableField>
            <#if tableField.joinDict!="">
            ${tableField.columnName}DictList: [],
            </#if>
        </#list>
        }
    },
    created() {
    },
    methods: {
        search() {
            this.query.pageNum = 1
            this.getTableData()
        },
        add() {
            this.$refs['edit'].open({
                optionType:'add',
                optionId:null,
                //操作主表的id
                mainTableId:this.query.${oneToManyTableDto.joinTableFieldHump},
            })
        },
        settingTablePage(pageNum, pageSize) {
            this.query.pageNum = pageNum
            this.query.pageSize = pageSize
            this.getTableData()
        },
        reset() {
            let ${oneToManyTableDto.joinTableFieldHump} = this.query.${oneToManyTableDto.joinTableFieldHump};
            this.query = {
                pageNum: 1,
                pageSize: 10,
                ${oneToManyTableDto.joinTableFieldHump}:${oneToManyTableDto.joinTableFieldHump},
                <#list oneToManyTableDto.whereFieldList as whereField>
                <#if whereField.columnName!=oneToManyTableDto.joinTableFieldHump>
                ${whereField.columnName}: null,
                </#if>
                </#list>
            }
            this.search()
        },
        getTableData() {
            this.table.loading = true
            get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(this.query).then(res => {
                this.table.total = res.total
                this.table.data = res.rows
                this.table.loading = false
            })
        },
        edit(optionType, ${oneToManyTableDto.primaryKeyField.columnName}) {

            this.$refs['edit'].open({
                optionType:optionType,
                optionId:${oneToManyTableDto.primaryKeyField.columnName},
                //操作主表的id
                mainTableId:this.query.${oneToManyTableDto.joinTableFieldHump},
            })
        },
        open(${primaryKeyName}) {
            this.query.${oneToManyTableDto.joinTableFieldHump} = ${primaryKeyName}
            this.dialogVisible = true
            this.getTableData()
            /*设置字典值*/
            <#list oneToManyTableDto.tableFieldList as tableField>
            <#if tableField.joinDict!="">
            getSysDictDetaillistByDictLabel({
                dictLabel:'${tableField.joinDict}'
            }).then(res=>{
                this.${tableField.columnName}DictList = res.rows
            })
            </#if>
            </#list>
        },
        remove(${primaryKeyName}) {
            delete${oneToManyTableDto.j_oinTable}ById(${primaryKeyName}).then(res => {
                this.$message.success('删除成功')
                this.search()
            })
        }
    }
}
</script>

<style scoped>

</style>
