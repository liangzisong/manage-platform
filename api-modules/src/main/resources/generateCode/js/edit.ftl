<template>
    <el-dialog
    :title="optionType==='see'?'查看':(optionType==='edit'?'编辑':(optionType==='add'?'添加':'提示'))"
    :visible.sync="dialogVisible"
    :close-on-click-modal="false"
    width="80%">
        <div>
            <el-form ref="form" class="add-edit-form" :model="formData" label-width="130px">
            <#list updateFieldList as updateField>
                <#--创建时间与修改时间不算-->
                <#if updateField.fieldInputType!='CREATE_TIME' && updateField.fieldInputType!='UPDATE_TIME'>
                    <#--当前字段是主键-->
                    <#if updateField.columnName==primaryKeyName>
                    <#--主键可以被用户输入-->
                        <#if primaryKeyCreatePolicy=='INPUT'>
                            <el-form-item label="${updateField.columnComment}">
                                <el-input class="form-item-val"
                                          :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                          v-model="formData.${updateFieldList[updateField_index].columnName}"></el-input>
                            </el-form-item>
                        </#if>
                    <#else>
                        <el-form-item label="${updateField.columnComment}">
                            <#if updateField.joinDict!="">
                                <el-select class="form-item-val"
                                           :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                           clearable v-model="formData.${updateField.columnName}" placeholder="请选择${updateField.columnComment}">
                                    <el-option
                                            v-for="item in ${updateField.columnName}DictList"
                                            :key="item.value"
                                            :label="item.label"
                                            :value="item.value">
                                    </el-option>
                                </el-select>
                            <#else>
                                <el-input
                                        class="form-item-val"
                                        :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                        v-model="formData.${updateFieldList[updateField_index].columnName}"></el-input>
                            </#if>
                        </el-form-item>
                    </#if>
                </#if>

            </#list>
            </el-form>
        </div>
        <span slot="footer" class="dialog-footer">
      <div v-if="optionType==='edit' || optionType==='add'">
        <el-button @click="close">取 消</el-button>
        <el-button type="primary" @click="save()">确 定</el-button>
      </div>
      <el-button v-else type="primary" @click="close">关 闭</el-button>
    </span>
    </el-dialog>
</template>

<script>
import { getById, updateById, saveData } from '@/api/modules/${moduleName}/${tableName}Api'
import {getSysDictDetaillistByDictLabel} from "@/api/common/dict";

export default {
    name: 'Edit',
    data() {
        return {
            optionType : '',
            dialogVisible: false,
            formData: {},
            //主键
            ${primaryKeyName}: null,
            /*字典表数据*/
            <#list updateFieldList as tableField>
            <#if tableField.joinDict!="">
            ${tableField.columnName}DictList: [],
            </#if>
            </#list>
        }
    },
    methods: {
        open(optionType, ${primaryKeyName}) {
            this.optionType = optionType
            this.${primaryKeyName} = ${primaryKeyName}
            this.dialogVisible = true
            if(optionType !== 'add'){
                this.getByIdData()
            }
            <#list updateFieldList as tableField>
            <#if tableField.joinDict!="">
            getSysDictDetaillistByDictLabel({
                dictLabel:'${tableField.joinDict}'
            }).then(res=>{
                this.${tableField.columnName}DictList = res.rows
            })
            </#if>
            </#list>
        },
        close() {
            this.formData = {}
            this.dialogVisible = false
        },
        save() {
            if(this.optionType === 'add') {
                saveData(this.formData).then(res=>{
                    this.$message.success('添加成功')
                    this.close()
                    this.$emit('refreshPageData')
                })
            }else if(this.optionType === 'edit') {
                updateById(this.formData).then(res=>{
                    this.$message.success('修改成功')
                    this.close()
                    this.$emit('refreshPageData')
                })
            }
        },
        getByIdData() {
            getById(this.${primaryKeyName}).then(res => {
                this.formData = res.obj
            })
        }
    }
}
</script>

<style scoped>

</style>
