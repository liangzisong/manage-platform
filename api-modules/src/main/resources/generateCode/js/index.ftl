<template>
    <div>
        <div class="search-group">
            <div class="search-item">
                <#list whereFieldList as whereField>
                    <#if whereField.joinDict!="">
                        <el-select clearable v-model="query.${whereField.columnName}" placeholder="请选择${whereField.columnComment}">
                            <el-option
                                    v-for="item in ${whereField.columnName}DictList"
                                    :key="item.value"
                                    :label="item.label"
                                    :value="item.value">
                            </el-option>
                        </el-select>
                    <#elseif (whereField.fieldSearchType == "EQ" || whereField.fieldSearchType == "LIKE")>
                        <el-input v-model="query.${whereField.columnName}" placeholder="请输入${whereField.columnComment}" />
                    </#if>
                </#list>
            </div>
            <el-button-group>
                <el-button type="primary" icon="el-icon-plus" @click="add()" />
                <el-button icon="el-icon-search" @click="search" />
                <el-button icon="el-icon-refresh" @click="reset" />
            </el-button-group>
        </div>
        <div class="content">
            <el-table v-loading="table.loading" :data="table.data" border style="width: 100%">
                <#list showFieldList as showField>
                    <#--如果是字典值-->
                    <#if showField.joinDict!="">
                        <el-table-column align="center" label="${showField.columnComment}">
                            <template slot-scope="{row}">
                                {{
                                ${showField.columnName}DictList.filter(d=>d.value===row.${showField.columnName}).length===1?
                                ${showField.columnName}DictList.filter(d=>d.value===row.${showField.columnName})[0].label:
                                row.${showField.columnName}
                                }}
                            </template>
                        </el-table-column>
                    <#elseif showField.fieldInputType=='CREATE_TIME' || showField.fieldInputType=='UPDATE_TIME'>
                        <el-table-column align="center" label="${showField.columnComment}" prop="${showField.columnName}">
                            <template slot-scope="{row}">
                                {{row.${showField.columnName} | parseTime}}
                            </template>
                        </el-table-column>
                    <#else>
                        <el-table-column align="center" label="${showField.columnComment}" prop="${showField.columnName}" />
                    </#if>
                </#list>
                <el-table-column align="center" label="操作" fixed="right" width="100px">
                    <template slot-scope="{row}">
                        <el-button-group class="operation">
                            <el-button type="text" v-permission="['${table_name}:list']" @click="edit('see',row.${primaryKeyName})">详情</el-button>
                            <#if oneToManyTableDto.open>
                                <el-button type="text" v-permission="['${table_name}:subTableData:list']" @click="open${oneToManyTableDto.j_oinTable}(row.${primaryKeyName})">子表数据</el-button>
                            </#if>
                            <el-button type="text" v-permission="['${table_name}:edit']" @click="edit('edit',row.${primaryKeyName})">编辑</el-button>
                            <c-button type="text" v-permission="['${table_name}:delete']" auto-confirm @confirm="remove(row.${primaryKeyName})"></c-button>
                        </el-button-group>
                    </template>
                </el-table-column>
            </el-table>
            <table-page :total="table.total" :current="query.pageNum" :size="query.pageSize" @settingTablePage="settingTablePage" />
        </div>
        <Edit ref="edit" @refreshPageData="search" />
        <#if oneToManyTableDto.open>
        <OneToManyTable ref="oneToManyTable" />
        </#if>
    </div>
</template>

<script>
import { deleteById, getByPage } from '@/api/modules/${moduleName}/${tableName}Api'
import Edit from '@/views/modules/${moduleName}/${tableName}/operation/${tableName}Edit'
<#if oneToManyTableDto.open>
import OneToManyTable from '@/views/modules/${moduleName}/${tableName}/operation/${tableName}OneToManyTable'
</#if>
import TablePage from '@/components/tablePage'
import CButton from '@/components/confirmButton/CButton'
import {getSysDictDetaillistByDictLabel} from "@/api/common/dict";

export default {
    name: '${t_ableName}',
    components: { CButton, TablePage, Edit, <#if oneToManyTableDto.open>OneToManyTable</#if> },
    data() {
        return {
            query: {
                pageNum: 1,
                pageSize: 10,
                <#list whereFieldList as whereField>
                ${whereField.columnName}: null,
                </#list>
            },
            table: {
                data: null,
                total: null,
                loading: false
            },
            /*字典表数据*/
        <#list tableFieldList as tableField>
            <#if tableField.joinDict!="">
            ${tableField.columnName}DictList: [],
            </#if>
        </#list>
        }
    },
    created() {
        this.getTableData()
        <#list tableFieldList as tableField>
            <#if tableField.joinDict!="">
        getSysDictDetaillistByDictLabel({
            dictLabel:'${tableField.joinDict}'
        }).then(res=>{
            this.${tableField.columnName}DictList = res.rows
        })
            </#if>
        </#list>
    },
    methods: {
        search() {
            this.query.pageNum = 1
            this.getTableData()
        },
        add() {
            this.$refs['edit'].open('add',null)
        },
        settingTablePage(pageNum, pageSize) {
            this.query.pageNum = pageNum
            this.query.pageSize = pageSize
            this.getTableData()
        },
        reset() {
            <#list whereFieldList as whereField>
                this.query.${whereField.columnName} = null
            </#list>
            this.search()
        },
        getTableData() {
            this.table.loading = true
            getByPage(this.query).then(res => {
                this.table.total = res.total
                this.table.data = res.rows
                this.table.loading = false
            })
        },
        edit(optionType, ${primaryKeyName}) {
            this.$refs['edit'].open(optionType, ${primaryKeyName})
        },
        <#if oneToManyTableDto.open>
        open${oneToManyTableDto.j_oinTable}(${primaryKeyName}) {
            this.$refs['oneToManyTable'].open(${primaryKeyName})
        },
        </#if>
        remove(${primaryKeyName}) {
            deleteById(${primaryKeyName}).then(res => {
                this.$message.success('删除成功')
                this.search()
            })
        },
    }
}
</script>

<style scoped>

</style>
