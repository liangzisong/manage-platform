import request from '@/utils/request'

/** 根据主键查询 */
export function getById(${primaryKeyName}) {
    return request({
    url: '/${tableName}/getById/'+${primaryKeyName},
    method: 'get'
    })
}

/** 根据主键修改 */
export function updateById(data) {
    return request({
    url: '/${tableName}/updateById',
    method: 'put',
    data
    })
}

/** 根据主键修改 */
export function saveData(data) {
    return request({
        url: '/${tableName}/save',
        method: 'post',
        data
    })
}
/** 分页查询列表数据 */
export function getByPage(params) {
    return request({
    url: '/${tableName}/getByPage',
    method: 'get',
    params
    })
}

/** 根据删除 */
export function deleteById(params) {
    return request({
    url: '/${tableName}/deleteById/'+params,
    method: 'delete'
    })
}

<#if oneToManyTableDto.open>
/** 根据主表id查询 */
export function get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(params) {
    return request({
    url: '/${tableName}/get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page',
    method: 'get',
    params
    })
}
/** 根据主键查询 */
export function get${oneToManyTableDto.j_oinTable}ById(${primaryKeyName}) {
    return request({
    url: '/${tableName}/get${oneToManyTableDto.j_oinTable}ById/'+${primaryKeyName},
    method: 'get'
    })
}

/** 根据主键修改 */
export function update${oneToManyTableDto.j_oinTable}ById(data) {
    return request({
    url: '/${tableName}/update${oneToManyTableDto.j_oinTable}ById',
    method: 'put',
    data
    })
}

/** 根据主键修改 */
export function save${oneToManyTableDto.j_oinTable}Data(data) {
    return request({
    url: '/${tableName}/save${oneToManyTableDto.j_oinTable}',
    method: 'post',
    data
    })
}
/** 根据删除 */
export function delete${oneToManyTableDto.j_oinTable}ById(params) {
    return request({
    url: '/${tableName}/delete${oneToManyTableDto.j_oinTable}ById/'+params,
    method: 'delete'
    })
}
</#if>
