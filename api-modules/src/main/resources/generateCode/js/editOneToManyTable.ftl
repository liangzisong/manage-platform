<template>
    <el-dialog
    :title="optionType==='see'?'查看':(optionType==='edit'?'编辑':(optionType==='add'?'添加':'提示'))"
    :visible.sync="dialogVisible"
    :close-on-click-modal="false"
    append-to-body
    width="80%">
        <div>
            <el-form ref="form" class="add-edit-form" :model="formData" label-width="130px">
<#--                <el-row>-->
<#--                    <el-col :span="12">-->
                        <#list oneToManyTableDto.updateFieldList as updateField>
                            <#--创建时间与修改时间不算-->
                            <#if updateField.fieldInputType!='CREATE_TIME' && updateField.fieldInputType!='UPDATE_TIME'>
                                <#--当前字段是主键-->
                                <#if updateField.columnName==oneToManyTableDto.primaryKeyField.columnName>
                                <#--主键可以被用户输入-->
                                    <#if oneToManyTableDto.primaryKeyCreatePolicy=='INPUT'>
                                        <el-form-item label="${updateField.columnComment}">
                                            <el-input
                                                    :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                    v-model="formData.${oneToManyTableDto.updateFieldList[updateField_index].columnName}"></el-input>
                                        </el-form-item>
                                    </#if>
                                <#elseif updateField.columnName!=oneToManyTableDto.joinTableFieldHump>
                                    <el-form-item label="${updateField.columnComment}">
                                        <#if updateField.joinDict!="">
                                            <el-select class="form-item-val"
                                                       :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                       clearable v-model="formData.${updateField.columnName}" placeholder="请选择${updateField.columnComment}">
                                                <el-option
                                                        v-for="item in ${updateField.columnName}DictList"
                                                        :key="item.value"
                                                        :label="item.label"
                                                        :value="item.value">
                                                </el-option>
                                            </el-select>
                                        <#else>
                                            <el-input
                                                    class="form-item-val"
                                                    :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                    v-model="formData.${oneToManyTableDto.updateFieldList[updateField_index].columnName}"></el-input>
                                        </#if>
                                    </el-form-item>
                                </#if>
                            </#if>
                        </#list>
<#--                    </el-col>-->
<#--                    <el-col :span="12">-->
<#--                        <#list updateFieldList as updateField>-->
<#--                            <#if updateField_index%2!=0>-->
<#--                                <el-form-item label="${updateField.columnComment}">-->
<#--                                    <el-input-->
<#--                                            :disabled="optionType==='see' || ${updateFieldList[i+(updateFieldList?size/2)].haveEdit? string('disabled','')}==='disabled'"-->
<#--                                            v-model="formData.${updateFieldList[i+(updateFieldList?size/2)].columnName}"></el-input>-->
<#--                                </el-form-item>-->
<#--                            </#if>-->
<#--                        </#list>-->
<#--                    </el-col>-->
<#--                </el-row>-->
            </el-form>
        </div>
        <span slot="footer" class="dialog-footer">
      <div v-if="optionType==='edit' || optionType==='add'">
        <el-button @click="close">取 消</el-button>
        <el-button type="primary" @click="save()">确 定</el-button>
      </div>
      <el-button v-else type="primary" @click="close">关 闭</el-button>
    </span>
    </el-dialog>
</template>

<script>
import { get${oneToManyTableDto.j_oinTable}ById, update${oneToManyTableDto.j_oinTable}ById, save${oneToManyTableDto.j_oinTable}Data } from '@/api/modules/${moduleName}/${tableName}Api'
import {getSysDictDetaillistByDictLabel} from "@/api/common/dict";

export default {
    name: 'Edit',
    data() {
        return {
            optionType : '',
            dialogVisible: false,
            formData: {
                ${oneToManyTableDto.joinTableFieldHump}:null
            },
            //主键
            optionId: null,
            mainTableId: null,
            /*字典表数据*/
            <#list oneToManyTableDto.updateFieldList as tableField>
            <#if tableField.joinDict!="">
            ${tableField.columnName}DictList: [],
            </#if>
            </#list>
        }
    },
    methods: {
        open(option) {
            this.optionType = option.optionType
            this.optionId = option.optionId
            this.mainTableId = option.mainTableId
            this.formData.${oneToManyTableDto.joinTableFieldHump} = this.mainTableId
            this.dialogVisible = true
            if(this.optionType !== 'add'){
                this.getByIdData()
            }
            <#list oneToManyTableDto.updateFieldList as tableField>
            <#if tableField.joinDict!="">
            getSysDictDetaillistByDictLabel({
                dictLabel:'${tableField.joinDict}'
            }).then(res=>{
                this.${tableField.columnName}DictList = res.rows
            })
            </#if>
            </#list>
        },
        close() {
            this.formData = {
                ${oneToManyTableDto.joinTableFieldHump}:this.mainTableId
            }
            this.dialogVisible = false
        },
        save() {
            if(this.optionType === 'add') {
                save${oneToManyTableDto.j_oinTable}Data(this.formData).then(res=>{
                    this.$message.success('添加成功')
                    this.close()
                    this.$emit('refreshPageData')
                })
            }else if(this.optionType === 'edit') {
                update${oneToManyTableDto.j_oinTable}ById(this.formData).then(res=>{
                    this.$message.success('修改成功')
                    this.close()
                    this.$emit('refreshPageData')
                })
            }
        },
        getByIdData() {
            get${oneToManyTableDto.j_oinTable}ById(this.optionId).then(res => {
                this.formData = res.obj
            })
        }
    }
}
</script>

<style scoped>

</style>
