package com.lzs.flowable;

import org.flowable.bpmn.model.BpmnModel;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.runtime.ActivityInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestBpmn {
    //仓库接口（加载流程图）
    @Resource
    private RepositoryService repositoryService;
    //运行实例接口（根据流程图创建一个申请）
    @Resource
    private RuntimeService runtimeService;
    //任务接口（查询待办、执行任务）
    @Resource
    private TaskService taskService;
    //历史记录查询接口 （根据历史实例运行信息、查看任历史务运行信息）
    @Resource
    private HistoryService historyService;
//    private String bpmnNameAndKey = "TEST_KEY1";
    private String bpmnNameAndKey = "testFlow";

    /**
     * @description: 建表，默认启动的时候就会建好表格
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:11
     */
    @Test
    public void createTable() {
    }

    /**
     * @description: 第一步：部署流程图
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:12
     */
    @Test
    public void deploy() {
        FileInputStream fileInputStream = null;
        ZipInputStream zipInputStream = null;
        String path  = "/Users/liangzisong/devTools/IdeaProjects/manage-platform/system-modules/src/test/test.bpmn20.xml.zip";
        try {
            File outfile = new File(path);
            fileInputStream = new FileInputStream(outfile);
            zipInputStream = new ZipInputStream(fileInputStream);
            Deployment deploy = repositoryService.createDeployment()
                    //eclipse创建好的bpmn和png文件，放入resources下的processes文件夹中
                    .addZipInputStream(zipInputStream)
                    .key(bpmnNameAndKey)
                    .name(bpmnNameAndKey + "name")
                    .category("HR")
                    .deploy();
            System.out.println("流程部署ID\t" + deploy.getId());
            System.out.println("流程keyID\t" + deploy.getKey());
            System.out.println("流程名称ID\t" + deploy.getName());
            System.out.println("流程分类ID\t" + deploy.getCategory());
            /*
            流程部署ID	1
            流程keyID	TEST_KEY1
            流程名称ID	TEST_KEY1name
            流程分类ID	HR
             */
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(fileInputStream)) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            if (Objects.nonNull(zipInputStream)) {
                try {
                    zipInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    /**
     * @description: 第二步：启动一个流程实例，相当于创建某人申请一次请假
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:12
     */
    @Test
    public void start() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
        System.out.println("流程定义key\t" + processInstance.getProcessDefinitionKey());
        /**
         * 流程实例ID	28a9dc8d-b24d-11ed-b48e-aa5c696654a5
         * 流程定义ID	testFlow:1:d64247c3-b24c-11ed-926c-aa5c696654a5
         * 流程定义key	testFlow
         */
    }

    /**
     * @description: 第三步:查找流程第一个人个人待办任务列表
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:16
     */
    @Test
    public void findMyTask() {
        String assignee = "总经理";
        List<Task> list = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (Task task : list) {
                System.out.println("任务ID\t" + task.getId());
                System.out.println("任务名称\t" + task.getName());
                System.out.println("任务执行人\t" + task.getAssignee());
            }
        }
    }

    /**
     * @description: 第四步：根据上一步的任务ID去执行任务
     * 对于流程图中的每个环节指定的人重复步骤三和步骤四，完成整体流程的运转
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:18
     */
    @Test
    public void complte() {
        String taskId = "28abd862-b24d-11ed-b48e-aa5c696654a5";
        taskService.complete(taskId);
        System.out.println("任务执行完成");
    }

    /**
     * @description: 第五步：根据流程图的ID（也就是流程实例ID），查看该流程的所有历史流程实例
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:19
     */
    @Test
    public void findhistProcessInstance() {
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionKey(bpmnNameAndKey)
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (HistoricProcessInstance historicProcessInstance : list) {
                System.out.println("部署对象ID\t" + historicProcessInstance.getDeploymentId());
                System.out.println("执行时长\t" + historicProcessInstance.getDurationInMillis());
                System.out.println("流程定义ID\t" + historicProcessInstance.getProcessDefinitionId());
                System.out.println("流程定义的key\t" + historicProcessInstance.getProcessDefinitionKey());
                System.out.println("流程定义名称\t" + historicProcessInstance.getProcessDefinitionName());
            }
        }
    }

    @Test
    public void findActivityInstance() {
        List<ActivityInstance> list = runtimeService.createActivityInstanceQuery()
                .processInstanceId("28a9dc8d-b24d-11ed-b48e-aa5c696654a5")
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (ActivityInstance activityInstance : list) {
                System.out.println("当前执行到\t" + activityInstance.getAssignee());
            }
        }

    }

    /**
     * @description: 第六步：查看所有历史任务的信息
     * @return:
     * @author: wangqy
     * @date: 2022/5/2 12:21
     */
    @Test
    public void findHisTask() {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (HistoricTaskInstance historicTaskInstance : list) {
                System.out.println("任务执行人\t" + historicTaskInstance.getAssignee());
                System.out.println("任务名称\t" + historicTaskInstance.getName());
                System.out.println("任务ID\t" + historicTaskInstance.getId());
                System.out.println("流程实例ID\t" + historicTaskInstance.getProcessInstanceId());
                System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
            }
        }
    }
}

