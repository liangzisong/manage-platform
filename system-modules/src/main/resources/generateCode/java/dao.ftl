package com.lzs.system.modules.${moduleName}.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.pojo.${moduleName}.${t_ableName};
<#if inOut>
import com.lzs.system.modules.${moduleName}.dto.${t_ableName}ExcelDto;
</#if>
import com.lzs.system.modules.${moduleName}.dto.${t_ableName}GetByPageDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
<#if inOut>
import java.util.List;
</#if>

@Repository
public interface ${t_ableName}Dao extends BaseMapper<${t_ableName}> {
    /**
     * 分页查询
     */
    IPage<${t_ableName}> getByPage(IPage page,@Param("dto")${t_ableName}GetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);
<#if inOut>
    /**
    * 获取导出excel的数据
    * @param dto
    * @return
    */
    List<${t_ableName}ExcelDto> getExportExcelData(@Param("dto")${t_ableName}GetByPageDto dto);
</#if>
}