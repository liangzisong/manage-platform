package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

<#if required>
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
</#if>
import java.util.List;

@Data
public class ${oneToManyTableDto.j_oinTable}SaveJoinDto {

<#list oneToManyTableDto.saveFieldList as saveField>
    @ApiModelProperty("${saveField.columnComment}")
    <#if saveField.required>
        <#if (saveField.fieldInputType=='INPUT'&&saveField.dataType=='VARCHAR') || saveField.fieldInputType=='RICH_TEXT'>
            @NotBlank(message = "请输入${saveField.columnComment}")
        <#else >
            @NotNull(message = "请输入${saveField.columnComment}")
        </#if>
    </#if>
    private <#rt>
    <#if saveField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if saveField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if saveField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if saveField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${saveField.columnName};
</#list>

}
