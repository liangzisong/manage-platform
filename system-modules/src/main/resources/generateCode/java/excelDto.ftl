package com.lzs.system.modules.${moduleName}.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;
<#if excelGenerateData>
import com.lzs.common.properties.PropertiesUtils;
import com.lzs.common.utils.date.DateUtils;
</#if>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

<#if excelGenerateData>
import javax.validation.constraints.NotBlank;
</#if>

@Data
@ToString
@ApiModel("${tableComment}_数据")
public class ${t_ableName}ExcelDto {

<#list tableFieldList as tableField>
    <#--如果不是创建时间获取修改时间 直接进入-->
    <#if (tableField.fieldInputType!="CREATE_TIME" && tableField.fieldInputType!="UPDATE_TIME")>
    <#lt>   @ApiModelProperty("${tableField.columnComment}")
    <#--无字典表数据-->
    <#if tableField.joinDict=="" && tableField.inOut>
        @Excel(name = "${tableField.columnComment}"
            <#--导出类型为图片-->
            <#if tableField.fieldInputType=="IMAGE_UPLOAD" && tableField.inOut>
                ,type = 2
            </#if>
        )
    </#if>
    private <#rt>
    <#if tableField.dataType== "VARCHAR"
    || tableField.dataType== "CHAR"
    || tableField.dataType== "TEXT">
        <#lt>String<#rt>
    </#if>
    <#if tableField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if tableField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#lt> ${tableField.columnName};

    <#--有字典表数据-->
    <#if tableField.joinDict!="" && tableField.inOut>
    @Excel(name = "${tableField.columnComment}")
    private <#rt>
    <#if tableField.dataType== "VARCHAR"
    || tableField.dataType== "CHAR"
    || tableField.dataType== "TEXT">
        <#lt>String<#rt>
    </#if>
    <#if tableField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if tableField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#lt> ${tableField.columnName}Str;
    </#if>
    <#--如果是创建时间或者修改时间 并且 excelGenerateData字段 打开-->
    <#elseif excelGenerateData>
    <#lt>   @ApiModelProperty("${tableField.columnComment}")
    private <#rt>
    <#if tableField.dataType== "VARCHAR"
    || tableField.dataType== "CHAR"
    || tableField.dataType== "TEXT">
        <#lt>String<#rt>
    </#if>
    <#if tableField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if tableField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#lt> ${tableField.columnName};
    </#if>
    <#if excelGenerateData>
        <#if tableField.fieldInputType=="CREATE_TIME">
    //导出才有
    @Excel(name = "${tableField.columnComment}")
    private String ${tableField.columnName}Str;
        </#if>
        <#if tableField.fieldInputType=="UPDATE_TIME">
    //导出才有
    @Excel(name = "${tableField.columnComment}")
    private String ${tableField.columnName}Str;
        </#if>
        <#if tableField.fieldInputType=="CREATE_TIME">
    public void set${tableField.columnNameTitleCase}(Long ${tableField.columnName}) {
        this.${tableField.columnName} = ${tableField.columnName};
        this.${tableField.columnName}Str = DateUtils.formatYMDHMS(this.${tableField.columnName});
    }
        </#if>
        <#if tableField.fieldInputType=="UPDATE_TIME">
    public void set${tableField.columnNameTitleCase}(Long ${tableField.columnName}) {
        this.${tableField.columnName} = ${tableField.columnName};
        this.${tableField.columnName}Str = DateUtils.formatYMDHMS(this.${tableField.columnName});
    }
        </#if>

    </#if>

</#list>
}
