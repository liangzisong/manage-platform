package com.lzs.system.modules.${moduleName}.service;

import com.baomidou.mybatisplus.extension.service.IService;
<#if oneToManyTableDto.open>
    import com.lzs.common.pojo.${moduleName}.${oneToManyTableDto.j_oinTable}Join;
</#if>
import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.pojo.${moduleName}.${t_ableName};
import com.lzs.system.modules.${moduleName}.dto.*;
<#if inOut>
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
</#if>

/**
 * ${tableComment}Service
 */
public interface ${t_ableName}Service extends IService<${t_ableName}> {

    CommonResult<${t_ableName}> getByPage(PageQuery pageQuery,${t_ableName}GetByPageDto dto);

    CommonResult<${t_ableName}> getById(String id);

    CommonResult updateById(${t_ableName}UpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(${t_ableName}SaveDto dto);

<#if oneToManyTableDto.open>
    CommonResult get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}Page(PageQuery pageQuery
    ,Get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}PageDto dto);

    CommonResult<${oneToManyTableDto.j_oinTable}Join> get${oneToManyTableDto.j_oinTable}ById(String id);

    CommonResult update${oneToManyTableDto.j_oinTable}ById(${oneToManyTableDto.j_oinTable}UpdateByIdJoinDto dto);

    CommonResult delete${oneToManyTableDto.j_oinTable}ById(String id);

    CommonResult save${oneToManyTableDto.j_oinTable}(${oneToManyTableDto.j_oinTable}SaveJoinDto dto);
</#if>
<#if inOut>
    /**
    * 导入
    * @param multipartFile
    * @return
    */
    CommonResult importExcel(MultipartFile multipartFile);

    /**
    * 导出
    * @param dto
    * @param response
    */
    void exportExcel(${t_ableName}GetByPageDto dto, HttpServletResponse response);

</#if>
}
