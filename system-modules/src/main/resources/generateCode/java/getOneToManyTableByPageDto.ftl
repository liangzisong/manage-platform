package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class Get${oneToManyTableDto.j_oinTable}By${oneToManyTableDto.j_oinField}PageDto {

    @ApiModelProperty("一对多关系表连接字段")
    private <#rt>
    <#if oneToManyTableDto.primaryKeyField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if oneToManyTableDto.primaryKeyField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if oneToManyTableDto.primaryKeyField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if oneToManyTableDto.primaryKeyField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${oneToManyTableDto.joinTableFieldHump};

    <#list oneToManyTableDto.whereFieldList as whereField>
        <#if oneToManyTableDto.joinTableFieldHump!=whereField.columnName>
    @ApiModelProperty("${whereField.columnName}")
    private <#rt>
    <#if whereField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if whereField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if whereField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if whereField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${whereField.columnName};
        </#if>

    </#list>

}
