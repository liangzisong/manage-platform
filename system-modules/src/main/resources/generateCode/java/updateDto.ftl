package com.lzs.system.modules.${moduleName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

<#if required>
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
</#if>
import java.util.List;

@Data
public class ${t_ableName}UpdateByIdDto {

<#list updateFieldList as updateField>
    @ApiModelProperty("${updateField.columnComment}")
    <#if updateField.required>
        <#if (updateField.fieldInputType=='INPUT'&&updateField.dataType=='VARCHAR') || updateField.fieldInputType=='RICH_TEXT'>
            @NotBlank(message = "请输入${updateField.columnComment}")
        <#else >
            @NotNull(message = "请输入${updateField.columnComment}")
        </#if>
    </#if>
    private <#rt>
    <#if updateField.dataType== "VARCHAR">
        <#lt>String<#rt>
    </#if>
    <#if updateField.dataType== "INT">
        <#lt>Integer<#rt>
    </#if>
    <#if updateField.dataType== "BIGINT">
        <#lt>Long<#rt>
    </#if>
    <#if updateField.dataType== "CHAR">
        <#lt>String<#rt>
    </#if>
    <#lt> ${updateField.columnName};
</#list>

}
