<template>
    <el-dialog
    :title="optionType==='see'?'查看':(optionType==='edit'?'编辑':(optionType==='add'?'添加':'提示'))"
    :visible.sync="dialogVisible"
    :close-on-click-modal="false"
    append-to-body
    width="80%">
        <div>
            <el-form ref="formDataRef"
                    <#if oneToManyTableDto.required>
                        :rules="formDataRules"
                    </#if>
                     class="add-edit-form"
                     :model="formData" label-width="130px">
<#--                <el-row>-->
<#--                    <el-col :span="12">-->
                        <#list oneToManyTableDto.updateFieldList as updateField>
                            <#--创建时间与修改时间不算-->
                            <#if updateField.fieldInputType!='CREATE_TIME' && updateField.fieldInputType!='UPDATE_TIME'>
                                <#--当前字段是主键-->
                                <#if updateField.columnName==oneToManyTableDto.primaryKeyField.columnName>
                                <#--主键可以被用户输入-->
                                    <#if oneToManyTableDto.primaryKeyCreatePolicy=='INPUT'>
                                        <el-form-item
                                                <#if updateField.required>
                                                    prop="${updateField.columnName}"
                                                </#if>
                                                label="${updateField.columnComment}">
                                            <el-input
                                                    :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                    v-model="formData.${oneToManyTableDto.updateFieldList[updateField_index].columnName}"></el-input>
                                        </el-form-item>
                                    </#if>
                                <#elseif updateField.columnName!=oneToManyTableDto.joinTableFieldHump>
                                    <el-form-item
                                            <#if updateField.required>
                                                prop="${updateField.columnName}"
                                            </#if>
                                            label="${updateField.columnComment}">
                                        <#if updateField.joinDict!="">
                                            <el-select class="form-item-val"
                                                       :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                       clearable v-model="formData.${updateField.columnName}" placeholder="请选择${updateField.columnComment}">
                                                <el-option
                                                        v-for="item in ${updateField.columnName}DictList"
                                                        :key="item.value"
                                                        :label="item.label"
                                                        :value="item.value">
                                                </el-option>
                                            </el-select>
                                        <#--图片上传-->
                                        <#elseif updateField.fieldInputType=='IMAGE_UPLOAD'>
                                            <el-upload
                                                    :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                    :action="basApiUrl+'/fileUpload/localImg?field=${updateField.columnName}'"
                                                    accept=".jpg,.png,.gif"
                                                    :show-file-list="false"
                                                    class="common-img-uploader"
                                                    :on-success="handleUploadImgSuccess">
                                                <img v-if="formData.${updateField.columnName}" :src="formData.${updateField.columnName}" class="common-img-box">
                                                <i v-else class="el-icon-plus common-img-uploader-icon"></i>
                                            </el-upload>
                                        <#--富文本-->
                                        <#elseif updateField.fieldInputType=='RICH_TEXT'>
                                            <div id="${updateField.columnName}-rich-text-editor"></div>
                                        <#--普通输入字段-->
                                        <#else>
                                            <el-input
                                                    class="form-item-val"
                                                    :disabled="optionType==='see' || (optionType==='edit' && ${updateField.haveEdit? string('false','true')})"
                                                    v-model="formData.${oneToManyTableDto.updateFieldList[updateField_index].columnName}"></el-input>
                                        </#if>
                                    </el-form-item>
                                </#if>
                            </#if>
                        </#list>
            </el-form>
        </div>
        <span slot="footer" class="dialog-footer">
      <div v-if="optionType==='edit' || optionType==='add'">
        <el-button @click="close">取 消</el-button>
        <l-button auto-loading type="primary" @click="save">确 定</l-button>
      </div>
      <el-button v-else type="primary" @click="close">关 闭</el-button>
    </span>
    </el-dialog>
</template>

<script>
import { get${oneToManyTableDto.j_oinTable}ById, update${oneToManyTableDto.j_oinTable}ById, save${oneToManyTableDto.j_oinTable}Data } from '@/api/modules/${moduleName}/${tableName}Api'
import LButton from '@/components/loadingButton/LButton'
<#list oneToManyTableDto.updateFieldList as tableField>
<#if tableField.joinDict!="">
import {getSysDictDetaillistByDictLabel} from "@/api/common/dict";
<#break/>
</#if>
</#list>
<#-- 是否存在富文本编辑器 -->
<#if oneToManyTableDto.haveRichTextEditor>
import xss from 'xss'
import E from 'wangeditor'
</#if>

export default {
    name: 'Edit',
    components:{
        LButton
    },
    data() {
        return {
            optionType : '',
            dialogVisible: false,
            formData: {
                <#list oneToManyTableDto.updateFieldList as updateField>
                <#if updateField.fieldInputType!='CREATE_TIME' && updateField.fieldInputType!='UPDATE_TIME'>
                ${updateField.columnName}: null,
                </#if>
                </#list>
            },
            <#if oneToManyTableDto.haveImgField || oneToManyTableDto.haveRichTextEditor>
            basApiUrl:process.env.VUE_APP_BASE_API,
            </#if>
        <#list oneToManyTableDto.updateFieldList as updateField>
        <#if updateField.fieldInputType=='RICH_TEXT'>
            ${updateField.columnName}Editor:null,
        </#if>
        </#list>
            //主键
            optionId: null,
            mainTableId: null,
            <#if oneToManyTableDto.required>
            formDataRules:{
                <#list oneToManyTableDto.updateFieldList as updateField>
                <#if updateField.required>
                <#if updateField.fieldInputType=='INPUT'>
                ${updateField.columnName}: [{ required: true, message: '请输入${updateField.columnComment}', trigger: 'blur' }],
                <#else>
                ${updateField.columnName}: [{ required: true, message: '请设置${updateField.columnComment}', trigger: 'change' }],
                </#if>
                </#if>

                </#list>
            },
            </#if>
            /*字典表数据*/
            <#list oneToManyTableDto.updateFieldList as tableField>
            <#if tableField.joinDict!="">
            ${tableField.columnName}DictList: [],
            </#if>
            </#list>
        }
    },
    methods: {
        open(option) {
            this.optionType = option.optionType
            this.optionId = option.optionId
            this.mainTableId = option.mainTableId
            this.formData.${oneToManyTableDto.joinTableFieldHump} = this.mainTableId
            this.dialogVisible = true
            <#if oneToManyTableDto.haveRichTextEditor>
            this.$nextTick(()=>{
                <#list oneToManyTableDto.updateFieldList as updateField>
                <#if updateField.fieldInputType=='RICH_TEXT'>
                if(!this.${updateField.columnName}Editor){
                    let ${updateField.columnName}Edit = document.getElementById('${updateField.columnName}-rich-text-editor');
                    this.${updateField.columnName}Editor = new E(${updateField.columnName}Edit)
                    this.${updateField.columnName}Editor.config.uploadImgMaxLength = 1 // 一次最多上传 5 个图片
                    this.${updateField.columnName}Editor.config.uploadFileName = 'file'
                    this.${updateField.columnName}Editor.config.uploadImgServer = this.basApiUrl + '/fileUpload/localImg'
                    this.${updateField.columnName}Editor.config.uploadImgHooks ={
                        customInsert: function (insertImg, result, editor) {
                            let url = result.obj.serverUrl;
                            insertImg(url)
                        }
                    }
                    this.${updateField.columnName}Editor.create()
                    //是否禁用
                    if(this.optionType==='see' || (this.optionType==='edit' && ${updateField.haveEdit? string('false','true')})){
                        this.${updateField.columnName}Editor.disable()
                    }else{
                        this.${updateField.columnName}Editor.enable()
                    }
                }else{
                    //是否禁用
                    if(this.optionType==='see' || (this.optionType==='edit' && ${updateField.haveEdit? string('false','true')})){
                        this.${updateField.columnName}Editor.disable()
                    }else{
                        this.${updateField.columnName}Editor.enable()
                    }
                    this.${updateField.columnName}Editor.txt.clear()
                }
                </#if>
                </#list>

            })
            </#if>
            if(this.optionType !== 'add'){
                this.getByIdData()
            }
            <#list oneToManyTableDto.updateFieldList as tableField>
            <#if tableField.joinDict!="">
            getSysDictDetaillistByDictLabel({
                dictLabel:'${tableField.joinDict}'
            }).then(res=>{
                this.${tableField.columnName}DictList = res.rows
            })
            </#if>
            </#list>
        },
        close() {
            <#if oneToManyTableDto.required>
            this.$refs['formDataRef'].resetFields()
            </#if>
            this.$clearObj(this.formData)
            this.formData.${oneToManyTableDto.joinTableFieldHump} = this.mainTableId
            this.dialogVisible = false
        },
        save(next) {
            <#list oneToManyTableDto.updateFieldList as updateField>
            <#if updateField.fieldInputType=='RICH_TEXT'>
            const ${updateField.columnName}Html = this.${updateField.columnName}Editor.txt.html()
            const ${updateField.columnName}SafeHtml = xss(${updateField.columnName}Html)
            this.formData.${updateField.columnName} = ${updateField.columnName}SafeHtml
            </#if>
            </#list>
            <#if oneToManyTableDto.required>
            this.$refs['formDataRef'].validate((valid) => {
                if (valid) {
                    if(this.optionType === 'add') {
                        save${oneToManyTableDto.j_oinTable}Data(this.formData).then(res=>{
                            this.$message.success('添加成功')
                            this.close()
                            this.$emit('refreshPageData')
                        }).finally(()=>{
                            next()
                        })
                    }else if(this.optionType === 'edit') {
                        update${oneToManyTableDto.j_oinTable}ById(this.formData).then(res=>{
                            this.$message.success('修改成功')
                            this.close()
                            this.$emit('refreshPageData')
                        }).finally(()=>{
                            next()
                        })
                    }
                } else {
                    next()
                    console.log('error submit!!');
                    return false;
                }
            });
            </#if>

        },
        getByIdData() {
            get${oneToManyTableDto.j_oinTable}ById(this.optionId).then(res => {
                this.$clearObj(this.formData)
                <#list oneToManyTableDto.updateFieldList as updateField>
                this.formData.${updateField.columnName} = res.obj.${updateField.columnName}
            <#if updateField.fieldInputType=='RICH_TEXT'>
                let ${updateField.columnName} = this.formData.${updateField.columnName}
                this.${updateField.columnName}Editor.txt.html(${updateField.columnName})
            </#if>
            </#list>
            })
        },
        handleUploadImgSuccess(res, file) {
            if (res.obj) {
                this.$set(this.formData, res.obj.field, res.obj.serverUrl)
            }
        }
    }
}
</script>

<style scoped>

</style>
