package com.lzs.system;

import cn.afterturn.easypoi.cache.manager.POICacheManager;
import com.lzs.common.utils.easy.cache.FileLoaderImpl;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 监听项目启动
 * @author 梁子松
 */
@Component
public class MyApplicationListener implements ApplicationListener<ApplicationReadyEvent> {
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        //监听项目启动时，对POICacheManager进行全局替换
        POICacheManager.setFileLoader(new FileLoaderImpl());
    }
}
