package com.lzs.system.modules.system.bo;

import com.lzs.common.pojo.common.SysRole;
import com.lzs.system.modules.system.pojo.UserRole;
import lombok.Data;

@Data
public class UserRoleBo extends UserRole {
    private SysRole sysRole;
}
