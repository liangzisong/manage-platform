package com.lzs.system.modules.system.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("userRole(UserRole)")
@Data
public class UserRole {
   @ApiModelProperty("id")
   @TableId
    private String id;
   @ApiModelProperty("角色id")
    private String roleId;
   @ApiModelProperty("用户id")
    private String userId;
}
