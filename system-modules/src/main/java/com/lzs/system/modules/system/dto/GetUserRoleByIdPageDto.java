package com.lzs.system.modules.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetUserRoleByIdPageDto {

    @ApiModelProperty("一对多关系表连接字段")
    private String userId;

    @ApiModelProperty("roleId")
    private String roleId;


}
