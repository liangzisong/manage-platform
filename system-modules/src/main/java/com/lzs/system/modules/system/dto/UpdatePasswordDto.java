package com.lzs.system.modules.system.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdatePasswordDto {
    @NotBlank(message = "请输入旧密码")
    private String oldPassword;
    @NotBlank(message = "请输入新密码")
    private String newPassword;
}
