package com.lzs.system.modules.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("系统用户(SysUser)")
@Data
public class SysUser {
   @ApiModelProperty("id")
   @TableId
            (type = IdType.ASSIGN_UUID)
    private String id;
   @ApiModelProperty("用户名")
    private String username;
   @ApiModelProperty("密码")
    private String password;
   @ApiModelProperty("是否删除")
    private String delFlag;
}
