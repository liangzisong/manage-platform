package com.lzs.system.modules.system.controller;

import com.lzs.common.config.Constant;
import com.lzs.common.enums.LoginValidateType;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.system.modules.system.dto.*;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.system.modules.system.pojo.SysUser;
import com.lzs.system.modules.system.pojo.UserRole;
import com.lzs.system.modules.system.service.SysUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;

import java.util.Arrays;

/**
* 系统用户Controller
*/
@RestController
@RequestMapping("sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;


    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<SysUser> getByPage(PageQuery pageQuery,SysUserGetByPageDto dto){
        return sysUserService.getByPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<SysUser> getById(@PathVariable String id){
        return sysUserService.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody SysUserUpdateByIdDto dto){
        return sysUserService.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return sysUserService.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return sysUserService.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody SysUserSaveDto dto){
        return sysUserService.save(dto);
    }

    @ApiOperation("根据用户id查询系统角色数据")
    @GetMapping("getSysRoleByIdPage")
    public CommonResult<SysRole> getSysRoleByIdPage(PageQuery pageQuery
    , GetUserRoleByIdPageDto dto){
        return sysUserService.getSysRoleByIdPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询子表[userRole]")
    @GetMapping("getUserRoleById/{id}")
    public CommonResult<UserRole> getUserRoleById(@PathVariable String id){
        return sysUserService.getUserRoleById(id);
    }

    @ApiOperation("根据id修改子表[userRole]")
    @PutMapping("updateUserRoleById")
    public CommonResult updateUserRoleById(@RequestBody UserRoleUpdateByIdDto dto){
        return sysUserService.updateUserRoleById(dto);
    }

    @ApiOperation("根据id删除子表[userRole]")
    @DeleteMapping("deleteUserRoleById/{id}")
    public CommonResult deleteUserRoleById(@PathVariable String id){
        return sysUserService.deleteUserRoleById(id);
    }

    @ApiOperation("添加数据子表[userRole]")
    @PostMapping("saveUserRole")
    public CommonResult saveUserRole(@RequestBody UserRoleSaveDto dto){
        return sysUserService.saveUserRole(dto);
    }

    @ApiOperation("修改密码")
    @PostMapping("updatePassword")
    public CommonResult updatePassword(@RequestBody UpdatePasswordDto dto){
        BeanValidator.check(dto);
        return sysUserService.updatePassword(dto);
    }
}
