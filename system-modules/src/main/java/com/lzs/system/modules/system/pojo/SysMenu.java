package com.lzs.system.modules.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("系统菜单(SysMenu)")
@Data
public class SysMenu {
   @ApiModelProperty("id")
   @TableId
            (type = IdType.ASSIGN_UUID)
    private String id;
   @ApiModelProperty("权限名")
    private String menuName;
   @ApiModelProperty("权限码")
    private String menuCode;
   @ApiModelProperty("路径")
    private String vuePath;
   @ApiModelProperty("组件位置")
    private String vueComponent;
   @ApiModelProperty("组件名称")
    private String vueName;
   @ApiModelProperty("组件标题")
    private String vueTitle;
   @ApiModelProperty("组件图标")
    private String vueIcon;
   @ApiModelProperty("父级id")
    private String parentId;
   @ApiModelProperty("重定向地址")
    private String vueRedirect;
}
