package com.lzs.system.modules.system.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("roleMenu(RoleMenu)")
@Data
public class RoleMenu {
   @ApiModelProperty("id")
   @TableId
    private String id;
   @ApiModelProperty("角色id")
    private String roleId;
   @ApiModelProperty("权限id")
    private String menuId;
}
