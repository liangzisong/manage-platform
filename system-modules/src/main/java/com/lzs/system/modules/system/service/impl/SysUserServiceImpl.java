package com.lzs.system.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.exception.LzsBusinessException;
import com.lzs.common.thread.ThreadLocalUtil;
import com.lzs.common.utils.SecurityUserUtils;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.common.utils.valid.NumberValidator;
import com.lzs.common.dao.SysRoleDao;
import com.lzs.system.modules.system.dao.UserRoleDao;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.system.modules.system.pojo.UserRole;
import com.lzs.system.modules.system.dto.*;
import com.lzs.system.modules.system.service.SysUserService;
import com.lzs.common.vo.CommonResult;
import com.lzs.system.modules.system.dao.SysUserDao;
import com.lzs.system.modules.system.pojo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserDao,SysUser> implements SysUserService {

    @Autowired
    private UserRoleDao userRoleDao;
    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    public CommonResult<SysUser> getByPage(PageQuery pageQuery,SysUserGetByPageDto dto){
        baseMapper.getByPage(pageQuery.toPage(), dto);
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<SysUser> getById(String id){
        SysUser sysUser = baseMapper.selectById(id);
        return CommonResult.OK(sysUser);
    }

    @Override
    public CommonResult updateById(SysUserUpdateByIdDto dto){
        BeanValidator.check(dto,SysUserUpdateByIdDto.class);
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(dto,sysUser);
        //加密
        PasswordEncoder pw= new BCryptPasswordEncoder();
        String encode=pw.encode(sysUser.getPassword());
        sysUser.setPassword(encode);
        int updateRow = baseMapper.updateById(sysUser);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteById(String id){
        int deleteRow = baseMapper.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteFlagById(String id){
        Integer deleteFlagRow = baseMapper.deleteFlagById(id);
        NumberValidator.checkIsNotOne(deleteFlagRow,"禁用失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult save(SysUserSaveDto dto){
        BeanValidator.check(dto,SysUserUpdateByIdDto.class);
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(dto,sysUser);
        //加密
        PasswordEncoder pw= new BCryptPasswordEncoder();
        String encode=pw.encode(sysUser.getPassword());
        sysUser.setPassword(encode);
        Integer insertRow = baseMapper.insert(sysUser);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult<SysRole> getSysRoleByIdPage(PageQuery pageQuery
    , GetUserRoleByIdPageDto dto){
        sysRoleDao.selectPageByUserId(pageQuery.toPage(), dto.getUserId());
        CommonResult commonResult = pageQuery.toCommonResult();
        return commonResult;
    }

    @Override
    public CommonResult<UserRole> getUserRoleById(String id){
    UserRole userRole = userRoleDao.selectById(id);
        return CommonResult.OK(userRole);
    }

    @Override
    public CommonResult updateUserRoleById(UserRoleUpdateByIdDto dto){
        BeanValidator.check(dto,UserRoleUpdateByIdDto.class);
        UserRole userRole = new UserRole();
        BeanUtils.copyProperties(dto,userRole);
        int updateRow = userRoleDao.updateById(userRole);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteUserRoleById(String id){
        int deleteRow = userRoleDao.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult saveUserRole(UserRoleSaveDto dto){
    BeanValidator.check(dto,UserRoleUpdateByIdDto.class);
        UserRole userRole = new UserRole();
        BeanUtils.copyProperties(dto,userRole);
        Integer insertRow = userRoleDao.insert(userRole);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult updatePassword(UpdatePasswordDto dto) {
        String userId = SecurityUserUtils.getSecurityUser().getUserId();
        SysUser sysUser = baseMapper.selectById(userId);
        String oldPasswordDb = sysUser.getPassword();
        PasswordEncoder pw= new BCryptPasswordEncoder();
        //比较密码
        if (!pw.matches(dto.getOldPassword(),oldPasswordDb)) {
            throw new LzsBusinessException("旧密码输入错误");
        }
        String newPassword = pw.encode(dto.getNewPassword());
        //重设密码，修改
        sysUser.setPassword(newPassword);
        int updateRow = baseMapper.updateById(sysUser);
        NumberValidator.checkIsNotOne(updateRow,"修改密码失败");
        return CommonResult.OK();
    }
}
