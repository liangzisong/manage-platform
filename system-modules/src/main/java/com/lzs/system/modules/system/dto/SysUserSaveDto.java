package com.lzs.system.modules.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysUserSaveDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("username")
    private String username;
    @ApiModelProperty("password")
    private String password;
    @ApiModelProperty("delFlag")
    private String delFlag;

}
