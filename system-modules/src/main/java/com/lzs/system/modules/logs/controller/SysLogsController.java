package com.lzs.system.modules.logs.controller;

import com.lzs.common.annotations.Log;
import com.lzs.common.dto.logs.SysLogsGetByPageDto;
import com.lzs.common.enums.OperationType;
import com.lzs.system.modules.logs.dto.*;
import com.lzs.common.pojo.common.SysLogs;
import com.lzs.system.modules.logs.service.SysLogsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;

/**
* 系统日志Controller
*/
@RestController
@RequestMapping("sysLogs")
public class SysLogsController {

    @Autowired
    private SysLogsService sysLogsService;

    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<SysLogs> getByPage(PageQuery pageQuery, SysLogsGetByPageDto dto){
        return sysLogsService.getByPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<SysLogs> getById(@PathVariable String id){
        return sysLogsService.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody SysLogsUpdateByIdDto dto){
        return sysLogsService.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return sysLogsService.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return sysLogsService.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody SysLogsSaveDto dto){
        return sysLogsService.save(dto);
    }

}
