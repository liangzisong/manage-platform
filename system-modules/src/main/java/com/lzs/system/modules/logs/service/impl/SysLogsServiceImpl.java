package com.lzs.system.modules.logs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.logs.SysLogsGetByPageDto;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.common.utils.valid.NumberValidator;
import com.lzs.system.modules.logs.dto.*;
import com.lzs.system.modules.logs.service.SysLogsService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.dao.SysLogsDao;
import com.lzs.common.pojo.common.SysLogs;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class SysLogsServiceImpl extends ServiceImpl<SysLogsDao,SysLogs> implements SysLogsService {


    @Override
    public CommonResult<SysLogs> getByPage(PageQuery pageQuery, SysLogsGetByPageDto dto){
        baseMapper.getByPage(pageQuery.toPage(), dto);
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<SysLogs> getById(String id){
        SysLogs sysLogs = baseMapper.selectById(id);
        return CommonResult.OK(sysLogs);
    }

    @Override
    public CommonResult updateById(SysLogsUpdateByIdDto dto){
        BeanValidator.check(dto,SysLogsUpdateByIdDto.class);
        SysLogs sysLogs = new SysLogs();
        BeanUtils.copyProperties(dto,sysLogs);
        int updateRow = baseMapper.updateById(sysLogs);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteById(String id){
        int deleteRow = baseMapper.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteFlagById(String id){
        Integer deleteFlagRow = baseMapper.deleteFlagById(id);
        NumberValidator.checkIsNotOne(deleteFlagRow,"禁用失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult save(SysLogsSaveDto dto){
        BeanValidator.check(dto,SysLogsUpdateByIdDto.class);
        SysLogs sysLogs = new SysLogs();
        BeanUtils.copyProperties(dto,sysLogs);
        Integer insertRow = baseMapper.insert(sysLogs);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

}
