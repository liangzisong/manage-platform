package com.lzs.system.modules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.system.modules.system.pojo.SysUser;
import com.lzs.system.modules.system.dto.SysUserGetByPageDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserDao extends BaseMapper<SysUser> {
    /**
     * 分页查询
     */
    IPage<SysUser> getByPage(IPage page,@Param("dto")SysUserGetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);


}