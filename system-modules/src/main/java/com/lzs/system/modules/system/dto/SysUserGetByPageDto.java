package com.lzs.system.modules.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysUserGetByPageDto {

        @ApiModelProperty("id")
        private String id;
        @ApiModelProperty("username")
        private String username;
        @ApiModelProperty("delFlag")
        private String delFlag;

}
