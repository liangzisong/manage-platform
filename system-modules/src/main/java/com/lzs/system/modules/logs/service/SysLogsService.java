package com.lzs.system.modules.logs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.logs.SysLogsGetByPageDto;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.pojo.common.SysLogs;
import com.lzs.system.modules.logs.dto.*;

/**
 * 系统日志Service
 */
public interface SysLogsService extends IService<SysLogs> {

    CommonResult<SysLogs> getByPage(PageQuery pageQuery, SysLogsGetByPageDto dto);

    CommonResult<SysLogs> getById(String id);

    CommonResult updateById(SysLogsUpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(SysLogsSaveDto dto);


}
