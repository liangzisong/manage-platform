package com.lzs.system.modules.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserRoleUpdateByIdDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("roleId")
    private String roleId;
    @ApiModelProperty("userId")
    private String userId;

}
