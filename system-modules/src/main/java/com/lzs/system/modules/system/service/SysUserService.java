package com.lzs.system.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.system.modules.system.pojo.UserRole;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import com.lzs.system.modules.system.pojo.SysUser;
import com.lzs.system.modules.system.dto.*;

/**
 * 系统用户Service
 */
public interface SysUserService extends IService<SysUser> {

    CommonResult<SysUser> getByPage(PageQuery pageQuery,SysUserGetByPageDto dto);

    CommonResult<SysUser> getById(String id);

    CommonResult updateById(SysUserUpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(SysUserSaveDto dto);

    CommonResult<SysRole> getSysRoleByIdPage(PageQuery pageQuery
    , GetUserRoleByIdPageDto dto);

    CommonResult<UserRole> getUserRoleById(String id);

    CommonResult updateUserRoleById(UserRoleUpdateByIdDto dto);

    CommonResult deleteUserRoleById(String id);

    CommonResult saveUserRole(UserRoleSaveDto dto);

    CommonResult updatePassword(UpdatePasswordDto dto);
}
