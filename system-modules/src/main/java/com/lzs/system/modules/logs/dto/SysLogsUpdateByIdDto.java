package com.lzs.system.modules.logs.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SysLogsUpdateByIdDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("operationStatus")
    private String operationStatus;
    @ApiModelProperty("operationUserId")
    private String operationUserId;
    @ApiModelProperty("operationDescribe")
    private String operationDescribe;
    @ApiModelProperty("requestParams")
    private String requestParams;
    @ApiModelProperty("requestIp")
    private String requestIp;
    @ApiModelProperty("requestUrl")
    private String requestUrl;
    @ApiModelProperty("responseData")
    private String responseData;
    @ApiModelProperty("errMsg")
    private String errMsg;
    @ApiModelProperty("createTime")
    private Long createTime;

}
