package com.lzs.system.modules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzs.system.modules.system.pojo.UserRole;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleDao extends BaseMapper<UserRole> {

}