package com.lzs.system.common.dto;

import lombok.Data;

@Data
public class MsgDto {
    //转跳路径
    private String jumpPath;
    //标题
    private String title;
    //消息
    private String message;
    private String status = "200";
}
