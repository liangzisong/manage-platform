package com.lzs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@MapperScan(basePackages = {"com.lzs.common.dao","com.lzs.system.modules.*.dao"})
@SpringBootApplication
@ConfigurationPropertiesScan
@EnableSwagger2
public class ManagePlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagePlatformApplication.class, args);
    }

}
