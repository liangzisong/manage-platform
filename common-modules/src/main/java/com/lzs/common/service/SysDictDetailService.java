package com.lzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.vo.SelectVo;

import java.util.List;

/**
 * 系统字典详情Service
 */
public interface SysDictDetailService extends IService<SysDictDetail> {

    List<SelectVo> listByDictLabel(String dictLabel);
}
