package com.lzs.common.service.impl;

import com.lzs.common.dto.DelLocalImgDto;
import com.lzs.common.properties.FileProperties;
import com.lzs.common.properties.MyProjectProperties;
import com.lzs.common.service.FileUploadService;
import com.lzs.common.utils.UUIDUtils;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.RichTextLocalImgVo;
import com.lzs.common.vo.SaveImageToLocalVo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private MyProjectProperties myProjectProperties;

    @Override
    public CommonResult<SaveImageToLocalVo> saveImageToLocal(MultipartFile multipartFile,String field) {
        FileProperties file = myProjectProperties.getFile();
        String saveTo = file.getSaveToLocalUrl();
        String originalFilename = multipartFile.getOriginalFilename();
        String fileType = originalFilename.substring(originalFilename.lastIndexOf("."));
        String newFileName = UUIDUtils.generateUUID() + fileType;

        try {
            multipartFile.transferTo(new File(saveTo+newFileName));
        } catch (IOException e) {
            log.error("上传图片失败");
            e.printStackTrace();
        }
        String serverPath = file.getServerPrefixLocal() + newFileName;
        SaveImageToLocalVo saveImageToLocalVo = new SaveImageToLocalVo();
        saveImageToLocalVo.setServerUrl(serverPath);
        saveImageToLocalVo.setField(field);
        saveImageToLocalVo.setFileName(newFileName);
        return CommonResult.OK(saveImageToLocalVo);
    }

    @Override
    public CommonResult delLocalImg(DelLocalImgDto delLocalImgDto) {
        FileProperties file = myProjectProperties.getFile();
        String saveTo = file.getSaveToLocalUrl();
        new File(saveTo+delLocalImgDto.getFileName()).delete();
        return CommonResult.OK();
    }

    @Override
    public RichTextLocalImgVo richTextLocalImg(MultipartFile[] multipartFiles) {
        RichTextLocalImgVo richTextLocalImgVo = new RichTextLocalImgVo();
        richTextLocalImgVo.setErrno(0);
        List<RichTextLocalImgVo.RichTextLocalImgVoInfo> list = new ArrayList<>();
        for (MultipartFile multipartFile : multipartFiles) {
            CommonResult<SaveImageToLocalVo> saveImageToLocalVoCommonResult = this.saveImageToLocal(multipartFile, "");
            SaveImageToLocalVo saveImageToLocalVo = saveImageToLocalVoCommonResult.getObj();
            String serverUrl = saveImageToLocalVo.getServerUrl();
            RichTextLocalImgVo.RichTextLocalImgVoInfo richTextLocalImgVoInfo = new RichTextLocalImgVo.RichTextLocalImgVoInfo();
            richTextLocalImgVoInfo.setUrl(serverUrl);
            richTextLocalImgVoInfo.setAlt(saveImageToLocalVo.getFileName());
            richTextLocalImgVoInfo.setHref("");
            list.add(richTextLocalImgVoInfo);
        }
        richTextLocalImgVo.setData(list);
        return richTextLocalImgVo;
    }


}
