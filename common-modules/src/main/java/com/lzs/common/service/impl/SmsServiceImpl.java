package com.lzs.common.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsResponseBody;
import com.aliyun.teaopenapi.models.Config;
import com.lzs.common.redis.CommonRedisKeyPrefix;
import com.lzs.common.service.SmsService;
import com.lzs.common.vo.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class SmsServiceImpl implements SmsService {
    @Value("${aliyun.sms.signName:你的signName}")
    private String signName;
    @Value("${aliyun.sms.templateCode:你的模版代码}")
    private String templateCode;
    @Value("${aliyun.sms.accessKeyId:你的accessKeyId}")
    private String accessKeyId;
    @Value("${aliyun.sms.accessSecret:你的accessSecret}")
    private String accessSecret;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public CommonResult<String> sendLoginSms(String phoneNo){
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String smsCode = RandomStringUtils.randomNumeric(6);
        log.info("手机号:{},登录验证码:{}",phoneNo,smsCode);
        if ("13211111111".equals(phoneNo)){
            smsCode = "1234";
            valueOperations.set(CommonRedisKeyPrefix.SEND_LOGIN_SMS+phoneNo,smsCode,10, TimeUnit.MINUTES);
            return CommonResult.OK();
        }
        valueOperations.set(CommonRedisKeyPrefix.SEND_LOGIN_SMS+phoneNo,smsCode,10, TimeUnit.MINUTES);
        return sendSms2(phoneNo,smsCode);
    }
    public CommonResult<String> sendSms2(String phoneNo, String validateCode){
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessSecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";

        try {
            Client client = new Client(config);
            SendSmsRequest sendSmsRequest = new SendSmsRequest();
            sendSmsRequest.setPhoneNumbers(phoneNo);
            sendSmsRequest.setSignName(signName);
            sendSmsRequest.setTemplateCode(templateCode);
            Map<String,String> map = new HashMap<>();
            map.put("code",validateCode);
            sendSmsRequest.setTemplateParam(JSONObject.toJSONString(map));
            SendSmsResponse smsResponse = client.sendSms(sendSmsRequest);
            log.info("发送短信返回结果:{}", JSONObject.toJSONString(smsResponse.getBody()));
            SendSmsResponseBody body = smsResponse.getBody();
            String code = body.code;
            if (!"OK".equals(code)){
                //发送失败
                return CommonResult.Err("短信发送失败");
            }
        } catch (Exception e) {
            log.error("发送短信异常",e);
            return CommonResult.Err("网络异常");
        }
        return CommonResult.OK();
    }
}
