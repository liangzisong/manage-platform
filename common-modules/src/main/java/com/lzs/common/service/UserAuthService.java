package com.lzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.pojo.common.SysUser;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.GetUserPowerVo;

public interface UserAuthService extends IService<SysUser> {
    /**
     * 获取用户权限
     * @param userId
     * @return
     */
    GetUserPowerVo getUserPower(String userId);

    CommonResult getInfo();
}
