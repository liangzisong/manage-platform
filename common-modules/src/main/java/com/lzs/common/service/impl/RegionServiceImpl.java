package com.lzs.common.service.impl;

import com.lzs.common.service.RegionService;
import com.lzs.common.utils.RegionUtils;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SelectVo;
import org.springframework.stereotype.Service;

@Service
public class RegionServiceImpl implements RegionService {
    @Override
    public CommonResult<SelectVo> getAllRegionData() {
        return CommonResult.rows(RegionUtils.regionList);
    }
}
