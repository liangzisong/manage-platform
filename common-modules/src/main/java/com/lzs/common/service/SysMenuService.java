package com.lzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.menu.*;
import com.lzs.common.pojo.common.SysMenu;
import com.lzs.common.pojo.common.bo.SysMenuBo;
import com.lzs.common.vo.CommonResult;

/**
 * Service
 */
public interface SysMenuService extends IService<SysMenu> {

    public CommonResult<SysMenu> getByPage(PageQuery pageQuery, SysMenuGetByPageDto dto);

    CommonResult<SysMenu> getById(String id);

    CommonResult updateById(SysMenuUpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(SysMenuSaveDto dto);

    CommonResult getSysMenuByIdPage(PageQuery pageQuery
            , GetSysMenuByIdPageDto dto);

    CommonResult<SysMenu> getSysMenuById(String id);

    CommonResult updateSysMenuById(SysMenuUpdateByIdDto dto);

    CommonResult deleteSysMenuById(String id);

    CommonResult saveSysMenu(SysMenuSaveDto dto);

    CommonResult getTreeMenuByRoleId(String roleId);

    /**
     * 查询树状下拉
     * @return
     */
    CommonResult getTreeSelect();

    /**
     * 根据角色id查询角色权限
     * @param roleId
     * @return
     */
    CommonResult getTreeSelectValueByRoleId(String roleId);

    /**
     * 分页查询树状结构
     * @param pageQuery
     * @param dto
     * @return
     */
    CommonResult<SysMenuBo> getListByPage(PageQuery pageQuery, GetThreeByPageDto dto);

    CommonResult<SysMenuBo> getList(GetThreeByPageDto dto);
}
