package com.lzs.common.service;

import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SelectVo;

public interface RegionService {
    /**
     * 获取所有区域数据
     * @return
     */
    CommonResult<SelectVo> getAllRegionData();
}
