package com.lzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.dto.role.*;
import com.lzs.common.pojo.common.RoleMenu;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.vo.SelectVo;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;

/**
 * 系统角色Service
 */
public interface SysRoleService extends IService<SysRole> {

    CommonResult<SysRole> getByPage(PageQuery pageQuery, SysRoleGetByPageDto dto);

    CommonResult<SysRole> getById(String id);

    CommonResult updateById(SysRoleUpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(SysRoleSaveDto dto);

    CommonResult getRoleMenuByIdPage(PageQuery pageQuery
    , GetRoleMenuByIdPageDto dto);

    CommonResult<RoleMenu> getRoleMenuById(String id);

    CommonResult updateRoleMenuById(RoleMenuUpdateByIdDto dto);

    CommonResult deleteRoleMenuById(String id);

    CommonResult saveRoleMenu(RoleMenuSaveDto dto);

    /**
     * 查询角色下拉
     * @return
     */
    CommonResult<SelectVo> getRoleSelect();
}
