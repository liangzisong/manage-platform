package com.lzs.common.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.dict.*;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.common.utils.valid.NumberValidator;
import com.lzs.common.dao.SysDictDetailDao;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.service.SysDictService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.dao.SysDictDao;
import com.lzs.common.pojo.common.SysDict;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictDao,SysDict> implements SysDictService {

        @Autowired
        private SysDictDetailDao sysDictDetailDao;

    @Override
    public CommonResult<SysDict> getByPage(PageQuery pageQuery, SysDictGetByPageDto dto){
        baseMapper.getByPage(pageQuery.toPage(), dto);
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<SysDict> getById(String id){
        SysDict sysDict = baseMapper.selectById(id);
        return CommonResult.OK(sysDict);
    }

    @Override
    public CommonResult updateById(SysDictUpdateByIdDto dto){
        BeanValidator.check(dto,SysDictUpdateByIdDto.class);
        SysDict sysDict = new SysDict();
        BeanUtils.copyProperties(dto,sysDict);
        int updateRow = baseMapper.updateById(sysDict);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteById(String id){
        int deleteRow = baseMapper.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteFlagById(String id){
        Integer deleteFlagRow = baseMapper.deleteFlagById(id);
        NumberValidator.checkIsNotOne(deleteFlagRow,"禁用失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult save(SysDictSaveDto dto){
        BeanValidator.check(dto,SysDictUpdateByIdDto.class);
        SysDict sysDict = new SysDict();
        BeanUtils.copyProperties(dto,sysDict);
        Integer insertRow = baseMapper.insert(sysDict);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult getSysDictDetailByIdPage(PageQuery pageQuery
    , GetSysDictDetailByIdPageDto dto){
        sysDictDetailDao.selectPage(pageQuery.toPage(), Wrappers.<SysDictDetail>lambdaQuery()
    .select()
    .eq(SysDictDetail::getSysDictId,dto.getSysDictId())
        .eq(
            StringUtils.isNotBlank
            (dto.getId()),SysDictDetail::getId,dto.getId())

        .like(
            StringUtils.isNotBlank
            (dto.getDictionaryLabel()),SysDictDetail::getDictionaryLabel,dto.getDictionaryLabel())

        .like(
            StringUtils.isNotBlank
            (dto.getDictionaryValue()),SysDictDetail::getDictionaryValue,dto.getDictionaryValue())

        .eq(
            StringUtils.isNotBlank
            (dto.getSysDictId()),SysDictDetail::getSysDictId,dto.getSysDictId())

    );
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<SysDictDetail> getSysDictDetailById(String id){
    SysDictDetail sysDictDetail = sysDictDetailDao.selectById(id);
        return CommonResult.OK(sysDictDetail);
    }

    @Override
    public CommonResult updateSysDictDetailById(SysDictDetailUpdateByIdDto dto){
        BeanValidator.check(dto,SysDictDetailUpdateByIdDto.class);
        SysDictDetail sysDictDetail = new SysDictDetail();
        BeanUtils.copyProperties(dto,sysDictDetail);
        int updateRow = sysDictDetailDao.updateById(sysDictDetail);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteSysDictDetailById(String id){
        int deleteRow = sysDictDetailDao.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult saveSysDictDetail(SysDictDetailSaveDto dto){
    BeanValidator.check(dto,SysDictDetailUpdateByIdDto.class);
        SysDictDetail sysDictDetail = new SysDictDetail();
        BeanUtils.copyProperties(dto,sysDictDetail);
        Integer insertRow = sysDictDetailDao.insert(sysDictDetail);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }
}
