package com.lzs.common.service;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.GenerateCodeDto;
import com.lzs.common.pojo.common.DbTable;
import com.lzs.common.pojo.common.bo.DbFieldBo;
import com.lzs.common.vo.CommonResult;

public interface CodeGenerateService {
    /**
     * 查询所有数据库表
     * @return
     */
    CommonResult<DbTable> tableByPage(PageQuery pageQuery, String tableName);

    CommonResult<DbFieldBo> fieldByTable(String tableName);

    /**
     * 生成代码
     * @param generateCodeDto
     * @return
     */
    CommonResult generateCode(GenerateCodeDto generateCodeDto);
}
