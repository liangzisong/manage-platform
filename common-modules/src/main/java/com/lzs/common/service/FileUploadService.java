package com.lzs.common.service;

import com.lzs.common.dto.DelLocalImgDto;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.RichTextLocalImgVo;
import com.lzs.common.vo.SaveImageToLocalVo;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    /**
     * 上传图片到本地
     * @return
     */
    CommonResult<SaveImageToLocalVo> saveImageToLocal(MultipartFile multipartFile, String field);

    /**
     * 删除图片
     * @param delLocalImgDto
     * @return
     */
    CommonResult<String> delLocalImg(DelLocalImgDto delLocalImgDto);

    RichTextLocalImgVo richTextLocalImg(MultipartFile[] multipartFiles);
}
