package com.lzs.common.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dao.SysDictDetailDao;
import com.lzs.common.pojo.common.SysDict;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.service.SysDictDetailService;
import com.lzs.common.service.SysDictService;
import com.lzs.common.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysDictDetailServiceImpl extends ServiceImpl<SysDictDetailDao,SysDictDetail> implements SysDictDetailService {

    @Autowired
    private SysDictService sysDictService;

    @Override
    public List<SelectVo> listByDictLabel(String dictLabel) {
        SysDict sysDict = sysDictService.getOne(Wrappers.<SysDict>lambdaQuery()
                .select(SysDict::getId)
                .eq(SysDict::getDictionaryName, dictLabel));
        List<SelectVo> selectVoList = baseMapper.selectList(Wrappers.<SysDictDetail>lambdaQuery()
                .select()
                .eq(SysDictDetail::getSysDictId,sysDict.getId())).stream().map(d -> {
            return SelectVo.builder().label(d.getDictionaryLabel()).value(d.getDictionaryValue()).build();
        }).collect(Collectors.toList());
        return selectVoList;
    }
}
