package com.lzs.common.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dao.SysRoleDao;
import com.lzs.common.dao.UserAuthDao;
import com.lzs.common.exception.LzsBusinessException;
import com.lzs.common.pojo.common.SysMenu;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.pojo.common.SysUser;
import com.lzs.common.redis.CommonRedisKeyPrefix;
import com.lzs.common.redis.RedisUserInfo;
import com.lzs.common.service.SysMenuService;
import com.lzs.common.service.UserAuthService;
import com.lzs.common.utils.SecurityUser;
import com.lzs.common.utils.SecurityUserUtils;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.GetInfoVo;
import com.lzs.common.vo.GetUserPowerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class UserAuthServiceImpl extends ServiceImpl<UserAuthDao, SysUser> implements UserAuthService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserAuthDao userAuthDao;
    @Autowired
    private SysRoleDao sysRoleDao;
    @Autowired
    private SysMenuService sysMenuService;

    @Override
    public GetUserPowerVo getUserPower(String userId) {
        GetUserPowerVo getUserPowerVo = new GetUserPowerVo();
        String userName = "";
        //获取缓存
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Object userInfoObj = valueOperations.get(CommonRedisKeyPrefix.USER_INFO + userId);
        //redis有缓存
        if(Objects.nonNull(userInfoObj)){
            RedisUserInfo redisUserInfo = JSONObject.parseObject(userInfoObj.toString(), RedisUserInfo.class);
            List<String> roleNameList = redisUserInfo.getRoleNameList();
            List<String> menuCodeList = redisUserInfo.getMenuCodeList();
            userName = redisUserInfo.getUsername();
            getUserPowerVo.setUserName(userName);
            getUserPowerVo.setRoleNameList(roleNameList);
            getUserPowerVo.setMenuCodeList(menuCodeList);
            //缓存过期
        }else {
            SysUser sysUser = userAuthDao.selectById(userId);
            if (sysUser == null) {
                throw new LzsBusinessException("用户不存在");
            }
            userName = sysUser.getUsername();
            getUserPowerVo.setUserName(userName);

            RedisUserInfo redisUserInfo = new RedisUserInfo();
            redisUserInfo.setId(sysUser.getId());
            redisUserInfo.setUsername(sysUser.getUsername());
            List<String> roleNameList = new ArrayList<>();
            List<String> menuCodeList = new ArrayList<>();
            //根据用户id查询角色和权限
            List<SysRole> sysRoleList = sysRoleDao.selectListByUserId(sysUser.getId());
            if(Objects.nonNull(sysRoleList)){
                //遍历设置用户角色
                for (SysRole sysRole : sysRoleList) {
                    roleNameList.add(sysRole.getRoleName());
                    //根据用户角色设置角色权限
                    List<SysMenu> menuList = sysMenuService.getTreeMenuByRoleId(sysRole.getId()).getRows();
                    for (SysMenu sysMenu : menuList) {
                        menuCodeList.add(sysMenu.getMenuCode());
                    }
                }
            }

            redisUserInfo.setRoleNameList(roleNameList);
            redisUserInfo.setMenuCodeList(menuCodeList);
            //缓存用户信息30分钟
            String redisUserInfoJson = JSONObject.toJSONString(redisUserInfo);
            valueOperations.set(CommonRedisKeyPrefix.USER_INFO+sysUser.getId(),redisUserInfoJson,30, TimeUnit.MINUTES);

            getUserPowerVo.setRoleNameList(roleNameList);
            getUserPowerVo.setMenuCodeList(menuCodeList);
        }
        return getUserPowerVo;
    }

    @Override
    public CommonResult getInfo() {
        List<SysMenu> menuAllList = new ArrayList<>();
        SecurityUser securityUser = SecurityUserUtils.getSecurityUser();
        List<SysRole> sysRoleList = sysRoleDao.selectListByUserId(securityUser.getUserId());
        if(Objects.nonNull(sysRoleList)){
            //遍历设置用户角色
            for (SysRole sysRole : sysRoleList) {
                //根据用户角色设置角色权限
                List<SysMenu> menuList = sysMenuService.getTreeMenuByRoleId(sysRole.getId()).getRows();
                menuAllList.addAll(menuList);
            }
        }
        GetInfoVo getInfoVo = new GetInfoVo();
        getInfoVo.setRoles(menuAllList);
        getInfoVo.setIntroduction("I am a super administrator");
        getInfoVo.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        getInfoVo.setName(securityUser.getUsername());
        getInfoVo.setId(securityUser.getUserId());
        return CommonResult.OK(getInfoVo);
    }


}
