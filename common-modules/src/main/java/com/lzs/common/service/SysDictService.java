package com.lzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzs.common.dto.dict.*;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.pojo.common.SysDict;

/**
 * 系统字典Service
 */
public interface SysDictService extends IService<SysDict> {

    CommonResult<SysDict> getByPage(PageQuery pageQuery, SysDictGetByPageDto dto);

    CommonResult<SysDict> getById(String id);

    CommonResult updateById(SysDictUpdateByIdDto dto);

    CommonResult deleteById(String id);

    CommonResult deleteFlagById(String id);

    CommonResult save(SysDictSaveDto dto);

    CommonResult getSysDictDetailByIdPage(PageQuery pageQuery
    , GetSysDictDetailByIdPageDto dto);

    CommonResult<SysDictDetail> getSysDictDetailById(String id);

    CommonResult updateSysDictDetailById(SysDictDetailUpdateByIdDto dto);

    CommonResult deleteSysDictDetailById(String id);

    CommonResult saveSysDictDetail(SysDictDetailSaveDto dto);

}
