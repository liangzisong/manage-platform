package com.lzs.common.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzs.common.dao.RoleMenuDao;
import com.lzs.common.dao.SysRoleDao;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.role.*;
import com.lzs.common.pojo.common.RoleMenu;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.utils.valid.BeanValidator;
import com.lzs.common.utils.valid.NumberValidator;
import com.lzs.common.vo.SelectVo;
import com.lzs.common.service.SysRoleService;
import com.lzs.common.vo.CommonResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRole> implements SysRoleService {

        @Autowired
        private RoleMenuDao roleMenuDao;

    @Override
    public CommonResult<SysRole> getByPage(PageQuery pageQuery, SysRoleGetByPageDto dto){
        baseMapper.getByPage(pageQuery.toPage(), dto);
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<SysRole> getById(String id){
        SysRole sysRole = baseMapper.selectById(id);
        return CommonResult.OK(sysRole);
    }

    @Override
    public CommonResult updateById(SysRoleUpdateByIdDto dto){
        BeanValidator.check(dto,SysRoleUpdateByIdDto.class);
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(dto,sysRole);
        int updateRow = baseMapper.updateById(sysRole);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        Set<String> menuIdSet = dto.getMenuIdSet();
        //删除之前的权限
        updateRow = roleMenuDao.delete(Wrappers.<RoleMenu>lambdaUpdate().eq(RoleMenu::getRoleId, dto.getId()));
        //添加新的权限
        for (String menuId : menuIdSet) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(sysRole.getId());
            roleMenu.setMenuId(menuId);
            updateRow = roleMenuDao.insert(roleMenu);
            NumberValidator.checkIsNotOne(updateRow,"修改失败");
        }
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteById(String id){
        int deleteRow = baseMapper.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteFlagById(String id){
        Integer deleteFlagRow = baseMapper.deleteFlagById(id);
        NumberValidator.checkIsNotOne(deleteFlagRow,"禁用失败");
        return CommonResult.OK();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommonResult save(SysRoleSaveDto dto){
        BeanValidator.check(dto,SysRoleUpdateByIdDto.class);
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(dto,sysRole);
        Integer insertRow = baseMapper.insert(sysRole);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        Set<String> menuIdSet = dto.getMenuIdSet();
        //删除之前的权限
        //添加新的权限
        for (String menuId : menuIdSet) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(sysRole.getId());
            roleMenu.setMenuId(menuId);
            insertRow = roleMenuDao.insert(roleMenu);
            NumberValidator.checkIsNotOne(insertRow,"添加失败");
        }
        return CommonResult.OK();
    }

    @Override
    public CommonResult getRoleMenuByIdPage(PageQuery pageQuery
    , GetRoleMenuByIdPageDto dto){
        roleMenuDao.selectPage(pageQuery.toPage(), Wrappers.<RoleMenu>lambdaQuery()
    .select()
    .eq(RoleMenu::getRoleId,dto.getRoleId())
            .eq(
            StringUtils.isNotBlank
            (dto.getMenuId()),RoleMenu::getMenuId,dto.getMenuId())

    );
        return pageQuery.toCommonResult();
    }

    @Override
    public CommonResult<RoleMenu> getRoleMenuById(String id){
    RoleMenu roleMenu = roleMenuDao.selectById(id);
        return CommonResult.OK(roleMenu);
    }

    @Override
    public CommonResult updateRoleMenuById(RoleMenuUpdateByIdDto dto){
        BeanValidator.check(dto,RoleMenuUpdateByIdDto.class);
        RoleMenu roleMenu = new RoleMenu();
        BeanUtils.copyProperties(dto,roleMenu);
        int updateRow = roleMenuDao.updateById(roleMenu);
        NumberValidator.checkIsNotOne(updateRow,"修改失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult deleteRoleMenuById(String id){
        int deleteRow = roleMenuDao.deleteById(id);
        NumberValidator.checkIsNotOne(deleteRow,"删除失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult saveRoleMenu(RoleMenuSaveDto dto){
    BeanValidator.check(dto,RoleMenuUpdateByIdDto.class);
        RoleMenu roleMenu = new RoleMenu();
        BeanUtils.copyProperties(dto,roleMenu);
        Integer insertRow = roleMenuDao.insert(roleMenu);
        NumberValidator.checkIsNotOne(insertRow,"添加失败");
        return CommonResult.OK();
    }

    @Override
    public CommonResult<SelectVo> getRoleSelect() {
        List<SysRole> sysRoleList = baseMapper.selectList(null);
        List<SelectVo> selectVoList = sysRoleList.stream()
                .map(r -> SelectVo.builder()
                        .label(r.getRoleName())
                        .value(r.getId()).build())
                .collect(Collectors.toList());
        return CommonResult.rows(selectVoList);
    }


}
