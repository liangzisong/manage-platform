package com.lzs.common.service;

import com.lzs.common.vo.CommonResult;

public interface SmsService {
    CommonResult<String> sendLoginSms(String phoneNo);
}
