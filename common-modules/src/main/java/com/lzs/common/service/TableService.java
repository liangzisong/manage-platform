package com.lzs.common.service;

import com.lzs.common.dto.table.UpdateByIdDto;
import com.lzs.common.pojo.common.DbTable;
import com.lzs.common.vo.CommonResult;

public interface TableService {
    /**
     * 根据表名称删除
     * @param tableName
     * @return
     */
    CommonResult deleteByTableName(String tableName);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    CommonResult<DbTable> getById(String id);

    /**
     * 根据id修改
     * @param dto
     * @return
     */
    CommonResult updateById(UpdateByIdDto dto);

    /**
     * 查询所有表
     * @return
     */
    CommonResult getAllTable();
}
