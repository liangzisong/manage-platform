package com.lzs.common.controller;

import com.lzs.common.dto.role.*;
import com.lzs.common.pojo.common.RoleMenu;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.service.SysRoleService;
import com.lzs.common.vo.SelectVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import springfox.documentation.annotations.ApiIgnore;

/**
* 系统角色Controller
*/
@RestController
@RequestMapping("sysRole")
@ApiIgnore
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<SysRole> getByPage(PageQuery pageQuery, SysRoleGetByPageDto dto){
        return sysRoleService.getByPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<SysRole> getById(@PathVariable String id){
        return sysRoleService.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody SysRoleUpdateByIdDto dto){
        return sysRoleService.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return sysRoleService.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return sysRoleService.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody SysRoleSaveDto dto){
        return sysRoleService.save(dto);
    }

    @ApiOperation("查询子表[roleMenu]数据")
    @GetMapping("getRoleMenuByIdPage")
    public CommonResult<RoleMenu> getRoleMenuByIdPage(PageQuery pageQuery
    , GetRoleMenuByIdPageDto dto){
        return sysRoleService.getRoleMenuByIdPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询子表[roleMenu]")
    @GetMapping("getRoleMenuById/{id}")
    public CommonResult<RoleMenu> getRoleMenuById(@PathVariable String id){
        return sysRoleService.getRoleMenuById(id);
    }

    @ApiOperation("根据id修改子表[roleMenu]")
    @PutMapping("updateRoleMenuById")
    public CommonResult updateRoleMenuById(@RequestBody RoleMenuUpdateByIdDto dto){
        return sysRoleService.updateRoleMenuById(dto);
    }

    @ApiOperation("根据id删除子表[roleMenu]")
    @DeleteMapping("deleteRoleMenuById/{id}")
    public CommonResult deleteRoleMenuById(@PathVariable String id){
        return sysRoleService.deleteRoleMenuById(id);
    }

    @ApiOperation("添加数据子表[roleMenu]")
    @PostMapping("saveRoleMenu")
    public CommonResult saveRoleMenu(@RequestBody RoleMenuSaveDto dto){
        return sysRoleService.saveRoleMenu(dto);
    }

    @ApiOperation("获取角色下拉")
    @GetMapping("getRoleSelect")
    public CommonResult<SelectVo> getRoleSelect(){
        return sysRoleService.getRoleSelect();
    }
}
