package com.lzs.common.controller;

import com.lzs.common.dto.dict.*;
import com.lzs.common.pojo.common.SysDict;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.service.SysDictService;
import com.lzs.common.vo.SelectVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.stream.Collectors;

/**
* 系统字典Controller
*/
@RestController
@RequestMapping("sysDict")
@ApiIgnore
public class SysDictController {

    @Autowired
    private SysDictService sysDictService;

    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<SysDict> getByPage(PageQuery pageQuery, SysDictGetByPageDto dto){
        return sysDictService.getByPage(pageQuery,dto);
    }

    @ApiOperation("查询所有")
    @GetMapping("list")
    public CommonResult<SelectVo> list(){
        List<SelectVo> selectVoList = sysDictService.list().stream().map(d -> {
            return SelectVo.builder().label(d.getDictionaryDescribe()).value(d.getDictionaryName()).build();
        }).collect(Collectors.toList());
        return CommonResult.rows(selectVoList);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<SysDict> getById(@PathVariable String id){
        return sysDictService.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody SysDictUpdateByIdDto dto){
        return sysDictService.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return sysDictService.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return sysDictService.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody SysDictSaveDto dto){
        return sysDictService.save(dto);
    }

    @ApiOperation("查询子表[系统字典详情]数据")
    @GetMapping("getSysDictDetailByIdPage")
    public CommonResult<SysDictDetail> getSysDictDetailByIdPage(PageQuery pageQuery
    , GetSysDictDetailByIdPageDto dto){
        return sysDictService.getSysDictDetailByIdPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询子表[系统字典详情]")
    @GetMapping("getSysDictDetailById/{id}")
    public CommonResult<SysDictDetail> getSysDictDetailById(@PathVariable String id){
        return sysDictService.getSysDictDetailById(id);
    }

    @ApiOperation("根据id修改子表[系统字典详情]")
    @PutMapping("updateSysDictDetailById")
    public CommonResult updateSysDictDetailById(@RequestBody SysDictDetailUpdateByIdDto dto){
        return sysDictService.updateSysDictDetailById(dto);
    }

    @ApiOperation("根据id删除子表[系统字典详情]")
    @DeleteMapping("deleteSysDictDetailById/{id}")
    public CommonResult deleteSysDictDetailById(@PathVariable String id){
        return sysDictService.deleteSysDictDetailById(id);
    }

    @ApiOperation("添加数据子表[系统字典详情]")
    @PostMapping("saveSysDictDetail")
    public CommonResult saveSysDictDetail(@RequestBody SysDictDetailSaveDto dto){
        return sysDictService.saveSysDictDetail(dto);
    }
}
