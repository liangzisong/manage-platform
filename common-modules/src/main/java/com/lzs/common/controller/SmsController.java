package com.lzs.common.controller;

import com.lzs.common.service.SmsService;
import com.lzs.common.vo.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("sms")
@Api(tags = "APP-发送验证码")
public class SmsController {
    @Autowired
    private SmsService smsService;
    @ApiOperation("发送验证码")
    @GetMapping("sendSms")
    @ApiImplicitParam(name = "phoneNo",value = "测试手机号[13211111111]测试验证码[1234]",paramType = "query")
    public CommonResult<String> sendSms(String phoneNo){
        return smsService.sendLoginSms(phoneNo);
    }
}
