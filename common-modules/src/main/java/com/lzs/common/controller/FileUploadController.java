package com.lzs.common.controller;

import com.lzs.common.dto.DelLocalImgDto;
import com.lzs.common.service.FileUploadService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SaveImageToLocalVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("fileUpload")
@Api(tags = "上传文件控制器")
public class FileUploadController {
    @Autowired
    private FileUploadService fileUploadService;

    @ApiOperation("上传图片到本地")
    @PostMapping("localImg")
    public CommonResult<SaveImageToLocalVo> localImg(@RequestParam("file") MultipartFile multipartFile, String field){
        return fileUploadService.saveImageToLocal(multipartFile,field);
    }

//    @ApiOperation("富文本上传图片到本地")
//    @PostMapping("richTextLocalImg")
//    public RichTextLocalImgVo richTextLocalImg(@RequestParam("files") MultipartFile[] multipartFiles){
//        return fileUploadService.richTextLocalImg(multipartFiles);
//    }

    @ApiOperation("删除图片")
    @DeleteMapping("delLocalImg")
    public CommonResult delLocalImg(@RequestBody DelLocalImgDto delLocalImgDto){
        return fileUploadService.delLocalImg(delLocalImgDto);
    }
}
