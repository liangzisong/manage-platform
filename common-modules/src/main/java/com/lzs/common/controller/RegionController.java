package com.lzs.common.controller;

import com.lzs.common.service.RegionService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SelectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "区域控制器")
@RestController
@RequestMapping("region")
public class RegionController {

    @Autowired
    private RegionService regionService;

    @GetMapping("getAllRegionData")
    @ApiOperation("获取所有区域信息")
    public CommonResult<SelectVo> getAllRegionData(){
        return regionService.getAllRegionData();
    }
}
