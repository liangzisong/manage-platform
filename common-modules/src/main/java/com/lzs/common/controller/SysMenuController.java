package com.lzs.common.controller;

import com.lzs.common.dto.menu.*;
import com.lzs.common.pojo.common.SysMenu;
import com.lzs.common.pojo.common.bo.SysMenuBo;
import com.lzs.common.service.SysMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.vo.CommonResult;
import springfox.documentation.annotations.ApiIgnore;

/**
* 菜单管理Controller
*/
@RestController
@RequestMapping("sysMenu")
@ApiIgnore
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    @ApiOperation("分页查询")
    @GetMapping("getByPage")
    public CommonResult<SysMenu> getByPage(PageQuery pageQuery, SysMenuGetByPageDto dto){
        return sysMenuService.getByPage(pageQuery,dto);
    }

    @ApiOperation("分页查询列表")
    @GetMapping("getListByPage")
    public CommonResult<SysMenuBo> getListByPage(PageQuery pageQuery, GetThreeByPageDto dto){
        return sysMenuService.getListByPage(pageQuery,dto);
    }

    @ApiOperation("查询列表")
    @GetMapping("getList")
    public CommonResult<SysMenuBo> getList(GetThreeByPageDto dto){
        return sysMenuService.getList(dto);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<SysMenu> getById(@PathVariable String id){
        return sysMenuService.getById(id);
    }

    @ApiOperation("根据id修改")
    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody SysMenuUpdateByIdDto dto){
        return sysMenuService.updateById(dto);
    }

    @ApiOperation("根据id删除")
    @DeleteMapping("deleteById/{id}")
    public CommonResult deleteById(@PathVariable String id){
        return sysMenuService.deleteById(id);
    }

    @ApiOperation("根据id禁用")
    @PutMapping("deleteFlagById/{id}")
    public CommonResult deleteFlagById(@PathVariable String id){
        return sysMenuService.deleteFlagById(id);
    }

    @ApiOperation("添加数据")
    @PostMapping("save")
    public CommonResult save(@RequestBody SysMenuSaveDto dto){
        return sysMenuService.save(dto);
    }

    @ApiOperation("查询子表[sysMenu]数据")
    @GetMapping("getSysMenuByIdPage")
    public CommonResult<SysMenu> getSysMenuByIdPage(PageQuery pageQuery
            , GetSysMenuByIdPageDto dto){
        return sysMenuService.getSysMenuByIdPage(pageQuery,dto);
    }

    @ApiOperation("根据id查询子表[sysMenu]")
    @GetMapping("getSysMenuById/{id}")
    public CommonResult<SysMenu> getSysMenuById(@PathVariable String id){
        return sysMenuService.getSysMenuById(id);
    }

    @ApiOperation("根据id修改子表[sysMenu]")
    @PutMapping("updateSysMenuById")
    public CommonResult updateSysMenuById(@RequestBody SysMenuUpdateByIdDto dto){
        return sysMenuService.updateSysMenuById(dto);
    }

    @ApiOperation("根据id删除子表[sysMenu]")
    @DeleteMapping("deleteSysMenuById/{id}")
    public CommonResult deleteSysMenuById(@PathVariable String id){
        return sysMenuService.deleteSysMenuById(id);
    }

    @ApiOperation("添加数据子表[sysMenu]")
    @PostMapping("saveSysMenu")
    public CommonResult saveSysMenu(@RequestBody SysMenuSaveDto dto){
        return sysMenuService.saveSysMenu(dto);
    }

    @ApiOperation("获取树状下拉查询数据")
    @GetMapping("getTreeSelect")
    public CommonResult getTreeSelect(){
        return sysMenuService.getTreeSelect();
    }

    @ApiOperation("根据角色id查询角色权限")
    @GetMapping("getTreeSelectValueByRoleId")
    public CommonResult getTreeSelectValueByRoleId(String roleId){
        return sysMenuService.getTreeSelectValueByRoleId(roleId);
    }
}
