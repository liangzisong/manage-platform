package com.lzs.common.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lzs.common.pojo.common.SysDict;
import com.lzs.common.pojo.common.SysDictDetail;
import com.lzs.common.service.SysDictDetailService;
import com.lzs.common.service.SysDictService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SelectVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.stream.Collectors;

/**
* 系统字典详情Controller
*/
@RestController
@RequestMapping("sysDictDetail")
@ApiIgnore
public class SysDictDetailController {

    @Autowired
    private SysDictDetailService sysDictDetailService;
    @Autowired
    private SysDictService sysDictService;

    @ApiOperation("查询所有")
    @GetMapping("listByDictLabel")
    public CommonResult<SelectVo> listByDictLabel(String dictLabel){
        List<SelectVo> selectVoList = sysDictDetailService.listByDictLabel(dictLabel);
        return CommonResult.rows(selectVoList);
    }
}
