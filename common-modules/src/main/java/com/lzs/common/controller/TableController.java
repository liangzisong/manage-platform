package com.lzs.common.controller;

import com.lzs.common.dto.table.UpdateByIdDto;
import com.lzs.common.pojo.common.DbTable;
import com.lzs.common.service.TableService;
import com.lzs.common.vo.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("table")
@ApiIgnore
public class TableController {

    @Autowired
    private TableService tableService;

    @ApiOperation("根据表名称删除")
    @DeleteMapping("deleteByTableName/{tableName}")
    public CommonResult deleteByTableName(@PathVariable String tableName){
        return tableService.deleteByTableName(tableName);
    }

    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public CommonResult<DbTable> getById(@PathVariable String id){
        return tableService.getById(id);
    }

    @PutMapping("updateById")
    public CommonResult updateById(@RequestBody UpdateByIdDto dto){
        return tableService.updateById(dto);
    }

    @GetMapping("getAllTable")
    public CommonResult getAllTable(){
        return tableService.getAllTable();
    }

}
