package com.lzs.common.controller;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.GenerateCodeDto;
import com.lzs.common.enums.PrimaryKeyCreatePolicy;
import com.lzs.common.enums.field.FieldInputType;
import com.lzs.common.enums.field.FieldSearchType;
import com.lzs.common.enums.field.FieldShowType;
import com.lzs.common.pojo.common.DbTable;
import com.lzs.common.pojo.common.bo.DbFieldBo;
import com.lzs.common.service.CodeGenerateService;
import com.lzs.common.vo.CommonResult;
import com.lzs.common.vo.SelectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "代码生成")
@RestController
@RequestMapping("codeGenerate")
@ApiIgnore
public class CodeGenerateController {

    @Autowired
    private CodeGenerateService codeGenerateService;

    @ApiOperation("查询所有数据库表")
    @GetMapping("tableByPage")
    public CommonResult<DbTable> tableByPage(PageQuery pageQuery, String tableName){
        return codeGenerateService.tableByPage(pageQuery,tableName);
    }

    @ApiOperation("根据表名查询所有字段信息")
    @GetMapping("fieldByTable")
    public CommonResult<DbFieldBo> fieldByTable(String tableName){
        return codeGenerateService.fieldByTable(tableName);
    }

    @ApiOperation("查询显示字段类型")
    @GetMapping("getFieldShowType")
    public CommonResult<FieldShowType> getFieldShowType(){
        List<SelectVo> selectVoList = Arrays.stream(FieldShowType.values()).map(f -> {
            return SelectVo.builder()
                    .label(f.getComment())
                    .value(f.name()).build();
        }).collect(Collectors.toList());
        CommonResult commonResult = CommonResult.rows(selectVoList);
        //默认值
        commonResult.setObj(FieldShowType.TEXT);
        return commonResult;
    }

    @ApiOperation("查询搜索字段类型")
    @GetMapping("getFieldSearchType")
    public CommonResult<FieldSearchType> getFieldSearchType(){
        List<SelectVo> selectVoList = Arrays.stream(FieldSearchType.values()).map(f -> {
            return SelectVo.builder()
                    .label(f.getComment())
                    .value(f.name()).build();
        }).collect(Collectors.toList());
        CommonResult commonResult = CommonResult.rows(selectVoList);
        return commonResult;
    }

    @ApiOperation("查询输入字段类型")
    @GetMapping("getFieldInputType")
    public CommonResult<FieldInputType> getFieldInputType(){
        List<SelectVo> selectVoList = Arrays.stream(FieldInputType.values()).map(f -> {
            return SelectVo.builder()
                    .label(f.getComment())
                    .value(f.name()).build();
        }).collect(Collectors.toList());
        CommonResult commonResult = CommonResult.rows(selectVoList);
        return commonResult;
    }

    @ApiOperation("查询主键生成策略类型")
    @GetMapping("getPrimaryKeyCreatePolicy")
    public CommonResult<PrimaryKeyCreatePolicy> getPrimaryKeyCreatePolicy(){
        List<SelectVo> selectVoList = Arrays.stream(PrimaryKeyCreatePolicy.values()).map(f -> {
            return SelectVo.builder()
                    .label(f.getComment())
                    .value(f.name()).build();
        }).collect(Collectors.toList());
        CommonResult commonResult = CommonResult.rows(selectVoList);
        //默认uuid
        commonResult.setObj(PrimaryKeyCreatePolicy.ASSIGN_UUID);
        return commonResult;
    }

    @ApiOperation("生成代码")
    @PostMapping("generateCode")
    public CommonResult generateCode(@RequestBody GenerateCodeDto generateCodeDto){
        System.out.println("generateCodeDto = " + generateCodeDto);
        return codeGenerateService.generateCode(generateCodeDto);
    }

}
