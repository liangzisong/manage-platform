package com.lzs.common.annotations;


import com.lzs.common.enums.OperationType;

import java.lang.annotation.*;

/**
 * 操作日志表
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    //操作内容
    String value() default "";
    //操作类型
    OperationType operationType() default OperationType.OTHER;
}
