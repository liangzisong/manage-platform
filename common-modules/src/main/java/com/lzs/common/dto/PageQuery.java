package com.lzs.common.dto;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzs.common.vo.CommonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("分页查询对象")
@Data
public class PageQuery {
    @ApiModelProperty("第几页")
    private Integer pageNum = 1;
    @ApiModelProperty("查询几条")
    private Integer pageSize = 10;
    @ApiModelProperty(hidden = true)
    private IPage iPage;

    /**
     * 转换为分页对象
     * @return
     */
    public IPage toPage(){
        iPage = new Page<>(pageNum,pageSize);
        return iPage;
    }

    /**
     * 转换为commonResult对象
     * @return
     */
    public CommonResult toCommonResult(){
        return CommonResult.rows(iPage.getRecords(), iPage.getTotal(), iPage.getPages());
    }

    /**
     * 将行查询改为分页查询
     */
    public void changePage(){
        this.pageSize = this.pageNum * this.pageSize;
        this.pageNum = (this.pageNum-1) * this.pageSize;
    }
}
