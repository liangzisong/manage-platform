package com.lzs.common.dto.logs;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SysLogsGetByPageDto {

        @ApiModelProperty("id")
        private String id;
        @ApiModelProperty("operationStatus")
        private String operationStatus;
        @ApiModelProperty("operationDescribe")
        private String operationDescribe;
        @ApiModelProperty("requestIp")
        private String requestIp;
        @ApiModelProperty("requestUrl")
        private String requestUrl;

}
