package com.lzs.common.dto.role;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class SysRoleSaveDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("roleName")
    private String roleName;
    @ApiModelProperty("角色权限")
    private Set<String> menuIdSet;

}
