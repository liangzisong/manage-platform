package com.lzs.common.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String username;
    private String password;
    private String uid;
    private String code;
    private Integer w;
    private Integer h;
}
