package com.lzs.common.dto.dict;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysDictDetailSaveDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("dictionaryLabel")
    private String dictionaryLabel;
    @ApiModelProperty("dictionaryValue")
    private String dictionaryValue;
    @ApiModelProperty("sysDictId")
    private String sysDictId;
    private String dictionaryDescribe;

}
