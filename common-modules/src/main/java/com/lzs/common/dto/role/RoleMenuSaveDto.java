package com.lzs.common.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleMenuSaveDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("roleId")
    private String roleId;
    @ApiModelProperty("menuId")
    private String menuId;

}
