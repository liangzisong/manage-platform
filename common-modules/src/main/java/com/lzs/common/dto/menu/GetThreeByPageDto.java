package com.lzs.common.dto.menu;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Data
public class GetThreeByPageDto extends SysMenuGetByPageDto{
    @ApiModelProperty("父级id")
    private String parentId;
}
