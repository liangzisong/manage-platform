package com.lzs.common.dto.dict;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysDictUpdateByIdDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("dictionaryName")
    private String dictionaryName;
    private String dictionaryDescribe;

}
