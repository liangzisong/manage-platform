package com.lzs.common.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetRoleMenuByIdPageDto {

    @ApiModelProperty("一对多关系表连接字段")
    private String roleId;

    @ApiModelProperty("menuId")
    private String menuId;


}
