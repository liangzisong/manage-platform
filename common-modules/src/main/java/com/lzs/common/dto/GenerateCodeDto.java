package com.lzs.common.dto;

import com.lzs.common.dto.table.OneToManyTableDto;
import com.lzs.common.enums.field.FieldInputType;
import com.lzs.common.enums.field.FieldShowType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GenerateCodeDto {
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("驼峰")
    private String table_name;
    @ApiModelProperty("表名-首字母大写")
    private String t_ableName;
    @ApiModelProperty("别名")
    private String asTableName;
    @ApiModelProperty("模块名")
    private String moduleName;
    @ApiModelProperty("主键名称")
    private String primaryKeyName;
    @ApiModelProperty("主键名称-首字母大写")
    private String p_rimaryKeyName;
    @ApiModelProperty("表注释")
    private String tableComment;
    @ApiModelProperty("主键生成策略")
    private String primaryKeyCreatePolicy;
    @ApiModelProperty("是否导入导出")
    private Boolean inOut;
    @ApiModelProperty("是否必填")
    private Boolean required;
    @ApiModelProperty("是否存在字典关联")
    private Boolean haveJoinDict = false;
    @ApiModelProperty("是否存在图片字段")
    private Boolean haveImgField = false;
    @ApiModelProperty("是否存在富文本字段")
    private Boolean haveRichTextEditor = false;
    @ApiModelProperty("是否导出时生成时间，不可乱设改字段")
    private Boolean excelGenerateData;

    @ApiModelProperty("表字段list")
    private List<GenerateCodeFieldDto> tableFieldList;

    @ApiModelProperty("(where)查询字段list")
    private List<GenerateCodeFieldDto> whereFieldList;

    @ApiModelProperty("列表显示字段list")
    private List<GenerateCodeFieldDto> showFieldList;

    @ApiModelProperty("保存字段list")
    private List<GenerateCodeFieldDto> saveFieldList;

    @ApiModelProperty("修改(查询)字段list")
    private List<GenerateCodeFieldDto> updateFieldList;

    @ApiModelProperty("一对多表关联")
    private OneToManyTableDto oneToManyTableDto;

    @Data
    public static class GenerateCodeFieldDto{
        @ApiModelProperty("字段名")
        private String columnName;
        @ApiModelProperty("字段名(首字母大写)")
        private String columnNameTitleCase;
        @ApiModelProperty("字段名(下划线)")
        private String column_name;
        @ApiModelProperty("数据类型 大写")
        private String dataType;
        @ApiModelProperty("数据库的类型")
        private String jdbcType;
        @ApiModelProperty("关联字典(字典的label)")
        private String joinDict;
        @ApiModelProperty("是否必填")
        private Boolean required;
        /**
         * eq 等于
         * like 模糊
         * ne 不等于
         * gt 大于
         * lt 小于
         * ge 大于等于
         * le 小于等于
         */
        //控制是否可以查询
        @ApiModelProperty("查询方式(大写) eq like ne gt lt ge le")
        private String fieldSearchType;
        @ApiModelProperty("字段注释")
        private String columnComment;
        //控制是否可以添加
        @ApiModelProperty("字段输入框类型")
        private FieldInputType fieldInputType;
        //控制列表是否显示
        @ApiModelProperty("列表显示类型")
        private FieldShowType fieldShowType;
        @ApiModelProperty("是否为主键")
        private Boolean primaryKey;
        //控制是否可以修改
        @ApiModelProperty("是否可编辑")
        private Boolean haveEdit;
        @ApiModelProperty("是否可导入/导出")
        private Boolean inOut;

        public void setDataType(String dataType) {
            this.dataType = dataType;
            if("INT".equals(this.dataType)){
                this.jdbcType = "INTEGER";
            }else {
                this.jdbcType = this.dataType;
            }
        }

//        public String getJdbcType() {
//            if("INT".equals(this.dataType)){
//                this.jdbcType = "INTEGER";
//            }
//            return jdbcType;
//        }
    }

}
