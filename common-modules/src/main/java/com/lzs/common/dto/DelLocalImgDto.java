package com.lzs.common.dto;

import lombok.Data;

@Data
public class DelLocalImgDto {
    private String fileName;
}
