package com.lzs.common.dto.menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysMenuSaveDto {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("menuName")
    private String menuName;
    @ApiModelProperty("menuCode")
    private String menuCode;
    @ApiModelProperty("vuePath")
    private String vuePath;
    @ApiModelProperty("vueComponent")
    private String vueComponent;
    @ApiModelProperty("vueName")
    private String vueName;
    @ApiModelProperty("vueTitle")
    private String vueTitle;
    @ApiModelProperty("vueIcon")
    private String vueIcon;
    @ApiModelProperty("parentId")
    private String parentId;
    @ApiModelProperty("vueRedirect")
    private String vueRedirect;
    @ApiModelProperty("params")
    private String params;

}
