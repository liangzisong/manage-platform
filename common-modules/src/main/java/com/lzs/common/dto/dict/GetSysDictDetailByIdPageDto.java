package com.lzs.common.dto.dict;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetSysDictDetailByIdPageDto {

    @ApiModelProperty("一对多关系表连接字段")
    private String sysDictId;

        @ApiModelProperty("id")
        private String id;
        @ApiModelProperty("dictionaryLabel")
        private String dictionaryLabel;
        @ApiModelProperty("dictionaryValue")
        private String dictionaryValue;
}
