package com.lzs.common.dto.table;

import com.lzs.common.dto.GenerateCodeDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("一对多表关联dto")
public class OneToManyTableDto {
    @ApiModelProperty("是否打开一对多关系")
    private Boolean open;
    @ApiModelProperty("连接表(驼峰)")
    private String joinTable;
    @ApiModelProperty("连接表(下划线)")
    private String join_table;
    @ApiModelProperty("连接的字段")
    private String joinField;
    @ApiModelProperty("连接的字段(大写)")
    private String j_oinField;
    @ApiModelProperty("连接表的字段(下划线)")
    private String joinTableField;
    @ApiModelProperty("连接表的字段(大写)")
    private String j_oinTableField;
    @ApiModelProperty("连接表的字段(驼峰)")
    private String joinTableFieldHump;
    @ApiModelProperty("主键生成策略")
    private String primaryKeyCreatePolicy;
    @ApiModelProperty("是否存在字典关联")
    private Boolean haveJoinDict = false;
    @ApiModelProperty("是否存在图片字段")
    private Boolean haveImgField = false;
    @ApiModelProperty("是否存在富文本字段")
    private Boolean haveRichTextEditor = false;
    @ApiModelProperty("是否必填")
    private Boolean required;


    @ApiModelProperty("主键信息")
    private GenerateCodeDto.GenerateCodeFieldDto primaryKeyField;

    @ApiModelProperty("连接表(首字母大写)")
    private String j_oinTable;
    @ApiModelProperty("表备注")
    private String tableComment;
    @ApiModelProperty("表字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> tableFieldList;

    @ApiModelProperty("(where)查询字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> whereFieldList;

    @ApiModelProperty("列表显示字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> showFieldList;

    @ApiModelProperty("保存字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> saveFieldList;

    @ApiModelProperty("修改(查询)字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> updateFieldList;
}
