package com.lzs.common.dto.menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetSysMenuByIdPageDto {

    @ApiModelProperty("一对多关系表连接字段")
    private String parentId;

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("menuName")
    private String menuName;

    @ApiModelProperty("menuCode")
    private String menuCode;

    @ApiModelProperty("vueTitle")
    private String vueTitle;


}
