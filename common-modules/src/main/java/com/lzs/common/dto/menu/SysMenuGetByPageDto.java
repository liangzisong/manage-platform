package com.lzs.common.dto.menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysMenuGetByPageDto {

        @ApiModelProperty("id")
        private String id;
        @ApiModelProperty("menuName")
        private String menuName;
        @ApiModelProperty("menuCode")
        private String menuCode;
        @ApiModelProperty("vueTitle")
        private String vueTitle;

}
