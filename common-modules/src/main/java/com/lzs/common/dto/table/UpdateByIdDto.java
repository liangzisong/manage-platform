package com.lzs.common.dto.table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("表-根据id修改dto")
public class UpdateByIdDto {
    @ApiModelProperty("修改前表名")
    private String oldTableName;
    @ApiModelProperty("修改后表名")
    private String newTableName;
    @ApiModelProperty("表注释")
    private String tableComment;
}
