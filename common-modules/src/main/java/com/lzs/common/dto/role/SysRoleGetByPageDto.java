package com.lzs.common.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysRoleGetByPageDto {

        @ApiModelProperty("roleName")
        private String roleName;

}
