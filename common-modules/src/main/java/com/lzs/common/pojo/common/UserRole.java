package com.lzs.common.pojo.common;

import lombok.Data;

@Data
public class UserRole {
    private String id;
    private String roleId;
    private String userId;
}
