package com.lzs.common.pojo.common;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Repository;

@ApiModel("系统日志(SysLogs)")
@Data
public class SysLogs {
   @ApiModelProperty("id")
            @TableId(type = IdType.ASSIGN_UUID)
    private String id;
   @ApiModelProperty("操作状态0成功1失败")
    private String operationStatus;
   @ApiModelProperty("操作用户id")
    private String operationUserId;
   @ApiModelProperty("操作描述")
    private String operationDescribe;
   @ApiModelProperty("请求内容")
    private String requestParams;
   @ApiModelProperty("请求ip")
    private String requestIp;
   @ApiModelProperty("请求路径")
    private String requestUrl;
   @ApiModelProperty("响应内容")
    private String responseData;
   @ApiModelProperty("异常信息")
    private String errMsg;
   @ApiModelProperty("创建时间")
        @TableField(fill = FieldFill.INSERT)
    private Long createTime;
}
