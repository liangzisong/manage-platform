package com.lzs.common.pojo.common;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("系统字典详情(SysDictDetail)")
@Data
public class SysDictDetail {
   @ApiModelProperty("id")
   @TableId
    private String id;
   @ApiModelProperty("标签名")
    private String dictionaryLabel;
   @ApiModelProperty("标签值")
    private String dictionaryValue;
    @ApiModelProperty("标签描述")
    private String dictionaryDescribe;
   @ApiModelProperty("字典id")
    private String sysDictId;
}
