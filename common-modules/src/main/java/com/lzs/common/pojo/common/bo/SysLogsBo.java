package com.lzs.common.pojo.common.bo;

import com.lzs.common.pojo.common.SysLogs;
import com.lzs.common.pojo.common.SysUser;
import lombok.Data;

@Data
public class SysLogsBo extends SysLogs {
    private SysUser operationUser;
}
