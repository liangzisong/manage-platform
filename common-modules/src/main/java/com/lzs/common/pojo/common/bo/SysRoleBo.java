package com.lzs.common.pojo.common.bo;

import com.lzs.common.pojo.common.SysMenu;
import com.lzs.common.pojo.common.SysRole;
import lombok.Data;

import java.util.List;

@Data
public class SysRoleBo extends SysRole {
    private List<SysMenu> menuList;
    private String userRoleId;
}
