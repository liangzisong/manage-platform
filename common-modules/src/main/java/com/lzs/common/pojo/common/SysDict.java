package com.lzs.common.pojo.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("系统字典(SysDict)")
@Data
public class SysDict {
   @ApiModelProperty("id")
   @TableId
            (type = IdType.ASSIGN_UUID)
    private String id;
   @ApiModelProperty("字典名")
    private String dictionaryName;
    @ApiModelProperty("字典描述")
    private String dictionaryDescribe;
}
