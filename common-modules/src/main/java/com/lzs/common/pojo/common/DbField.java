package com.lzs.common.pojo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 数据库表信息
 * 通过 desc information_schema.tables 查到
 */
@Setter @Getter
@NoArgsConstructor @AllArgsConstructor
@ToString
@ApiModel("表字段")
public class DbField {

    @ApiModelProperty("名称")
    private String columnName;
    @ApiModelProperty("类型")
    private String dataType;
    @ApiModelProperty("是否非空")
    private String isNullable;
    @ApiModelProperty("索引类型")
    private String columnKey;
    @ApiModelProperty("默认值")
    private String columnDefault;
    @ApiModelProperty("其它信息")
    private String extra;
    @ApiModelProperty("描述")
    private String columnComment;
}
