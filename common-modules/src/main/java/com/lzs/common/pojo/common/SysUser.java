package com.lzs.common.pojo.common;

import lombok.Data;

@Data
public class SysUser {
    private String id;
    private String username;
    private String password;
    private String delFlag;
}
