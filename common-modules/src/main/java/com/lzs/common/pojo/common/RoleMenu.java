package com.lzs.common.pojo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("角色权限")
@Data
public class RoleMenu {
    @ApiModelProperty("主键id")
    private String id;
    @ApiModelProperty("主键id")
    private String roleId;
    @ApiModelProperty("主键id")
    private String menuId;
}
