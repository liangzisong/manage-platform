package com.lzs.common.pojo.common.bo;

import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.pojo.common.SysUser;
import lombok.Data;

import java.util.List;

@Data
public class SysUserBo extends SysUser {
    private List<SysRole> roleList;
}
