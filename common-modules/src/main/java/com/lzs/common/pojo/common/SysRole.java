package com.lzs.common.pojo.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("系统角色(SysRole)")
@Data
public class SysRole {
   @ApiModelProperty("id")
   @TableId
            (type = IdType.ASSIGN_UUID)
    private String id;
   @ApiModelProperty("角色名")
    private String roleName;
}
