package com.lzs.common.pojo.common.bo;

import com.lzs.common.enums.field.FieldInputType;
import com.lzs.common.pojo.common.DbField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel("数据库表字段bo")
@Data
@EqualsAndHashCode(callSuper = true)
public class DbFieldBo extends DbField {

    @ApiModelProperty("是否为主键")
    private Boolean primaryKey;

    @ApiModelProperty("是否可编辑")
    private Boolean haveEdit = true;

    private FieldInputType fieldInputType = FieldInputType.INPUT;
}
