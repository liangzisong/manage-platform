package com.lzs.common.pojo.common.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.lzs.common.pojo.common.SysMenu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("系统菜单(SysMenuBo)")
public class SysMenuBo extends SysMenu {

    @TableField(exist = false)
    @ApiModelProperty("是否存在子集")
    private Boolean hasChildren;
}
