package com.lzs.common.config;

import com.lzs.common.enums.ResultCodeEnum;
import com.lzs.common.exception.LzsBusinessException;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class JwtTokenUtil {

    private static Long tokenExpiration = 24 * 60 * 60 * 1000L;
    private static String tokenSignKey = "123456asdfcascq1343df3d3dqeqd3qvsfdcsdvdwercwfdvshjkercrwsoyopjrybrddfbnde45246534rtj63h3g6g636wr";
//    private static String userRoleKey = "userRole";

    public static String createToken(String userId) {
        Date date = new Date((System.currentTimeMillis() + tokenExpiration));

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String format = simpleDateFormat.format(date);
//        System.out.println("format-1 = " + format);

        String token = Jwts.builder().setSubject(userId)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, tokenSignKey).compressWith(CompressionCodecs.GZIP).compact();
        return token;
    }

//    public static String createToken(String userId, String role) {
//        String token = Jwts.builder().setSubject(userId)
//                .claim(userRoleKey, role)
//                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
//                .signWith(SignatureAlgorithm.HS512, tokenSignKey).compressWith(CompressionCodecs.GZIP).compact();
//        return token;
//    }

    public static String getUserIdFromToken(String token) {
        try {
            String userName = Jwts.parserBuilder()
                    .setSigningKey(tokenSignKey)
                    .build()
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
            return userName;
        }catch (Exception e){
            log.error("解析token异常[{}]",e.getMessage());
            throw new LzsBusinessException(ResultCodeEnum.UNAUTHORIZED_ERROR,"登陆已过期，请重新登陆");
        }
    }

    public static Date getExpiration(String token) {
        try {
            Date userName = Jwts.parserBuilder()
                    .setSigningKey(tokenSignKey)
                    .build()
                    .parseClaimsJws(token)
                    .getBody()
                    .getExpiration();
            return userName;
        }catch (Exception e){
            log.error("解析token异常[{}]",e.getMessage());
            throw new LzsBusinessException(ResultCodeEnum.UNAUTHORIZED_ERROR,"登陆已过期，请重新登陆");
        }
    }

//    public static void main(String[] args) {
//        String token = JwtTokenUtil.createToken("1111");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date expiration = JwtTokenUtil.getExpiration(token);
//        String format = simpleDateFormat.format(expiration);
//        System.out.println("format = " + format);
//    }

//    public static String getUserRoleFromToken(String token) {
//        Claims claims = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token).getBody();
//        return claims.get(userRoleKey).toString();
//    }
}
