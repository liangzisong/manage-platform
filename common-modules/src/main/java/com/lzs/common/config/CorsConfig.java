//package com.lzs.common.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//
///**
// * FileName: CorsConfig
// * <p>
// * <p>
// * <p>
// * Description: 配置cors跨域解析
// *
// * @author lzs
// * @version 1.0.0
// * @create 2020/2/7 22:33
// */
//@Configuration
//public class CorsConfig {
//
//
//    @Bean
//    public CorsFilter corsFilter (){
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.addAllowedOrigin("http://localhost:9528");
//        corsConfiguration.addAllowedOrigin("http://192.168.1.100:9528");
////        corsConfiguration.addAllowedOrigin("*");
//        // 允许发送Cookie信息
//        corsConfiguration.setAllowCredentials(true);
//        corsConfiguration.setMaxAge(3600L);
//        corsConfiguration.addAllowedMethod("OPTIONS");
//        corsConfiguration.addAllowedMethod("HEAD");
//        corsConfiguration.addAllowedMethod("GET");
//        corsConfiguration.addAllowedMethod("PUT");
//        corsConfiguration.addAllowedMethod("POST");
//        corsConfiguration.addAllowedMethod("DELETE");
//        corsConfiguration.addAllowedMethod("PATCH");
//
//        // 所有的头信息都被放行
//        corsConfiguration.addAllowedHeader("*");
//        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
//        corsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);
//        return new CorsFilter(corsConfigurationSource);
//    }
//
//}
