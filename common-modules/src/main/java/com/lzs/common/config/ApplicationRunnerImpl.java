package com.lzs.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public void run(ApplicationArguments args) {
        try {
            ValueOperations valueOperations = redisTemplate.opsForValue();
            valueOperations.set("test_ping","ping",1, TimeUnit.SECONDS);
            log.info("**********************************");
            log.info("***********Redis连接成功***********");
            log.info("**********************************");
        }catch (Exception e){
            log.error("**********************************");
            log.error("***********Redis连接失败***********");
            log.error("*********请检查连接是否正常**********");
            log.error("**********************************");
            throw e;
        }
    }
}
