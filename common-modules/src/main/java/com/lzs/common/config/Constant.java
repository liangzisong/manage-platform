package com.lzs.common.config;

import com.lzs.common.enums.LoginValidateType;

public interface Constant {
    String DB_NAME = "test001";
    String PROJECT_PATH = "/Users/liangzisong/devTools/IdeaProjects/manage-platform/";
    //security设置用户角色需要用到
    String ROLE_ = "ROLE_";
    //登录验证类型
    LoginValidateType LOGIN_VALIDATE_TYPE = LoginValidateType.none;

}
