package com.lzs.common.config.log;


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class LoggingLayoutConfig extends LayoutBase<ILoggingEvent> implements ApplicationContextAware {

    private static final String SPACE = " ";
    private static final String ENTER = "\r\n";
    private static final String LEFT = "[";
    private static final String RIGHT = "]";
    private static final String DATE_FORMAT_STR = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_TIME_FORMAT_STR = "yyyyMMddHHmmssSSS";

//    private static String smallName = null;

    private static ApplicationContext applicationContext;

    @Override
    public String doLayout(ILoggingEvent event) {
        StringBuilder sb = new StringBuilder();
        if (null != event && null != event.getMDCPropertyMap()) {
            //服务名
//            String smallName = getSmallName();
            //2020-02-26 11:39:28.015 [http-nio-8001-exec-3] INFO  c.sdworan.xxk.common.interceptor.CommonInterceptor [50] - /shiro/authenticationError;JSESSIONID=f277d4a62b9344ac918ab32340b29a0e: 进入的时间:2020-02-26 11:39:28
            String nowDate = DateTimeFormatter.ofPattern(DATE_FORMAT_STR).format(LocalDateTime.now());

            sb.append(CoreConstants.CURLY_LEFT);
//            sb.append(smallName);
//            sb.append(CoreConstants.DASH_CHAR);
            sb.append(nowDate);
            //逗号
            sb.append(CoreConstants.COMMA_CHAR);
//            sb.append(ThreadLocalUtil.getLogRandom());
            sb.append(CoreConstants.CURLY_RIGHT);
            sb.append(SPACE);
            sb.append(LEFT);
            sb.append(event.getThreadName());
            sb.append(RIGHT);
            sb.append(SPACE);
            sb.append(event.getLevel());
            sb.append(SPACE);
            sb.append(event.getLoggerName());
            sb.append(SPACE);
            sb.append(CoreConstants.DASH_CHAR);
            sb.append(SPACE);
            sb.append(event.getFormattedMessage());
            sb.append(ENTER);
        }

        return sb.toString();
    }

    /**
     * 生成五位随机数
     * @author 梁子松
     * @return 五位随机数
     */
    private String logRandom(){
        StringBuffer sb = new StringBuffer();
        for (int i=0;i<5;i++){
            int nextInt = 0;
            for (int j = 0; j < 3; j++) {
                nextInt = RandomUtils.nextInt(0, 3);
            }
            if (nextInt==0) {
                sb.append(RandomUtils.nextInt(0, 10));
            }else if (nextInt==1){
                sb.append((char)RandomUtils.nextInt(65, 91));
            }else {
                sb.append((char)RandomUtils.nextInt(97, 123));
            }
        }
        return sb.toString();
    }

    /**
     * 获取服务简称
     * @author 梁子松
     * @return 服务简称
     */
//    public String getSmallName(){
//        if(smallName==null){
//            try {
//                final String smallName = this.getContext().getProperty("SMALL_NAME");
//                LoggingLayoutConfig.smallName = smallName;
//            }catch (Exception e){
//                log.warn("没有找到系统简称,请指定sdworan.small-name");
//                e.printStackTrace();
//            }
//            if (LoggingLayoutConfig.smallName == null) {
//                LoggingLayoutConfig.smallName = "未指定服务名";
//            }
//
//        }
//        return smallName;
//    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
