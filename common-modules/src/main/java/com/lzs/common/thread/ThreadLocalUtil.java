package com.lzs.common.thread;

import javax.servlet.http.HttpServletRequest;

/**
 * FileName: ThreadLocalUtil
 * <p>
 * Description: 当前线程工具类
 *
 * @author lzs
 * @version 1.0.0
 * @create 2019/3/22 17:08
 */
public class ThreadLocalUtil {


//    private static ThreadLocal<HttpServletRequest> requestThreadLocal = new ThreadLocal<>();
    private static ThreadLocal<String> logIdThreadLocal = new ThreadLocal<>();

    //添加request到本地线程
//    public static void addRequest(HttpServletRequest request){
//        requestThreadLocal.set(request);
//    }

    //获得到request的方法
//    public static HttpServletRequest getRequest(){
//        return requestThreadLocal.get();
//    }

    public static void addLogId(String logId){
        logIdThreadLocal.set(logId);
    }
    public static String getLogId(){
        return logIdThreadLocal.get();
    }
    //删除当前线程的方法
    public static void removeBatch(){
        logIdThreadLocal.remove();
    }

}
