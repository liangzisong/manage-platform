package com.lzs.common.enums.field;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字段查询类型
 */
@Getter
@AllArgsConstructor
public enum FieldSearchType {
    EQ("输入框-精确查询"),
    LIKE("输入框-模糊查询"),
//    DATE_BETWEEN("时间区间"),
    SELECT("下拉选"),
    ;

    //描述
    private String comment;

}
