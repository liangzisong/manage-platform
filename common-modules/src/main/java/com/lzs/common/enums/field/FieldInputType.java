package com.lzs.common.enums.field;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字段查询类型
 */
@Getter
@AllArgsConstructor
public enum FieldInputType {
    INPUT("输入框"),
    SELECT("下拉选"),
    CREATE_TIME("创建时间"),
    UPDATE_TIME("修改时间"),
    IMAGE_UPLOAD("图片上传"),
    RICH_TEXT("富文本"),
//    RADIO("单选框"),
//    CHECKBOX("多选框"),
//    INPUT_NUMBER("计数器"),
//    CASCADER("级联选择器"),
//    SWITCH("开关"),
//    TIME_PICKER("时间选择器"),
//    DATE_PICKER("日期选择器"),
//    DATE_TIME_PICKER("时间日期选择器"),
//    UPLOAD("上传"),
//    RATE("评分"),
//    COLOR_PICKER("颜色选择器"),
//    TRANSFER("穿梭框"),

    ;

    //描述
    private String comment;

}
