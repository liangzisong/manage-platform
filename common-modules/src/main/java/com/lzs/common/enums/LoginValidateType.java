package com.lzs.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginValidateType {
    //图片拖动
    imageDrag("imageDrag"),
    //查看图片文字
    viewPictureText("viewPictureText"),
    //无任何验证
    none("none");
    private String type;
}
