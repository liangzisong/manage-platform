package com.lzs.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCodeEnum {
    //业务系统异常
    BUS_ERROR(500),
    //成功
    SUCCESS(200),
    //未授权
    UNAUTHORIZED_ERROR(401),

    ;
    int code;
}
