package com.lzs.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperationType {
    //插入
    INSERT(1,"插入"),
    //删除
    DELETE(2,"删除"),
    //查询
    SELECT(3,"查询"),
    //编辑
    UPDATE(4,"编辑"),
    //导入
    IMPORT(5,"导入"),
    //导出
    EXPORT(6,"导出"),
    //其他
    OTHER(10,"其他");
    private Integer code;
    private String describe;
}
