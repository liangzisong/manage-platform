package com.lzs.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 主键生成策略
 */
@Getter
@AllArgsConstructor
public enum PrimaryKeyCreatePolicy {
    AUTO("数据库自增"),
    INPUT("用户输入"),
    ASSIGN_ID("雪花算法"),
    ASSIGN_UUID("UUID"),
    ;

    //描述
    private String comment;

}
