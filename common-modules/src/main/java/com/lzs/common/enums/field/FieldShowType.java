package com.lzs.common.enums.field;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字段显示类型
 */
@Getter
@AllArgsConstructor
public enum FieldShowType {
    IMAGE("图片"),
    TEXT("文本"),
    ;

    //描述
    private String comment;

}
