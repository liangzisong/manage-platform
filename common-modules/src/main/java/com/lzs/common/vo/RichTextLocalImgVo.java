package com.lzs.common.vo;

import lombok.Data;

import java.util.List;

@Data
public class RichTextLocalImgVo {
    // 0 成功
    private Integer errno;
    private List<RichTextLocalImgVoInfo> data = null;
    @Data
    public static class RichTextLocalImgVoInfo{
        private String url;
        private String alt;
        private String href;
    }
}
