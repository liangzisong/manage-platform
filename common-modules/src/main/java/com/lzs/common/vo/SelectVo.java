package com.lzs.common.vo;

import lombok.*;

import java.util.List;

@Setter @Getter
@NoArgsConstructor @AllArgsConstructor
@Builder
public class SelectVo {
    private String label;
    private String value;
    private List<SelectVo> children;
}
