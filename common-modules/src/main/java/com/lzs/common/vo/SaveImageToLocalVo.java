package com.lzs.common.vo;

import lombok.Data;

@Data
public class SaveImageToLocalVo {
    //图片路径
    private String serverUrl;
    //字段，原样传输
    private String field;
    private String fileName;
}
