package com.lzs.common.vo;

import lombok.Data;

import java.util.List;

@Data
public class GetUserPowerVo {
    private List<String> roleNameList;
    private List<String> menuCodeList;
    private String userName;
}
