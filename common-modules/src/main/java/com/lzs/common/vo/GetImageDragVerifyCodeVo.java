package com.lzs.common.vo;

import lombok.Data;

@Data
public class GetImageDragVerifyCodeVo {
    //大图
    private String bigImage;
    //小图
    private String smallImage;
    private Integer OriginalH;
    private Integer OriginalW;
    private String uid;
    //偏移量 宽
    private Integer w;
    //偏移量 高
    private Integer h;
}
