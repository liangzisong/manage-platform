package com.lzs.common.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.enums.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.BooleanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@ToString
@ApiModel("返回对象")
public class CommonResult <T> {


    @ApiModelProperty("总条数")
    private Long total;// 总条数
    @ApiModelProperty("总页数")
    private Long totalPage;// 总页数
    @ApiModelProperty("当前页数据")
    private List<T> rows;// 当前页数据
    //返回单个对象时放入
    @ApiModelProperty("单个对象")
    private T obj;
    @ApiModelProperty("状态码：200是正常")
    private int code;
    @ApiModelProperty("消息")
    private String msg;

    private CommonResult(){

    }

    /**
     * 成功 无返回体
     * @return
     */
    public static CommonResult OK(){
        CommonResult commonResult = new CommonResult();
        commonResult.setMsg("操作成功");
        commonResult.setCode(ResultCodeEnum.SUCCESS.getCode());
        return commonResult;
    }

    public static CommonResult OK_MSG(String msg){
        CommonResult commonResult = new CommonResult();
        commonResult.setMsg(msg);
        commonResult.setCode(ResultCodeEnum.SUCCESS.getCode());
        return commonResult;
    }

    /**
     * 成功 单个对象
     * @param obj
     * @return
     */
    public static CommonResult OK(Object obj){
        CommonResult commonResult = CommonResult.OK();
        commonResult.setObj(obj);
        return commonResult;
    }

    /**
     * 空对象
     * @return
     */
    public static CommonResult EmptyRows(){
        CommonResult pageResult = CommonResult.OK();
        pageResult.setRows(new ArrayList());
        pageResult.setTotal(0L);
        return pageResult;
    }

    /**
     * 失败返回
     * @return
     */
    public static CommonResult Err(){
        CommonResult pageResult = new CommonResult<>();
        pageResult.setCode(ResultCodeEnum.BUS_ERROR.getCode());
        pageResult.setMsg("操作失败");
        return pageResult;
    }

    /**
     * 失败返回
     * @param msg
     * @return
     */
    public static CommonResult Err(String msg){
        CommonResult commonResult = CommonResult.Err();
        commonResult.setMsg(msg);
        return commonResult;
    }

    /**
     * 失败返回
     * @return
     */
    public static CommonResult Err(ResultCodeEnum code,String msg){
        CommonResult commonResult = CommonResult.Err();
        commonResult.setCode(code.getCode());
        if (Objects.nonNull(msg)) {
            commonResult.setMsg(msg);
        }
        return commonResult;
    }

    /**
     * 返回集合
     * @param list
     * @return
     */
    public static CommonResult rows(List list){
        CommonResult pageResult = CommonResult.OK();
        pageResult.setRows(list);
        return pageResult;
    }

    /**
     * 分页
     * @param list
     * @param total
     * @return
     */
    public static CommonResult rows(List list,long total){
        CommonResult pageResult = CommonResult.OK();
        pageResult.setRows(list);
        pageResult.setTotal(total);
        return pageResult;
    }
    public boolean isSuccess(){
        return code == ResultCodeEnum.SUCCESS.getCode();
    }
    public boolean isFail(){
        return BooleanUtils.isFalse(this.isSuccess());
    }
    /**
     * 分页
     * @param list
     * @param total
     * @return
     */
    public static CommonResult rows(List list,long total,long totalPage){
        CommonResult pageResult = CommonResult.OK();
        pageResult.setRows(list);
        pageResult.setTotal(total);
        pageResult.setTotalPage(totalPage);
        return pageResult;
    }

    /**
     * 封装
     * @param page
     * @return
     */
    public static CommonResult page(IPage page){
        return CommonResult.rows(page.getRecords(), page.getTotal(), page.getPages());
    }
}
