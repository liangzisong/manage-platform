package com.lzs.common.vo;

import com.lzs.common.dto.GenerateCodeDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
@Data
public class GenerateCodeVo {
    @ApiModelProperty("表名")
    private String tableName;
    private String TableName;
    @ApiModelProperty("表注释")
    private String tableComment;

    @ApiModelProperty("模块名")
    private String moduleName;

    @ApiModelProperty("生成字段list")
    private List<GenerateCodeDto.GenerateCodeFieldDto> generateCodeFieldDtoList;

    @Data
    public static class GenerateCodeFieldDto{
        @ApiModelProperty("字段名")
        private String columnName;
        @ApiModelProperty("数据类型")
        private String dataType;
        @ApiModelProperty("字段注释")
        private String columnComment;
        @ApiModelProperty("字段输入框类型")
        private String fieldInputType;
        @ApiModelProperty("列表显示类型")
        private String fieldShowType;
    }
}
