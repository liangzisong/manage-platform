package com.lzs.common.vo;

import lombok.Data;

import java.util.List;

@Data
public class GetInfoVo {
    private String id;
    private List roles;
    private String introduction;
    private String avatar;
    private String name;
}
