package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.dto.PageQuery;
import com.lzs.common.dto.menu.GetThreeByPageDto;
import com.lzs.common.dto.menu.SysMenuGetByPageDto;
import com.lzs.common.pojo.common.SysMenu;
import com.lzs.common.pojo.common.bo.SysMenuBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuDao extends BaseMapper<SysMenu> {
    /**
     * 分页查询
     */
    IPage<SysMenu> getByPage(IPage page,@Param("dto") SysMenuGetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);


    List<SysMenu> selectParentListByRoleId(@Param("roleId")String roleId);

    List<SysMenu> selectByParentId(@Param("roleId")String roleId, @Param("parentId") String parentId);

    IPage<SysMenu> getListByPage(IPage page,@Param("dto") SysMenuGetByPageDto dto);

    List<SysMenuBo> getList(@Param("dto") GetThreeByPageDto dto);
}