package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.pojo.common.SysRole;
import com.lzs.common.dto.role.SysRoleGetByPageDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleDao extends BaseMapper<SysRole> {
    /**
     * 分页查询
     */
    IPage<SysRole> getByPage(IPage page,@Param("dto")SysRoleGetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);

    List<SysRole> selectListByUserId(@Param("userId") String userId);

    /**
     * 根据用户id分页查询角色
     * @param toPage
     * @param userId
     * @return
     */
    IPage<SysRole> selectPageByUserId(IPage toPage,@Param("userId")  String userId);
}