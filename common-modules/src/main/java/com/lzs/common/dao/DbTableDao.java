package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.pojo.common.DbTable;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DbTableDao {

    IPage<DbTable> tableByPage(IPage toPage, @Param("dbName") String dbName, @Param("tableName") String tableName);


    /**
     * 根据表名删除
     * @param tableName
     * @return
     */
    void deleteByTableName(@Param("tableName") String tableName);

    /**
     * 修改表明
     * @param oldTableName 之前的表名
     * @param newTableName 修改后的表名
     */
    void rename(@Param("oldTableName") String oldTableName,@Param("newTableName") String newTableName);

    /**
     * 修改表注释
     * @param tableName
     * @param tableComment
     */
    void updateComment(@Param("tableName") String tableName,@Param("tableComment") String tableComment);
    /**
     * 根据id查询
     * @param id
     * @return
     */
    DbTable getById(@Param("tableName") String id,@Param("dbName") String dbName);

    /**
     * 查询所有表
     * @return
     */
    List<DbTable> getAllTable(@Param("dbName") String dbName);
}
