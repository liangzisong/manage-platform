package com.lzs.common.dao;

import com.lzs.common.pojo.common.DbField;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DbFieldDao {

    List<DbField> fieldByTable(@Param("dbName") String dbName, @Param("tableName") String tableName);
}
