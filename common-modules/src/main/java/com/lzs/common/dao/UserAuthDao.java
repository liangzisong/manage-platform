package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzs.common.pojo.common.SysUser;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthDao extends BaseMapper<SysUser> {
}
