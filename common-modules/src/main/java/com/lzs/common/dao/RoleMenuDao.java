package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzs.common.pojo.common.RoleMenu;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMenuDao extends BaseMapper<RoleMenu> {

}