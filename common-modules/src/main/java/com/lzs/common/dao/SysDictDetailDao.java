package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzs.common.pojo.common.SysDictDetail;
import org.springframework.stereotype.Repository;

@Repository
public interface SysDictDetailDao extends BaseMapper<SysDictDetail> {

}