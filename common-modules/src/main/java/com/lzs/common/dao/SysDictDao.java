package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.pojo.common.SysDict;
import com.lzs.common.dto.dict.SysDictGetByPageDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysDictDao extends BaseMapper<SysDict> {
    /**
     * 分页查询
     */
    IPage<SysDict> getByPage(IPage page,@Param("dto")SysDictGetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);


}