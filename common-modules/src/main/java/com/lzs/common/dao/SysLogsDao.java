package com.lzs.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzs.common.dto.logs.SysLogsGetByPageDto;
import com.lzs.common.pojo.common.SysLogs;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysLogsDao extends BaseMapper<SysLogs> {
    /**
     * 分页查询
     */
    IPage<SysLogs> getByPage(IPage page,@Param("dto") SysLogsGetByPageDto dto);


    /**
     * 逻辑删除
     */
    Integer deleteFlagById(@Param("id")String id);


}