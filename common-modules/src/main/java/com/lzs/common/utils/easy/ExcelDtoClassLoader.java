package com.lzs.common.utils.easy;

import com.lzs.common.utils.JudgeSystemUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Data
@Slf4j
public class ExcelDtoClassLoader extends ClassLoader {
    private String path;
    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        String myPath = this.getPath();
        System.out.println(myPath);
        if (JudgeSystemUtil.isWindows()) {
            myPath = myPath.replaceAll("\\\\", "/");
        }
        log.info("myPath=[{}]",myPath);
        log.info("name=[{}]",name);
        byte[] cLassBytes = null;
        Path path = null;
        try {
            path = Paths.get(new URI(myPath));
            cLassBytes = Files.readAllBytes(path);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        Class clazz = defineClass(name, cLassBytes, 0, cLassBytes.length);
        return clazz;
    }
}
