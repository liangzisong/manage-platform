package com.lzs.common.utils.wechat.weixin;

import lombok.Data;

/**
 * 企业付款到零钱返回信息
 */
@Data
public class WechatCompanyTransferPersonApiResult {
	private String return_code;
	private String return_msg;

	private String result_code;
	private String err_code;
	private String err_code_des;
	private String mch_appid;
	private String mchid;
	private String device_info;
	private String nonce_str;
	private String payment_no;
	private String partner_trade_no;
	private String payment_time;
}
