package com.lzs.common.utils.wechat.wechatpay;

import com.lzs.common.utils.MD5;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.util.DigestUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;


@Slf4j
public class WxPayUtil {

    private static final String ALGORITHM = "AES";
    private static final String ALGORITHM_MODE_PADDING = "AES/ECB/PKCS7Padding";
   static {
        try {
            Security.addProvider(new BouncyCastleProvider());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String decryptData(byte[] data,String signKey) throws Exception{
        SecretKeySpec secretKey = new SecretKeySpec(Objects.requireNonNull(MD5.getMD5(signKey)).toLowerCase().getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM_MODE_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(data));
    }

    public static String doRefund(String url, String xmlStr, String mchid, String sslPath){
        try {
            KeyStore keyStore  = KeyStore.getInstance("PKCS12");
            FileInputStream instream = new FileInputStream(new File(sslPath));//P12文件在服务器磁盘中的目录
            try {
                /**
                 * 此处要改成你的MCHID
                 * */
                keyStore.load(instream, mchid.toCharArray());//这里写密码..默认是你的MCHID
                SSLContext sslcontext = SSLContexts.custom()
                        .loadKeyMaterial(keyStore, mchid.toCharArray())//这里也是写密码的
                        .build();
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                        sslcontext,
                        new String[] { "TLSv1" },
                        null,
                        SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
                CloseableHttpClient httpclient = HttpClients.custom()
                        .setSSLSocketFactory(sslsf)
                        .build();
                try {
                    HttpPost httpost = new HttpPost(url); // 设置响应头信息
                    httpost.addHeader("Connection", "keep-alive");
                    httpost.addHeader("Accept", "*/*");
                    httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                    httpost.addHeader("Host", "api.mch.weixin.qq.com");
                    httpost.addHeader("X-Requested-With", "XMLHttpRequest");
                    httpost.addHeader("Cache-Control", "max-age=0");
                    httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
                    httpost.setEntity(new StringEntity(xmlStr, "UTF-8"));
                    CloseableHttpResponse response = httpclient.execute(httpost);
                    try {
                        HttpEntity entity = response.getEntity();

                        String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
                        EntityUtils.consume(entity);
                        return jsonStr;
                    } finally {
                        response.close();
                    }
                } finally {
                    httpclient.close();
                }
            } finally {
                instream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    //发送https POST 请求
    public static String httpsPost(String httpsUrl, String xmlStr) {

        BufferedReader in = null;
        InputStreamReader inputStreamReader = null;
        InputStream inputStream = null;
        HttpsURLConnection urlCon = null;
        try {
            urlCon = (HttpsURLConnection) (new URL(httpsUrl)).openConnection();
            urlCon.setDoInput(true);
            urlCon.setDoOutput(true);
            urlCon.setRequestMethod("POST");
            urlCon.setRequestProperty("Content-Length",
                    String.valueOf(xmlStr.getBytes().length));
            urlCon.setUseCaches(false);
            urlCon.getOutputStream().write(xmlStr.getBytes(StandardCharsets.UTF_8));
            urlCon.getOutputStream().flush();
            inputStream = urlCon.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            in = new BufferedReader(inputStreamReader);
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            log.info(sb.toString());
            return sb.toString();
        } catch (Exception e) {
           log.info("调用微信接口异常  error:{}",e);
        }finally{
            try {
                if (in != null) {
                    in.close();
                }
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (urlCon != null) {
                    urlCon.disconnect();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return null;
    }

    //将SortedMap转换成xml格式的String
    @SuppressWarnings({  "rawtypes" })
    public static String getReqXml(SortedMap<Object, Object> map){
        StringBuffer sb =new StringBuffer();
        sb.append("<xml>");
        Set<?> es = map.entrySet();
        Iterator<?> it = es.iterator();
        while(it.hasNext()){
            Map.Entry entry = (Map.Entry)it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if("attach".equalsIgnoreCase(k) ||
                    "body".equalsIgnoreCase(k) ||
                    "sign".equalsIgnoreCase(k)){
                sb.append("<"+k+">"+"<![CDATA["+v+"]]>"+"</"+k+">");
            }else{
                sb.append("<"+k+">"+v+"</"+k+">");
            }
        }
        sb.append("</xml>");
        return sb.toString();
    }

    //微信签名
    @SuppressWarnings("rawtypes")
    public static String createSign(SortedMap<Object, Object> map,String key){
        StringBuffer sb =new StringBuffer();
        Set<?> es = map.entrySet();
        Iterator<?> it = es.iterator();
        while(it.hasNext()){
            Map.Entry entry = (Map.Entry)it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if(null != v && !"".equals(v)&& !"sign".equals(k)){

                sb.append(k + "=" + v + "&");
            }
        }
        sb.append("key="+key);
        String md5Val = DigestUtils.md5DigestAsHex(getContentBytes(sb.toString(), "UTF-8")).toUpperCase();
        return md5Val;
    }
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }

    //将XML格式的String转换成MAP
    @SuppressWarnings({ "rawtypes" })
    public static Map<String,String> doXmlParse(String xmlStr){
        if(xmlStr == null || "".equals(xmlStr)){
            return null;
        }
        xmlStr = xmlStr.replaceFirst("encoding=\".*\"", "encoding=\"UTF-8\"");
        Map<String,String> m = new HashMap<String,String>();
        InputStream in = null;
        try {
            in = new ByteArrayInputStream(xmlStr.getBytes("UTF-8"));
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(in);
            Element root = doc.getRootElement();
            List list = root.getChildren();
            Iterator it = list.iterator();
            while(it.hasNext()){
                Element e = (Element)it.next();
                String k = e.getName();
                String v = "";
                List children = e.getChildren();
                if(children.isEmpty()){
                    v = e.getTextNormalize();
                }else{
                    v = getChildrenText(children);
                }
                m.put(k, v);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            try {
                in.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return m;
    }

    @SuppressWarnings("rawtypes")
    public static String getChildrenText(List children){
        StringBuffer sb =new StringBuffer();
        if(!children.isEmpty()){
            Iterator it = children.iterator();
            while(it.hasNext()){
                Element e = (Element)it.next();
                String name = e.getName();
                String value = e.getTextNormalize();
                List list = e.getChildren();
                sb.append("<"+name+">");
                if(!list.isEmpty()){
                    sb.append(getChildrenText(list));
                }
                sb.append(value);
                sb.append("</"+name+">");
            }
        }
        return sb.toString();
    }

    /**
     * 生成签名
     *
     * @param data 待签名数据
     * @param key API密钥
     * @return 签名
     */
    public static String generateSignature(final Map<String, String> data, String key) throws Exception {
        return generateSignature(data, key, SignType.MD5);
    }

    public static enum SignType{
        MD5, HMACSHA256
    }

    /**
     * 生成签名. 注意，若含有sign_type字段，必须和signType参数保持一致。
     *
     * @param data 待签名数据
     * @param key API密钥
     * @param signType 签名方式
     * @return 签名
     */
    public static String generateSignature(final Map<String, String> data, String key, SignType signType) throws Exception {
        Set<String> keySet = data.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        for (String k : keyArray) {
            if ("sign".equals(k)) {
                continue;
            }
            if (data.get(k).trim().length() > 0) {
                // 参数值为空，则不参与签名
                sb.append(k).append("=").append(data.get(k).trim()).append("&");
            }
        }
        sb.append("key=").append(key);
        if (SignType.MD5.equals(signType)) {
            return Objects.requireNonNull(MD5.getMD5(sb.toString())).toUpperCase();
        }
        else if (SignType.HMACSHA256.equals(signType)) {
            return HMACSHA256(sb.toString(), key);
        }
        else {
            throw new Exception(String.format("Invalid sign_type: %s", signType));
        }
    }
    /**
     * 生成 HMACSHA256
     * @param data 待处理数据
     * @param key 密钥
     * @return 加密结果
     * @throws Exception
     */
    public static String HMACSHA256(String data, String key) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] array = sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte item : array) {
            sb.append(Integer.toHexString((item & 0xFF) | 0x100), 1, 3);
        }
        return sb.toString().toUpperCase();
    }
    /**
     * 将Map转换为XML格式的字符串
     *
     * @param data Map类型数据
     * @return XML格式的字符串
     * @throws Exception
     */
    public static String mapToXml(Map<String, String> data) throws Exception {
        org.w3c.dom.Document document = newDocument();
        org.w3c.dom.Element root = document.createElement("xml");
        document.appendChild(root);
        for (String key: data.keySet()) {
            String value = data.get(key);
            if (value == null) {
                value = "";
            }
            value = value.trim();
            org.w3c.dom.Element filed = document.createElement(key);
            filed.appendChild(document.createTextNode(value));
            root.appendChild(filed);
        }
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMSource source = new DOMSource(document);
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String output = writer.getBuffer().toString(); //.replaceAll("\n|\r", "");
        try {
            writer.close();
        }
        catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return output;
    }
    public static org.w3c.dom.Document newDocument() throws ParserConfigurationException {
        return newDocumentBuilder().newDocument();
    }
    public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        documentBuilderFactory.setXIncludeAware(false);
        documentBuilderFactory.setExpandEntityReferences(false);

        return documentBuilderFactory.newDocumentBuilder();
    }

    /**
     * XML格式字符串转换为Map
     *
     * @param strXML XML字符串
     * @return XML数据转换后的Map
     * @throws Exception
     */
    public static Map<String, String> xmlToMap(String strXML) throws Exception {
        try {
            Map<String, String> data = new HashMap<String, String>();
            DocumentBuilder documentBuilder = newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(strXML.getBytes(StandardCharsets.UTF_8));
            org.w3c.dom.Document doc = documentBuilder.parse(stream);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int idx = 0; idx < nodeList.getLength(); ++idx) {
                Node node = nodeList.item(idx);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                    data.put(element.getNodeName(), element.getTextContent());
                }
            }
            try {
                stream.close();
            } catch (Exception ex) {
                // do nothing
            }
            return data;
        } catch (Exception ex) {
            log.error("Invalid XML, can not convert to map. Error message: {}. XML content: {}", ex.getMessage(), strXML);
            throw ex;
        }

    }

    public static boolean validateSignature(Map<String, String> map, String signKey) throws Exception {
        String sign = map.get("sign");
        if (sign == null) {
            return false;
        }
        return sign.equals(generateSignature(map,signKey));
    }


    public static boolean verifySign(String data,String sign,String filelname) throws Exception {
        PublicKey publicKeyByCert = getPublicKey(filelname);
        return verify(data, sign, publicKeyByCert);
    }
    /**
     * 验签
     * @param srcData
     * @param publicKey
     * @param sign
     * @return
     * @throws Exception
     */
    public static boolean verify(String srcData, String sign,  PublicKey publicKey) throws Exception {
        byte[] keyBytes = publicKey.getEncoded();
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey key = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(key);
        signature.update(srcData.getBytes());
        return signature.verify(Base64.decodeBase64(sign.getBytes()));
    }
    public static PublicKey getPublicKey(String filename) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
        try {
            String publicKey = content.replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace("-----END PUBLIC KEY-----", "")
                    .replaceAll("\\s+", "");

            return RSAUtils.getPublicKey(publicKey);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("当前Java环境不支持RSA", e);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            throw new RuntimeException("无效的密钥格式");
        }
    }
}
