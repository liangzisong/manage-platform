package com.lzs.common.utils.valid;

import com.lzs.common.exception.LzsBusinessException;

public class NumberValidator {

    public static void checkIsOne(Integer num, String msg){
        if (num == null) {
            throw new LzsBusinessException(msg);
        }
        if (num!=1) {
            throw new LzsBusinessException(msg);
        }
    }

    public static boolean checkIsOne(Integer num){
        if (num == null) {
            return false;
        }
        if (num.intValue()!=1) {
            return false;
        }
        return true;
    }

    public static boolean checkIsNotOne(Integer num){
        return !checkIsOne(num);
    }

    public static boolean equals(Integer num,Integer num2){
        if (num == null && num2 == null) {
            return true;
        }
        if (num == null || num2 == null) {
            return false;
        }
        return num.equals(num2);
    }

    public static void checkIsNotOne(Integer num, String msg){
        if (!checkIsOne(num)) {
            throw new LzsBusinessException(msg);
        }
    }

    public static void checkIsZero(Integer num, String msg) {
        if (num == null) {
            throw new LzsBusinessException(msg);
        }
        if (num!=0) {
            throw new LzsBusinessException(msg);
        }
    }

}
