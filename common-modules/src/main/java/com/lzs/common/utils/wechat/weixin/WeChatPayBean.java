package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class WeChatPayBean {
    /**
     * transaction_id : 1217752501201407033233368018
     * amount : {"payer_total":100,"total":100,"currency":"CNY","payer_currency":"CNY"}
     * mchid : 1230000109
     * trade_state : SUCCESS
     * bank_type : CMC
     * promotion_detail : [{"amount":100,"wechatpay_contribute":0,"coupon_id":"109519","scope":"GLOBAL","merchant_contribute":0,"name":"单品惠-6","other_contribute":0,"currency":"CNY","stock_id":"931386","goods_detail":[{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100},{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100}]},{"amount":100,"wechatpay_contribute":0,"coupon_id":"109519","scope":"GLOBAL","merchant_contribute":0,"name":"单品惠-6","other_contribute":0,"currency":"CNY","stock_id":"931386","goods_detail":[{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100},{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100}]}]
     * success_time : 2018-06-08T10:34:56+08:00
     * payer : {"openid":"oUpF8uMuAJO_M2pxb1Q9zNjWeS6o"}
     * out_trade_no : 1217752501201407033233368018
     * appid : wxd678efh567hg6787
     * trade_state_desc : 支付成功
     * trade_type : MICROPAY
     * attach : 自定义数据
     * scene_info : {"device_id":"013467007045764"}
     */

    //微信支付订单号
    @JSONField(name = "transaction_id")
    private String transactionId;
    @JSONField(name = "amount")
    private AmountBean amount;
    //商户号
    @JSONField(name = "mchid")
    private String mchid;
    //交易状态
    @JSONField(name = "trade_state")
    private String tradeState;
    //付款银行
    @JSONField(name = "bank_type")
    private String bankType;
    //支付完成时间
    @JSONField(name = "success_time")
    private Date successTime;
    @JSONField(name = "payer")
    private PayerBean payer;
    //商户订单号
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    @JSONField(name = "appid")
    private String appid;
    //交易状态描述
    @JSONField(name = "trade_state_desc")
    private String tradeStateDesc;
    //交易类型
    @JSONField(name = "trade_type")
    private String tradeType;
    //附加数据
    @JSONField(name = "attach")
    private String attach;
    @JSONField(name = "scene_info")
    private SceneInfoBean sceneInfo;
    @JSONField(name = "promotion_detail")
    private List<PromotionDetailBean> promotionDetail;

}
