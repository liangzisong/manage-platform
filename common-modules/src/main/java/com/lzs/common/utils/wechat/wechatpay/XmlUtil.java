package com.lzs.common.utils.wechat.wechatpay;


import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;

/**
 * @Author Allen.Lv
 * @Description //TODO
 * @Date 11:51 2019/3/1
 * @Desc: Coding Happy!
 **/
public class XmlUtil {

    /**
     *
     * @param inputXml
     * @param type
     * @return
     * @throws Exception
     */
    public static Object xml2Object(String inputXml, Class<?> type) {
        if (null == inputXml || "".equals(inputXml)) {
            return null;
        }
        XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("-_", "_")));
        xstream.alias("xml", type);
        return xstream.fromXML(inputXml);
    }

    /**
     * xml字符串转对象，忽略没有的属性
     * @param inputXml
     * @param type
     * @return
     * @throws Exception
     */
    public static Object xml2ObjectNoSuchField(String inputXml, Class<?> type) {
        if (null == inputXml || "".equals(inputXml)) {
            return null;
        }
        XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("-_", "_"))){
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        if (definedIn == Object.class) {
                            return false;
                        }
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
        XStream.setupDefaultSecurity(xstream);

        xstream.processAnnotations(type);
        xstream.alias("xml", type);
        xstream.allowTypes(new Class[]{type});
        return xstream.fromXML(inputXml);
    }

    /**
     *
     * @param inputStream
     * @param type
     * @return
     * @throws Exception
     */
    public static Object xml2Object(InputStream inputStream, Class<?> type) throws Exception {
        if (null == inputStream) {
            return null;
        }
        XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("-_", "_")));
        xstream.alias("xml", type);
        return xstream.fromXML(inputStream, type);
    }

    /**
     *
     * @param ro
     * @param types
     * @return
     * @throws Exception
     */
    public static String object2Xml(Object ro, Class<?> types) throws Exception {
        if (null == ro) {
            return null;
        }
        XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("-_", "_")));
        xstream.alias("xml", types);
        return xstream.toXML(ro);
    }
    public static String convertMap2Xml(Map<Object, Object> paraMap) {
        StringBuilder xmlStr = new StringBuilder();
        if (paraMap != null) {
            xmlStr.append("<xml>");
            Set<Object> keySet = paraMap.keySet();
            for (Object aKeySet : keySet) {
                String key = (String) aKeySet;
                String val = String.valueOf(paraMap.get(key));
                xmlStr.append("<");
                xmlStr.append(key);
                xmlStr.append(">");
                xmlStr.append(val);
                xmlStr.append("</");
                xmlStr.append(key);
                xmlStr.append(">");
            }
            xmlStr.append("</xml>");
        }
        return xmlStr.toString();
    }
}
