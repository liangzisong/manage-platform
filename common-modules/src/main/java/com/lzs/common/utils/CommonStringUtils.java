package com.lzs.common.utils;

/**
 * 字符串工具类
 */
public class CommonStringUtils {

    public static String toAsName(String str){
        char[] charArray = str.toCharArray();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < charArray.length; i++) {
            char itemChar = charArray[i];
            if(itemChar>='A'&&itemChar<='Z'){
                //转小写
                sb.append(Character.toLowerCase(itemChar));
            }
        }
        return sb.toString();
    }
}
