package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

@Data
public class PromotionDetailBean {
    /**
     * amount : 100
     * wechatpay_contribute : 0
     * coupon_id : 109519
     * scope : GLOBAL
     * merchant_contribute : 0
     * name : 单品惠-6
     * other_contribute : 0
     * currency : CNY
     * stock_id : 931386
     * goods_detail : [{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100},{"goods_remark":"商品备注信息","quantity":1,"discount_amount":1,"goods_id":"M1006","unit_price":100}]
     */

    @JSONField(name = "amount")
    private int amount;
    @JSONField(name = "wechatpay_contribute")
    private int wechatpayContribute;
    @JSONField(name = "coupon_id")
    private String couponId;
    @JSONField(name = "scope")
    private String scope;
    @JSONField(name = "merchant_contribute")
    private int merchantContribute;
    @JSONField(name = "name")
    private String name;
    @JSONField(name = "other_contribute")
    private int otherContribute;
    @JSONField(name = "currency")
    private String currency;
    @JSONField(name = "stock_id")
    private String stockId;
    @JSONField(name = "goods_detail")
    private List<GoodsDetailBean> goodsDetail;

}
