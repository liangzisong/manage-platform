package com.lzs.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * FileName: UUIDUtils
 * <p>
 * <p>
 * <p>
 * Description:
 *
 * @author lzs
 * @version 1.0.0
 * @create 2019/9/6 19:29
 */
public class UUIDUtils {

    public static String generateUUID(){
        // 重新命名文件的名称
        String uuid = UUID.randomUUID().toString();
        // 去除-
        String[] split = StringUtils.split(uuid, "-");
        uuid = "";
        for (String s : split) {
            uuid = uuid+s;
        }
        return uuid;
    }

}
