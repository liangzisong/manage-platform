package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class AmountBean {
    /**
     * payer_total : 100
     * total : 100
     * currency : CNY
     * payer_currency : CNY
     */

    @JSONField(name = "payer_total")
    private int payerTotal;
    @JSONField(name = "total")
    private int total;
    @JSONField(name = "currency")
    private String currency;
    @JSONField(name = "payer_currency")
    private String payerCurrency;

}
