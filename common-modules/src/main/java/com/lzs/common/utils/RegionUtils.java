package com.lzs.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lzs.common.vo.SelectVo;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class RegionUtils {
    public static List<SelectVo> regionList = null;
    static {
        String jsonStr = "";
        try {
            URL url = RegionUtils.class.getClassLoader().getResource("json/city.data.json");
            File file = new File(url.getPath());
            FileReader fileReader = new FileReader(file);
            Reader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            regionList = JSONObject.parseArray(jsonStr, SelectVo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
