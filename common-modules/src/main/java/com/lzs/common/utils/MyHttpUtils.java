package com.lzs.common.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;

@Slf4j
public class MyHttpUtils {
    public static String getPostData(HttpServletRequest request) {
        StringBuffer data = new StringBuffer();
        String line = null;
        BufferedReader reader = null;
        try {
            reader = request.getReader();
            while (null != (line = reader.readLine()))
                data.append(line);
        } catch (Exception e) {
            log.error("异常:"+e.getMessage());
        } finally {
        }
        return data.toString();
    }
}
