package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class GoodsDetailBean {
    /**
     * goods_remark : 商品备注信息
     * quantity : 1
     * discount_amount : 1
     * goods_id : M1006
     * unit_price : 100
     */

    @JSONField(name = "goods_remark")
    private String goodsRemark;
    @JSONField(name = "quantity")
    private int quantity;
    @JSONField(name = "discount_amount")
    private int discountAmount;
    @JSONField(name = "goods_id")
    private String goodsId;
    @JSONField(name = "unit_price")
    private int unitPrice;
}
