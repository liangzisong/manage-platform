package com.lzs.common.utils.easy;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class Test {
    @Excel(name = "名称")
    private String name;
    @Excel(name = "图片",type = 2,savePath = "/Users/liangzisong/devTools/IdeaProjects/manage-platform/common-modules/src/test")
    private String img;
}
