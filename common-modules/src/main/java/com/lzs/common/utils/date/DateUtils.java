package com.lzs.common.utils.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class DateUtils {
    public static final SimpleDateFormat ymdhmsSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static String formatYMDHMS(Long time){
        if (Objects.isNull(time)) {
            return null;
        }
        return ymdhmsSDF.format(new Date(time));
    }
}
