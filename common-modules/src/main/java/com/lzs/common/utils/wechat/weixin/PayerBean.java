package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class PayerBean {
    /**
     * openid : oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
     */

    @JSONField(name = "openid")
    private String openid;

}
