package com.lzs.common.utils.wechat.weixin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class SceneInfoBean {
    /**
     * device_id : 013467007045764
     */

    @JSONField(name = "device_id")
    private String deviceId;

}
