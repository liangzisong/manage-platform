package com.lzs.common.redis;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class RedisUserInfo {
    private String id;
    private String username;
    //角色名称
    private List<String> roleNameList;
    //菜单权限
    private List<String> menuCodeList;

}
