package com.lzs.common.redis;

public interface CommonRedisKeyPrefix {
    static final String USER_INFO = "user_info:";
    //登陆验证码
    public static final String SEND_LOGIN_SMS = "SEND_LOGIN_SMS:";

}
