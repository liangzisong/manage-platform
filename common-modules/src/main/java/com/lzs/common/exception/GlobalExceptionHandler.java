package com.lzs.common.exception;

import com.lzs.common.vo.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = LzsBusinessException.class)
    @ResponseBody
    public CommonResult lzsBusinessExceptionHandler(LzsBusinessException e){
        log.error("发生业务异常！原因是：[{}]",e.getMessage());
        return CommonResult.Err(e.getCodeEnum(),e.getMessage());
    }

    @ExceptionHandler(value = AuthenticationException.class)
    @ResponseBody
    public CommonResult authenticationException(AuthenticationException e){
        log.error("发生登陆异常！原因是：[{}]",e.getMessage());
        return CommonResult.Err("用户名或密码错误");
    }



    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public CommonResult RuntimeExceptionHandler(RuntimeException e){
        String message = e.getMessage();
        if (Objects.isNull(message)) {
            message = "接口空指针异常";
        }
        log.error("发生系统运行异常！原因是：[{}]", message);
        e.printStackTrace();
        return CommonResult.Err(message);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public CommonResult ExceptionHandler(Exception e){
        log.error("发生系统异常！原因是：[{}]",e.getMessage());
        e.printStackTrace();
        return CommonResult.Err(e.getMessage());
    }

}
