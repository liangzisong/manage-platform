package com.lzs.common.exception;

import com.lzs.common.enums.ResultCodeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * FileName: Exception
 * <p>
 * <p>
 * <p>
 * Description:
 *
 * @author lzs
 * @version 1.0.0
 * @create 2019/12/31 15:20
 */
@NoArgsConstructor
@Getter
@Setter
public class LzsBusinessException extends RuntimeException {

    private static final long serialVersionUID = 5157957078861442627L;

    private ResultCodeEnum codeEnum = ResultCodeEnum.BUS_ERROR;

    private String message;

    public LzsBusinessException(String message) {
        this.message = message;
    }

    public LzsBusinessException(ResultCodeEnum codeEnum,String message) {
        this.message = message;
        this.codeEnum = codeEnum;
    }

    public LzsBusinessException(ResultCodeEnum codeEnum,String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.codeEnum = codeEnum;
    }

}
