package com.lzs.common.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 系统文件配置
 * @author liangzisong
 */
@ConfigurationProperties(prefix = "weixin")
@Getter
@Setter
@ToString
public class WeixinProperties extends BaseWeixinProperties {

    private String mchid;
    private String notifyUrl;
    private String refundNotifyUrl;
    private String signKey;
    private String appUniformUrl;
    private String h5UniformUrl;
    private String jsapiUniformUrl;
    private String qrCodeUniformUrl;
    private String publicKeyPath;
    private String apiV3Key;
    private String merchantSerialNumber;
    private String merchantPrivateKey;
    private String companyTransferPersonCertName;
    private BaseWeixinProperties miniapp;
}
