package com.lzs.common.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PropertiesUtils {
    //excel内的图片保存位置
    @Value("${my-project.file.excel-upload-save-to-local-url}")
    private String excelUploadImgSaveToLocalUrl;
    private static String excelUploadImgSaveToLocalUrlVal;

    @Value("${my-project.file.excel-server-prefix-local}")
    private String excelServerPrefixLocal;
    private static String excelServerPrefixLocalVal;

    @PostConstruct
    public void setExcelUploadImgSaveToLocalUrl() {
        excelUploadImgSaveToLocalUrlVal = this.excelUploadImgSaveToLocalUrl;
    }
    public static String getExcelUploadImgSaveToLocalUrlVal() {
        return excelUploadImgSaveToLocalUrlVal;
    }

    @PostConstruct
    public void setExcelServerPrefixLocal() {
        excelServerPrefixLocalVal = this.excelServerPrefixLocal;
    }

    public static String getExcelServerPrefixLocalVal() {
        return excelServerPrefixLocalVal;
    }
}
