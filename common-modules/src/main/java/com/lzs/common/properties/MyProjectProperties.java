package com.lzs.common.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.List;

@Setter @Getter
@AllArgsConstructor @NoArgsConstructor
@ToString
@ConfigurationProperties(prefix = "my-project")
public class MyProjectProperties {
    private FileProperties file;
    private List<String> corsUrlList;
    private RedisKeyProperties redisKey = new RedisKeyProperties();
}
