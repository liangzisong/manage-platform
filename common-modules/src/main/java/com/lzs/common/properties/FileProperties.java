package com.lzs.common.properties;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@ToString
public class FileProperties {
    // 文件保存位置
//    private String saveToTempUrl;
    private String saveToLocalUrl;
    // 图片服务前缀
//    private String serverPrefixTemp;
    private String serverPrefixLocal;
    private String codeFileUrl;

}
