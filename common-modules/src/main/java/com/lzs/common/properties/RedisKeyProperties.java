package com.lzs.common.properties;

import lombok.Data;

@Data
public class RedisKeyProperties {
    //图片滑动验证码登录key
    private String loginImageSlideVerifyCode = "login:image:slide:verify_code:";
    //图片输入验证码登录key
    private String loginImageInputVerifyCode = "login:image:input:verify_code:";
}
