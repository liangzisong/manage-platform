package com.lzs.common.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 系统文件配置
 * @author liangzisong
 */
@Getter
@Setter
@ToString
public class BaseWeixinProperties {

    private String appid;
    private String secret;
}
