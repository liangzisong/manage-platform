package com.lzs.flowable.controller;

import com.lzs.common.dto.PageQuery;
import com.lzs.common.utils.BeanMapUtils;
import com.lzs.common.vo.CommonResult;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ActivityInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("flowableRuntimeTask")
public class RuntimeTaskController {

    @Autowired
    private TaskService taskService;

    private RuntimeService runtimeService;

    @GetMapping("getByPage")
    public CommonResult getByPage(PageQuery pageQuery){
        pageQuery.changePage();
        Integer pageNum = pageQuery.getPageNum();
        Integer pageSize = pageQuery.getPageSize();
        List<Task> tasks = taskService.createTaskQuery()
                .active()
                .listPage(pageNum,pageSize);
        long count = taskService.createTaskQuery()
                .active()
                .count();
        List<Map> list = new ArrayList(tasks.size());
        for (Task task : tasks) {
            Map map = new HashMap<String,Object>();
            map.put("assignee",task.getAssignee());
            map.put("id",task.getId());
            map.put("definitionKey",task.getTaskDefinitionKey());
            map.put("createTime",task.getCreateTime().getTime());
            map.put("processInstanceId",task.getProcessInstanceId());
            map.put("processDefinitionId",task.getProcessDefinitionId());
            list.add(map);
        }
        return CommonResult.rows(list,count);
    }

    @DeleteMapping("removeTask/{taskId}")
    public CommonResult removeTask(@PathVariable String taskId){
        taskService.deleteTask(taskId);
        return CommonResult.OK();
    }
}
