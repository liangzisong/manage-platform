package com.lzs.flowable.dto;

import lombok.Data;

@Data
public class FlowableProcdefStartDto {
    private String deploymentId;
}
