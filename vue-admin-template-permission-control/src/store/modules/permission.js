import { constantRoutes } from '@/router'
/* Layout */
import Layout from '@/layout'
/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

export function settingUserRoutes(role, newRoleArray, newRoleStrArray) {
  // console.log('childrenRole=', role.menuName)

  const newRole = {
    path: role.vuePath,
    name: role.menuName
  }
  if (JSON.stringify(newRole) === '{}') {
    return
  }
  if (!role.vueComponent) {
    return
  }
  if (role.vueComponent === 'Layout') {
    newRole.component = Layout
  } else {
    newRole.component = resolve => require([`@/views/${role.vueComponent}`], resolve)
    // newRole.component = () => import('@/views/form/index')
    // newRole.component = () => import(`@/views/${role.vueComponent}`)
    // console.log('newRole.component=', newRole.component)
    // debugger
  }

  if (role.vueTitle) {
    if (!newRole['meta']) {
      newRole['meta'] = {}
    }
    newRole.meta.title = role.vueTitle
  }
  if (role.vueIcon) {
    if (!newRole['meta']) {
      newRole['meta'] = {}
    }
    newRole['meta'].icon = role.vueIcon
  }
  if (role.params) {
    newRole['params'] = role.params
  }
  if (role.vueRedirect) {
    newRole['redirect'] = role.vueRedirect
  }
  // console.log('newRole=', newRole)
  newRoleArray.push(newRole)
  newRoleStrArray.push(role.menuCode)

  if (role.childList && role.childList.length > 0) {
    newRole.children = []
    for (let i = 0; i < role.childList.length; i++) {
      const childrenRole = role.childList[i]
      settingUserRoutes(childrenRole, newRole.children, newRoleStrArray)
    }
  }
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      let newRoleArray = null
      newRoleArray = []
      let newRoleStrArray = null
      newRoleStrArray = []
      for (let i = 0; i < roles.length; i++) {
        const role = roles[i]
        settingUserRoutes(role, newRoleArray, newRoleStrArray)
      }
      // console.log('newRoleArray=', newRoleArray)
      // newRoleArray = [{
      //   path: '/form1',
      //   component: Layout,
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'Form',
      //       component: () => import('@/views/modules/system/sysUserIndex'),
      //       meta: { title: 'Form', icon: 'form' }
      //     }
      //   ]
      // }]
      // let accessedRoutes
      // if (roles.includes('admin')) {
      //   accessedRoutes = asyncRoutes || []
      // } else {
      //   accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      // }
      commit('SET_ROUTES', newRoleArray)
      resolve(newRoleArray)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
