import request from '@/utils/request'

export function uploadLocalImg(data) {
  return request({
    url: '/fileUpload/localImg',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data
  })
}
export function delLocalImg(data) {
  return request({
    url: '/fileUpload/delLocalImg',
    method: 'delete',
    data
  })
}

