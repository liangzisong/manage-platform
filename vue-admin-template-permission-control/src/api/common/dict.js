import request from '@/utils/request'

export function getSysDictList(params) {
  return request({
    url: '/sysDict/list',
    method: 'get',
    params
  })
}

export function getSysDictDetaillistByDictLabel(params) {
  return request({
    url: '/sysDictDetail/listByDictLabel',
    method: 'get',
    params
  })
}
