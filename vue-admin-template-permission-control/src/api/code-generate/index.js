import request from '@/utils/request'

export function tableByPage(params) {
  return request({
    url: '/codeGenerate/tableByPage',
    method: 'get',
    params
  })
}

export function getFieldByTable(params) {
  return request({
    url: '/codeGenerate/fieldByTable',
    method: 'get',
    params
  })
}

export function getFieldShowType(params) {
  return request({
    url: '/codeGenerate/getFieldShowType',
    method: 'get',
    params
  })
}

export function getFieldSearchType(params) {
  return request({
    url: '/codeGenerate/getFieldSearchType',
    method: 'get',
    params
  })
}

export function getFieldInputType(params) {
  return request({
    url: '/codeGenerate/getFieldInputType',
    method: 'get',
    params
  })
}

export function getPrimaryKeyCreatePolicy(params) {
  return request({
    url: '/codeGenerate/getPrimaryKeyCreatePolicy',
    method: 'get',
    params
  })
}

/**
 * 根据表名称删除表
 * @param params
 * @returns {AxiosPromise}
 */
export function deleteByTableName(params) {
  return request({
    url: '/table/deleteByTableName/' + params,
    method: 'delete'
  })
}

export function generateCode(data) {
  return request({
    url: '/codeGenerate/generateCode',
    method: 'post',
    data
  })
}
