import request from '@/utils/request'

export function getById(tableName) {
  return request({
    url: '/table/getById/' + tableName,
    method: 'get'
  })
}

export function getAllTable() {
  return request({
    url: '/table/getAllTable',
    method: 'get'
  })
}

export function updateById(data) {
  return request({
    url: '/table/updateById',
    method: 'put',
    data
  })
}
