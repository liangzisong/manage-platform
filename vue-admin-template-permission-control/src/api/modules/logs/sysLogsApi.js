import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/sysLogs/getById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateById(data) {
  return request({
    url: '/sysLogs/updateById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveData(data) {
  return request({
    url: '/sysLogs/save',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/sysLogs/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/sysLogs/deleteById/' + params,
    method: 'delete'
  })
}

