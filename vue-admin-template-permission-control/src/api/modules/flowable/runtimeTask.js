import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/flowableProcdef/getById/' + id,
    method: 'get'
  })
}

/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/flowableRuntimeTask/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/flowableProcdef/deleteById/' + params,
    method: 'delete'
  })
}
