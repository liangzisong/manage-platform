import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/flowableProcdef/getById/' + id,
    method: 'get'
  })
}

/** 启动流程 */
export function startFlow(data) {
  return request({
    url: '/flowableProcdef/start',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/flowableProcdef/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/flowableProcdef/deleteById/' + params,
    method: 'delete'
  })
}

/**
 * 上传bpmn文件
 * @param data
 * @returns {AxiosPromise}
 */
export function importBpmnFile(data) {
  return request({
    url: '/flowableProcdef/importBpmnFile',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data
  })
}

export function getFlowImg(params) {
  return request({
    url: '/flowableProcdef/getFlowImg',
    method: 'get',
    params
  })
}
