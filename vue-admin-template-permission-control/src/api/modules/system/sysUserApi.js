import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/sysUser/getById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateById(data) {
  return request({
    url: '/sysUser/updateById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveData(data) {
  return request({
    url: '/sysUser/save',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/sysUser/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/sysUser/deleteById/' + params,
    method: 'delete'
  })
}

/** 根据主表id查询 */
export function getSysRoleByIdPage(params) {
  return request({
    url: '/sysUser/getSysRoleByIdPage',
    method: 'get',
    params
  })
}
/** 根据主键查询 */
export function getUserRoleById(id) {
  return request({
    url: '/sysUser/getUserRoleById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateUserRoleById(data) {
  return request({
    url: '/sysUser/updateUserRoleById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveUserRoleData(data) {
  return request({
    url: '/sysUser/saveUserRole',
    method: 'post',
    data
  })
}
/** 根据删除 */
export function deleteUserRoleById(params) {
  return request({
    url: '/sysUser/deleteUserRoleById/' + params,
    method: 'delete'
  })
}

export function updatePassword(data) {
  return request({
    url: '/sysUser/updatePassword',
    method: 'post',
    data
  })
}
