import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/sysRole/getById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateById(data) {
  return request({
    url: '/sysRole/updateById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveData(data) {
  return request({
    url: '/sysRole/save',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/sysRole/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/sysRole/deleteById/' + params,
    method: 'delete'
  })
}

/** 根据主表id查询 */
export function getRoleMenuByIdPage(params) {
  return request({
    url: '/sysRole/getRoleMenuByIdPage',
    method: 'get',
    params
  })
}
/** 根据主键查询 */
export function getRoleMenuById(id) {
  return request({
    url: '/sysRole/getRoleMenuById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateRoleMenuById(data) {
  return request({
    url: '/sysRole/updateRoleMenuById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveRoleMenuData(data) {
  return request({
    url: '/sysRole/saveRoleMenu',
    method: 'post',
    data
  })
}
/** 根据删除 */
export function deleteRoleMenuById(params) {
  return request({
    url: '/sysRole/deleteRoleMenuById/' + params,
    method: 'delete'
  })
}

/** 角色下拉 */
export function getRoleSelect() {
  return request({
    url: '/sysRole/getRoleSelect',
    method: 'get'
  })
}
