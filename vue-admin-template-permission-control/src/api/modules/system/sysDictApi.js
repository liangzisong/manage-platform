import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/sysDict/getById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateById(data) {
  return request({
    url: '/sysDict/updateById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveData(data) {
  return request({
    url: '/sysDict/save',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/sysDict/getByPage',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/sysDict/deleteById/' + params,
    method: 'delete'
  })
}

/** 根据主表id查询 */
export function getSysDictDetailByIdPage(params) {
  return request({
    url: '/sysDict/getSysDictDetailByIdPage',
    method: 'get',
    params
  })
}
/** 根据主键查询 */
export function getSysDictDetailById(id) {
  return request({
    url: '/sysDict/getSysDictDetailById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateSysDictDetailById(data) {
  return request({
    url: '/sysDict/updateSysDictDetailById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveSysDictDetailData(data) {
  return request({
    url: '/sysDict/saveSysDictDetail',
    method: 'post',
    data
  })
}
/** 根据删除 */
export function deleteSysDictDetailById(params) {
  return request({
    url: '/sysDict/deleteSysDictDetailById/' + params,
    method: 'delete'
  })
}
