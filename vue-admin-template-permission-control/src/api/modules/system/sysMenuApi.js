import request from '@/utils/request'

/** 根据主键查询 */
export function getById(id) {
  return request({
    url: '/sysMenu/getById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateById(data) {
  return request({
    url: '/sysMenu/updateById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveData(data) {
  return request({
    url: '/sysMenu/save',
    method: 'post',
    data
  })
}
/** 分页查询列表数据 */
export function getByPage(params) {
  return request({
    url: '/sysMenu/getByPage',
    method: 'get',
    params
  })
}
/** 分页查询列表数据 */
export function getListByPage(params) {
  return request({
    url: '/sysMenu/getListByPage',
    method: 'get',
    params
  })
}
/** 查询列表数据 */
export function getList(params) {
  return request({
    url: '/sysMenu/getList',
    method: 'get',
    params
  })
}

/** 根据删除 */
export function deleteById(params) {
  return request({
    url: '/sysMenu/deleteById/' + params,
    method: 'delete'
  })
}

/** 根据主表id查询 */
export function getSysMenuByIdPage(params) {
  return request({
    url: '/sysMenu/getSysMenuByIdPage',
    method: 'get',
    params
  })
}
/** 根据主键查询 */
export function getSysMenuById(id) {
  return request({
    url: '/sysMenu/getSysMenuById/' + id,
    method: 'get'
  })
}

/** 根据主键修改 */
export function updateSysMenuById(data) {
  return request({
    url: '/sysMenu/updateSysMenuById',
    method: 'put',
    data
  })
}

/** 根据主键修改 */
export function saveSysMenuData(data) {
  return request({
    url: '/sysMenu/saveSysMenu',
    method: 'post',
    data
  })
}
/** 根据删除 */
export function deleteSysMenuById(params) {
  return request({
    url: '/sysMenu/deleteSysMenuById/' + params,
    method: 'delete'
  })
}

/** 获取树状下拉查询数据 */
export function getTreeSelect() {
  return request({
    url: '/sysMenu/getTreeSelect',
    method: 'get'
  })
}
/** 根据角色id查询角色权限 */
export function getTreeSelectValueByRoleId(params) {
  return request({
    url: '/sysMenu/getTreeSelectValueByRoleId',
    method: 'get',
    params
  })
}
