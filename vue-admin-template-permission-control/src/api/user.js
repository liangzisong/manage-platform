import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/userAuth/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/userAuth/info',
    method: 'get'
  })
}

/**
 * 获取登陆菜单权限
 * @returns {AxiosPromise}
 */
export function getLoginMenu() {
  return request({
    url: '/userAuth/getLoginMenu',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/userAuth/logout',
    method: 'post'
  })
}

export function getImageDragVerifyCode() {
  return request({
    url: '/userAuth/getImageDragVerifyCode',
    method: 'get'
  })
}
export function getLoginValidateType() {
  return request({
    url: '/userAuth/getLoginValidateType',
    method: 'get'
  })
}
export function getImageTextVerifyCode() {
  return request({
    url: '/userAuth/getImageTextVerifyCode',
    method: 'get'
  })
}
